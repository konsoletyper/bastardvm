package org.bastardvm.compiler.model.test

import org.junit.Test
import org.bastardvm.compiler.model.*
import kotlin.test.assertEquals

class TypeParserTest {
    private val parser = TypeParser()

    @Test
    fun parsesPrimitives() {
        assertEquals(Type.Boolean, parse("Z"))
        assertEquals(Type.Int, parse("I"))
    }

    @Test
    fun parsesObject() {
        assertEquals(Type.Obj("java.lang.Object"), parse("Ljava/lang/Object;"))
    }

    @Test
    fun parsesArray() {
        assertEquals(Type.Array(Type.Obj("java.lang.Object")), parse("[Ljava/lang/Object;"))
        assertEquals(Type.Array(Type.Int), parse("[I"))
    }

    @Test
    fun parsesMethodSignature() {
        assertEquals(MethodSignature(Type.Boolean, Type.Obj("java.lang.String"), Type.Int),
                parseSignature("(Ljava/lang/String;I)Z"))
    }

    @Test
    fun parsesMethodSignatureWithVoidResult() {
        assertEquals(MethodSignature(Type.Void, Type.Array(Type.Obj("java.lang.String"))),
                parseSignature("([Ljava/lang/String;)V"))
    }

    @Test
    fun parsesMethodSignatureWithNoParameters() {
        assertEquals(MethodSignature(Type.Int), parseSignature("()I"))
    }

    private fun parse(str: String): Type {
        val result = parser.reset().parse(str)
        if (parser.lastIndex < str.length) {
            throw TypeParseException(str, parser.lastIndex)
        }
        return result
    }

    private fun parseSignature(str: String): MethodSignature {
        val result = parser.reset().parseSignature(str)
        if (parser.lastIndex < str.length) {
            throw TypeParseException(str, parser.lastIndex)
        }
        return result
    }
}