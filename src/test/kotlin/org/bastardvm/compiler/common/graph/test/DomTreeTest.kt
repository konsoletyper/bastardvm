package org.bastardvm.compiler.common.graph.test

import org.bastardvm.compiler.common.graph.Graph
import org.bastardvm.compiler.common.graph.MutableGraph
import org.bastardvm.compiler.common.graph.Tree
import org.bastardvm.compiler.common.graph.buildDominatorTree
import org.junit.Test
import kotlin.test.assertEquals

class DomTreeTest {
    val names = ('a'..'z').map { "$it" }.toList()

    @Test fun buildsDominatorTree() {
        val graph = MutableGraph<String>()
        val nodes = names.take(6).map { graph.nodeOf(it) }.toList()
        nodes[0].connectTo(nodes[1])
        nodes[1].connectTo(nodes[2])
        nodes[2].connectTo(nodes[3])
        nodes[0].connectTo(nodes[4])
        nodes[3].connectTo(nodes[5])
        nodes[4].connectTo(nodes[5])
        val domTree = graph.buildDominatorTree { it }

        assertEquals(setOf("b", "e", "f"), successors(graph, domTree, "a"))
        assertEquals(setOf("c"), successors(graph, domTree, "b"))
        assertEquals(setOf("d"), successors(graph, domTree, "c"))
        assertEquals(emptySet(), successors(graph, domTree, "d"))
        assertEquals(emptySet(), successors(graph, domTree, "e"))
        assertEquals(emptySet(), successors(graph, domTree, "f"))
    }

    @Test fun respectsLoop() {
        val graph = MutableGraph<String>()
        val nodes = names.take(5).map { graph.nodeOf(it) }.toList()
        nodes[0].connectTo(nodes[1])
        nodes[1].connectTo(nodes[2])
        nodes[2].connectTo(nodes[3])
        nodes[3].connectTo(nodes[1])
        nodes[1].connectTo(nodes[4])
        val domTree = graph.buildDominatorTree { it }

        assertEquals(setOf("b"), successors(graph, domTree, "a"))
        assertEquals(setOf("c", "e"), successors(graph, domTree, "b"))
        assertEquals(setOf("d"), successors(graph, domTree, "c"))
        assertEquals(emptySet(), successors(graph, domTree, "d"))
        assertEquals(emptySet(), successors(graph, domTree, "e"))
    }

    @Test fun respectsIrreducibleLoop() {
        val graph = MutableGraph<String>()
        val nodes = names.take(5).map { graph.nodeOf(it) }.toList()
        nodes[0].connectTo(nodes[1])
        nodes[0].connectTo(nodes[2])
        nodes[1].connectTo(nodes[2])
        nodes[2].connectTo(nodes[3])
        nodes[3].connectTo(nodes[1])
        nodes[1].connectTo(nodes[4])
        val domTree = graph.buildDominatorTree { it }

        assertEquals(setOf("b", "c"), successors(graph, domTree, "a"))
        assertEquals(setOf("e"), successors(graph, domTree, "b"))
        assertEquals(setOf("d"), successors(graph, domTree, "c"))
        assertEquals(emptySet(), successors(graph, domTree, "d"))
        assertEquals(emptySet(), successors(graph, domTree, "e"))
    }

    private fun successors(graph: Graph<String>, tree: Tree<Graph.Node<String>>, value: String): Set<String> {
        return tree.nodeMap[graph.nodeMap[value]!!]!!.successors.map { it.value.value }.toSet()
    }
}
