package org.bastardvm.compiler.common.graph.test

import org.bastardvm.compiler.common.graph.MutableGraph
import org.bastardvm.compiler.common.graph.topologicalSort
import org.junit.Test
import kotlin.test.assertTrue

class TopologicalSortTest {
    @Test fun sorts() {
        val graph = MutableGraph<String>()
        graph.nodeOf("a").connectTo(graph.nodeOf("b"))
        graph.nodeOf("b").connectTo(graph.nodeOf("c"))
        graph.nodeOf("c").connectTo(graph.nodeOf("d"))
        graph.nodeOf("d").connectTo(graph.nodeOf("g"))
        graph.nodeOf("a").connectTo(graph.nodeOf("e"))
        graph.nodeOf("e").connectTo(graph.nodeOf("f"))
        graph.nodeOf("f").connectTo(graph.nodeOf("g"))
        graph.nodeOf("g").connectTo(graph.nodeOf("h"))
        graph.nodeOf("h").connectTo(graph.nodeOf("g"))
        val list = graph.topologicalSort().map { it.value }

        assertTrue { list.first() == "a" }
        assertTrue { list.last() == "h" }
        assertTrue { list.indexOf("a") < list.indexOf("h") }
        assertTrue { list.indexOf("a") < list.indexOf("g") }
        assertTrue { list.indexOf("g") < list.indexOf("h") }
        assertTrue { list.indexOf("c") < list.indexOf("g") }
        assertTrue { list.indexOf("b") < list.indexOf("c") }
        assertTrue { list.indexOf("e") < list.indexOf("g") }
    }
}