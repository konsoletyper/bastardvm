package org.bastardvm.compiler.common.graph.test

import org.bastardvm.compiler.common.graph.MutableLCATree
import org.junit.Test
import kotlin.test.assertEquals

class MutableLCATreeTest {
    @Test fun simpleTest() {
        val tree = MutableLCATree("root")
        val a = tree.root.appendChild("a")
        val b = tree.root.appendChild("b")
        val c = a.appendChild("c")
        val d = a.appendChild("d")
        val e = b.appendChild("e")

        assertEquals(tree.root.value, e.lca(c).value)
        assertEquals(a.value, c.lca(d).value)
        assertEquals(tree.root.value, tree.root.lca(e).value)
    }

    @Test fun deepTest() {
        val tree = MutableLCATree("root")
        var a = tree.root.appendChild("a")
        var b = tree.root.appendChild("b")
        for (i in 1..20) {
            a = a.appendChild("a$i")
            b = b.appendChild("b$i")
        }
        var c = tree.nodeMap["a10"]!!.appendChild("c")
        for (i in 1..20) {
            c = c.appendChild("c$i")
        }
        assertEquals("root", tree.nodeMap["a20"]!!.lca(tree.nodeMap["b20"]!!).value)
        assertEquals("root", tree.nodeMap["a20"]!!.lca(tree.nodeMap["b10"]!!).value)
        assertEquals("root", tree.nodeMap["a20"]!!.lca(tree.nodeMap["b2"]!!).value)
        assertEquals("a10", tree.nodeMap["a10"]!!.lca(tree.nodeMap["a20"]!!).value)
        assertEquals("a5", tree.nodeMap["a5"]!!.lca(tree.nodeMap["a20"]!!).value)
        assertEquals("a", tree.nodeMap["a"]!!.lca(tree.nodeMap["a20"]!!).value)
        assertEquals("a10", tree.nodeMap["a20"]!!.lca(tree.nodeMap["c20"]!!).value)
        assertEquals("a10", tree.nodeMap["a20"]!!.lca(tree.nodeMap["c2"]!!).value)
        assertEquals("a10", tree.nodeMap["a12"]!!.lca(tree.nodeMap["c20"]!!).value)
    }
}
