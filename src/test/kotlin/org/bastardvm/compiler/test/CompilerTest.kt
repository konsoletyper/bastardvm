package org.bastardvm.compiler.test

import org.bastardvm.compiler.model.Type
import org.junit.Test
import kotlin.test.assertEquals

class CompilerTest {
    @Test
    fun loopSupported() {
        val result = CompilerTest.runner.evaluateJS("loop", Type.Int, listOf(Type.Int), listOf(5))
        assertEquals(TestMethods.loop(5), (result as Number).toInt())
    }

    @Test
    fun supportsShortCircuit() {
        assertShortCircuit(6, 6)
        assertShortCircuit(6, 15)
        assertShortCircuit(15, 6)
        assertShortCircuit(16, 16)
        assertShortCircuit(11, 11)
        assertShortCircuit(30, 5)
        assertShortCircuit(5, 30)
    }

    fun assertShortCircuit(a: Int, b: Int) {
        val result = runner.evaluateJS("shortCircuit", Type.Int, listOf(Type.Int, Type.Int), listOf(a, b))
        assertEquals(TestMethods.shortCircuit(a, b), (result as Number).toInt())
    }

    @Test
    fun supportsElseIf() {
        assertElseIf(1, 2)
        assertElseIf(2, 2)
        assertElseIf(60, 70)
        assertElseIf(70, 60)
    }

    fun assertElseIf(a: Int, b: Int) {
        val result = runner.evaluateJS("elseIf", Type.Int, listOf(Type.Int, Type.Int), listOf(a, b))
        assertEquals(TestMethods.elseIf(a, b), (result as Number).toInt())
    }

    @Test
    fun supportsNestedLoop() {
        val result = runner.evaluateJS("nestedLoop", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.nestedLoop(), (result as Number).toInt())
    }

    @Test
    fun supportsAdjacentLoops() {
        val result = runner.evaluateJS("adjacentLoop", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.adjacentLoop(), (result as Number).toInt())
    }

    @Test
    fun supportsLoopWithMultipleBreaks() {
        assertLoopWithMultipleExits(10)
        assertLoopWithMultipleExits(20)
        assertLoopWithMultipleExits(30)
        assertLoopWithMultipleExits(40)
        assertLoopWithMultipleExits(100)
        assertLoopWithMultipleExits(500)
        assertLoopWithMultipleExits(50000)
    }

    fun assertLoopWithMultipleExits(n: Int) {
        val result = runner.evaluateJS("loopWithMultipleExits", Type.Int, listOf(Type.Int), listOf(n))
        assertEquals(TestMethods.loopWithMultipleExits(n), (result as Number).toInt())
    }

    @Test
    fun supportsDoWhileLoop() {
        val result = runner.evaluateJS("doWhileLoop", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.doWhileLoop(), (result as Number).toInt())
    }

    @Test
    fun supportsDoWhileLoopWithContinue() {
        val result = runner.evaluateJS("doWhileLoopWithContinue", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.doWhileLoopWithContinue(), (result as Number).toInt())
    }

    @Test
    fun supportsConsequentLoop() {
        val result = runner.evaluateJS("consequentLoops", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.consequentLoops(), (result as Number).toInt())
    }

    @Test
    fun supportsContinueToOuterLoop() {
        val result = runner.evaluateJS("continueOuterLoop", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.continueOuterLoop(), (result as Number).toInt())
    }

    @Test
    fun supportsBreakOuterLoop() {
        val result = runner.evaluateJS("breakOuterLoop", Type.Int, emptyList(), emptyList())
        assertEquals(TestMethods.breakOuterLoop(), (result as Number).toInt())
    }

    @Test
    fun supportsSwitch() {
        for (i in 0..8) {
            val result = runner.evaluateJS("switchWorks", Type.Int, listOf(Type.Int), listOf(i))
            assertEquals(TestMethods.switchWorks(i), (result as Number).toInt())
        }
    }

    companion object {
        val runner = JSRunner(TestMethods::class.qualifiedName!!)
    }
}
