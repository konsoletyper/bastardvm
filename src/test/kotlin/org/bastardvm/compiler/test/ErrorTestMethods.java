package org.bastardvm.compiler.test;

public class ErrorTestMethods {
    public static int methodNotFound() {
        return "foo;bar".split(";").length;
    }

    public static void invokeDynamicNotSupported() {
        apply(() -> js.Console.println("foo"));
    }

    private static void apply(Function f) {
        f.foo();
    }

    interface Function {
        void foo();
    }

    class ParentNotFound extends Thread {
    }
}
