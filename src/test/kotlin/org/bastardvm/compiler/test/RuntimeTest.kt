package org.bastardvm.compiler.test

import org.bastardvm.compiler.model.Type
import org.junit.Test
import kotlin.test.assertEquals

class RuntimeTest {
    @Test
    fun numericArrayFilledWithZero() {
        val result = runner.evaluateJS("initializeNumericArray", Type.Int, listOf(), listOf())
        assertEquals(RuntimeTestMethods.initializeNumericArray(), (result as Number).toInt())
    }

    @Test
    fun objectArrayFilledWithNull() {
        val result = runner.evaluateJS("initializeObjectArray", Type.Int, listOf(), listOf())
        assertEquals(RuntimeTestMethods.initializeObjectArray(), (result as Number).toInt())
    }

    @Test
    fun compares() {
        val result = runner.evaluateJS("compare", Type.Int, listOf(Type.Float, Type.Float), listOf(10F, 20F))
        assertEquals(RuntimeTestMethods.compare(10F, 20F), (result as Number).toInt())
    }

    @Test
    fun createsMultiArray() {
        var result = runner.evaluateJS("initializeMultiArray", Type.Int, emptyList(), emptyList())
        assertEquals(RuntimeTestMethods.initializeMultiArray(), (result as Number).toInt())

        result = runner.evaluateJS("initializeMultiArrayWithPartialDimensions", Type.Int, emptyList(), emptyList())
        assertEquals(RuntimeTestMethods.initializeMultiArrayWithPartialDimensions(), (result as Number).toInt())

        result = runner.evaluateJS("initializeMultiArrayOfObject", Type.Int, emptyList(), emptyList())
        assertEquals(RuntimeTestMethods.initializeMultiArrayOfObject(), (result as Number).toInt())
    }

    @Test
    fun clonesArray() {
        val result = runner.evaluateJS("cloneArray", Type.Int, emptyList(), emptyList())
        assertEquals(RuntimeTestMethods.cloneArray(), (result as Number).toInt())
    }

    companion object {
        val runner = JSRunner(RuntimeTestMethods::class.qualifiedName!!)
    }
}