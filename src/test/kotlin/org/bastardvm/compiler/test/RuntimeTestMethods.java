package org.bastardvm.compiler.test;

public class RuntimeTestMethods {
    public static int initializeNumericArray() {
        int[] array = new int[10];
        array[5] = 1;
        int zero = 0;
        int nonZero = 0;
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == 0) {
                ++zero;
            } else if (array[i] != 0) {
                ++nonZero;
            }
        }
        return zero * 100 + nonZero;
    }

    public static int initializeObjectArray() {
        Empty[] array = new Empty[10];
        array[5] = new Empty();
        int nullCount = 0;
        int nonNullCount = 0;
        for (Empty elem : array) {
            if (elem == null) {
                ++nullCount;
            } else if (elem != null) {
                ++nonNullCount;
            }
        }
        return nullCount * 100 + nonNullCount;
    }

    public static int compare(float a, float b) {
        String s = a > b ? "foo" : "bar";
        return s.equals("foo") ? 5 : 6;
    }

    public static int initializeMultiArray() {
        int[][][] multiArray = new int[4][3][5];
        multiArray[1][2][3] = 2;
        multiArray[3][2][1] = 1;
        int sum = 0;
        for (int i = 0; i < multiArray.length; ++i) {
            sum += 2;
            for (int j = 0; j < multiArray[i].length; ++j) {
                for (int k = 0; k < multiArray[i][j].length; ++k) {
                    int elem = multiArray[i][j][k];
                    if (elem == 0) {
                        ++sum;
                    } else {
                        sum += elem * 100;
                    }
                }
            }
        }
        return sum;
    }

    public static int initializeMultiArrayWithPartialDimensions() {
        int[][][] multiArray = new int[4][3][];
        multiArray[1][2] = new int[] { 2 };
        multiArray[3][2] = new int[] { 3 };
        int sum = 0;
        for (int i = 0; i < multiArray.length; ++i) {
            for (int j = 0; j < multiArray[i].length; ++j) {
                if (multiArray[i][j] == null) {
                    sum += 5;
                    continue;
                }
                for (int k = 0; k < multiArray[i][j].length; ++k) {
                    int elem = multiArray[i][j][k];
                    if (elem == 0) {
                        ++sum;
                    } else {
                        sum += elem * 100;
                    }
                }
            }
        }
        return sum;
    }

    public static int initializeMultiArrayOfObject() {
        Empty[][][] multiArray = new Empty[4][3][5];
        multiArray[1][2][3] = new Empty();
        multiArray[3][2][1] = new Empty();
        int sum = 0;
        for (int i = 0; i < multiArray.length; ++i) {
            sum += 5;
            for (int j = 0; j < multiArray[i].length; ++j) {
                for (int k = 0; k < multiArray[i][j].length; ++k) {
                    Empty elem = multiArray[i][j][k];
                    if (elem == null) {
                        ++sum;
                    } else {
                        sum += 100;
                    }
                }
            }
        }
        return sum;
    }

    public static int cloneArray() {
        int[] array = { 2, 3, 5, 7, 11 };
        int[] copy = array.clone();
        int sum = array == copy ? 0 : 100;
        for (int i = 0; i < array.length; ++i) {
            sum += array[i] == copy[i] ? 1 : 0;
        }
        return sum;
    }

    static class Empty {
    }
}
