package org.bastardvm.compiler.test

import org.bastardvm.compiler.model.Type
import org.junit.Test
import kotlin.test.assertEquals

class LibraryTest {
    @Test
    fun stringHashCodeWorks() {
        val result = runner.evaluateJS("stringHashCode", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.stringHashCode(), (result as Number).toInt())
    }

    @Test
    fun substringWorks() {
        val result = runner.evaluateJS("substring", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.substring(), (result as Number).toInt())
    }

    @Test
    fun charAtWorks() {
        val result = runner.evaluateJS("charAt", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.charAt(), (result as Number).toInt())
    }

    @Test
    fun stringLengthWorks() {
        val result = runner.evaluateJS("stringLength", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.stringLength(), (result as Number).toInt())
    }

    @Test
    fun stringEqualsWorks() {
        val result = runner.evaluateJS("stringEquals", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.stringEquals(), (result as Number).toInt())
    }

    @Test
    fun stringBuilderWorks() {
        val result = runner.evaluateJS("stringBuilder", Type.Int, listOf(), listOf())
        assertEquals(LibraryTestMethods.stringBuilder(), (result as Number).toInt())
    }

    companion object {
        val runner = JSRunner(LibraryTestMethods::class.qualifiedName!!)
    }
}