package org.bastardvm.compiler.test

import org.bastardvm.compiler.ClassFilters
import org.bastardvm.compiler.Compiler
import org.bastardvm.compiler.bytecode.CompositeClassSource
import org.bastardvm.compiler.bytecode.RemovingClassSource
import org.bastardvm.compiler.bytecode.RenamingClassSource
import org.bastardvm.compiler.diagnostics.ProblemLocation
import org.bastardvm.compiler.diagnostics.Reporter
import org.bastardvm.compiler.diagnostics.SourceLocation
import org.bastardvm.compiler.model.*
import org.bastardvm.compiler.rendering.DefaultNaming
import org.junit.Test
import kotlin.test.assertTrue

class CompilerDiagnosticsTest {
    @Test
    fun methodNotFoundReported() {
        buildScript()
        assertHas(MethodReference("methodNotFound", Type.Int), "Method nof found: "
                + "Ljava/lang/String;.split(Ljava/lang/String;)[Ljava/lang/String;")
    }

    @Test
    fun parentNotFoundReported() {
        buildScript()
        assertHas("\$ParentNotFound", "Can't load parent class: java.lang.Thread")
    }

    @Test
    fun invokeDynamicNotSupportedReported() {
        buildScript()
        assertHas(MethodReference("invokeDynamicNotSupported", Type.Void),
                "InvokeDynamic instruction is not supported yet")
    }

    fun assertHas(method: MethodReference, message: String) {
        assertTrue("Method $method is expected to have message: $message") {
            methodErrors.getOrPut(method) { hashSetOf() }.contains(message)
        }
    }

    fun assertHas(className: String, message: String) {
        assertTrue("Class $className is expected to have message: $message") {
            classErrors.getOrPut(className) { hashSetOf() }.contains(message)
        }
    }

    companion object {
        val methodErrors: MutableMap<MethodReference, MutableSet<String>> = hashMapOf()
        val fieldErrors: MutableMap<FieldReference, MutableSet<String>> = hashMapOf()
        val classErrors: MutableMap<String, MutableSet<String>> = hashMapOf()
        var built = false

        fun buildScript() {
            if (built) {
                return
            }
            built = true
            val source = RenamingClassSource("org.bastardvm.classlib", "T",
                    RemovingClassSource(listOf("java", "js"),
                            CompositeClassSource.fromClasspath()))
            val reporter = object : Reporter {
                override fun error(source: SourceLocation?, problemLocation: ProblemLocation, message: String) {
                    report(problemLocation, message)
                }
                override fun warn(source: SourceLocation?, problemLocation: ProblemLocation, message: String) {
                    report(problemLocation, message)
                }
            }
            val naming = DefaultNaming()
            val compiler = Compiler(
                    source = source,
                    nameFilters = listOf(
                            ClassFilters.includeWildcard("${ErrorTestMethods::class.qualifiedName}*"),
                            ClassFilters.includeWildcard("java.**"),
                            ClassFilters.includeWildcard("js.**"),
                            ClassFilters.includeWildcard("org.bastardvm.platform.**")),
                    filters = listOf({ _ -> true }),
                    model = BytecodeModel(source, reporter),
                    naming = naming,
                    reporter = reporter)
            val buffer = StringBuilder()
            compiler.compile(buffer)
        }

        fun report(location: ProblemLocation, message: String) {
            when (location) {
                is ProblemLocation.Method -> methodErrors.getOrPut(location.method.nameWithSignature,
                        { hashSetOf() }).add(message)
                is ProblemLocation.Field -> fieldErrors.getOrPut(location.field.nameAndType,
                        { hashSetOf() }).add(message)
                is ProblemLocation.Class -> classErrors.getOrPut(
                        location.className.removePrefix(ErrorTestMethods::class.qualifiedName!!), { hashSetOf() })
                        .add(message)
            }
        }
    }
}