package org.bastardvm.compiler.test

import org.bastardvm.compiler.ClassFilters
import org.bastardvm.compiler.Compiler
import org.bastardvm.compiler.bytecode.CompositeClassSource
import org.bastardvm.compiler.bytecode.RemovingClassSource
import org.bastardvm.compiler.bytecode.RenamingClassSource
import org.bastardvm.compiler.diagnostics.WritingReporter
import org.bastardvm.compiler.model.BytecodeModel
import org.bastardvm.compiler.model.FullMethodReference
import org.bastardvm.compiler.model.Type
import org.bastardvm.compiler.rendering.DefaultNaming
import org.mozilla.javascript.Context
import org.mozilla.javascript.Function
import java.io.StringWriter

class JSRunner(val testClass: String) {
    val naming = DefaultNaming()
    var scriptInitialized = false
    private var scriptImpl: String = ""
    val script by lazy {
        if (!scriptInitialized) {
            scriptInitialized = true
            scriptImpl = buildScript()
            System.out.println(scriptImpl)
        }
        scriptImpl
    }

    fun evaluateJS(methodName: String, resultType: Type, parameterTypes: List<Type>, parameters: List<Any?>): Any? {
        val cx = Context.enter()
        try {
            val scope = cx.initStandardObjects()
            cx.evaluateString(scope, script, "bastardvm.js", 1, null)
            val methodRef = FullMethodReference(Type.Obj(testClass), methodName, resultType,
                    *parameterTypes.toTypedArray())
            val function = scope.get(naming.getNameFor(methodRef)) as Function
            return function.call(cx, scope, scope, parameters.toTypedArray())
        } finally {
            Context.exit()
        }
    }

    private fun buildScript(): String {
        val source = RenamingClassSource("org.bastardvm.classlib", "T",
                RemovingClassSource(listOf("java", "js"),
                        CompositeClassSource.fromClasspath()))
        val reporter = WritingReporter(StringWriter())
        val compiler = Compiler(
                source = source,
                nameFilters = listOf(
                        ClassFilters.includeWildcard("$testClass*"),
                        ClassFilters.includeWildcard("java.**"),
                        ClassFilters.includeWildcard("js.**"),
                        ClassFilters.includeWildcard("org.bastardvm.platform.**")),
                filters = listOf({ _ -> true }),
                model = BytecodeModel(source, reporter),
                naming = naming,
                reporter = reporter)
        val buffer = StringBuilder()
        compiler.compile(buffer)
        return buffer.toString()
    }
}