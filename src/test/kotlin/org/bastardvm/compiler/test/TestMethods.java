package org.bastardvm.compiler.test;

public class TestMethods {
    public static int loop(int n) {
        n += 5;
        for (int i = 0; i < 10; ++i) {
            ++n;
        }
        return n;
    }

    public static int shortCircuit(int a, int b) {
        int c;
        if (a > 10 && b > 10 && a + b > 30) {
            c = 1;
        } else {
            c = 0;
        }
        return c;
    }

    public static int elseIf(int a, int b) {
        int c = 0;
        if (a > b) {
            c = 1;
        } else if (a > 50) {
            c = 2;
        }
        return c;
    }

    public static int nestedLoop() {
        int n = 0;
        for (int i = 0; i < 6; ++i) {
            n += 4;
            for (int j = 1; j < 3; ++j) {
                n += j;
                for (int k = 0; k < 4; ++k) {
                    n++;
                }
            }
            n *= 2;
        }
        return n;
    }

    public static int adjacentLoop() {
        int n = 0;
        for (int i = 0; i < 10; ++i) {
            n += 4;
            if (i < 5) {
                n += 2;
            } else {
                n += 3;
            }
        }
        return n;
    }

    public static int loopWithMultipleExits(int max) {
        int n = 0;
        for (int i = 0; i < 10; ++i) {
            n += 4;
            if (n > max) {
                n *= 3;
                break;
            }
            n += 2;
            if (n > max) {
                return n;
            }
            n += 1;
        }
        n /= 2;
        return n;
    }

    public static int doWhileLoop() {
        int n = 0;
        do {
            n += 4;
        } while (n < 30);
        return n;
    }

    public static int doWhileLoopWithContinue() {
        int n = 0;
        do {
            do {
                n += 3;
            } while (n < 20);
            n += 3;
        } while (n < 60);
        return n;
    }

    public static int consequentLoops() {
        int n = 0;
        int i;
        for (i = 0; i < 5; ++i) {
            n += 3;
        }
        for (; i < 9; ++i) {
            n += 10;
        }
        return n;
    }

    public static int continueOuterLoop() {
        int n = 0;
        int i = 0;
        outer: while (i++ < 5) {
            n = n + 5;
            for (int j = 0; j < 5; ++j) {
                n = n + 1;
                if (j > i || n > 20) {
                    continue outer;
                }
                n = n + 1;
            }
        }
        return n;
    }

    public static int breakOuterLoop() {
        int n = 0;
        int i = 0;
        outer: while (i++ < 5) {
            n = n + 5;
            for (int j = 0; j < 5; ++j) {
                n = n + 1;
                if (j > i || n > 20) {
                    break outer;
                }
                n = n + 1;
            }
        }
        return n;
    }

    public static int switchWorks(int n) {
        int k = 1;
        switch (n) {
            case 0: k = 2; break;
            case 1: k = 3; break;
            case 2: k = 4; break;
            case 3: return 5;
            case 4:
            case 5: k = 7; break;
            case 6: k = 8;
            case 7: k *= 2; break;
            default: k = 10; break;
        }
        return k;
    }
}
