package org.bastardvm.compiler.test;

public class LibraryTestMethods {
    public static int stringHashCode() {
        return string().hashCode();
    }

    public static int substring() {
        return string().substring(3, 6).hashCode();
    }

    public static int charAt() {
        return string().charAt(0) + string().charAt(1) * string().charAt(4);
    }

    public static int stringLength() {
        return string().length();
    }

    public static int stringEquals() {
        int n = 0;
        n = 10 * n + (string().equals("string") ? 1 : 0);
        n = 10 * n + (string().equals(string()) ? 1 : 0);
        n = 10 * n + (string().equals(null) ? 1 : 0);
        n = 10 * n + (string().equals("foobarbaz") ? 1 : 0);
        return n;
    }

    public static int stringBuilder() {
        return (string() + ":" + number() + '.').hashCode();
    }

    private static String string() {
        return "foobarbaz";
    }

    private static int number() {
        return 23;
    }
}
