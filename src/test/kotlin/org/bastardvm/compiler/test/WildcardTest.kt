package org.bastardvm.compiler.test

import org.bastardvm.compiler.ClassFilters
import kotlin.test.*
import org.junit.Test

class WildcardTest {
    @Test
    fun exactWildcard() {
        assertTrue { ClassFilters.matchWildcard("abc", "abc") }
        assertFalse { ClassFilters.matchWildcard("abc", "ab") }
        assertFalse { ClassFilters.matchWildcard("abc", "abd") }
        assertFalse { ClassFilters.matchWildcard("abc", "abcd") }
    }

    @Test
    fun wildcardWithQuestionMark() {
        assertTrue { ClassFilters.matchWildcard("a?c", "abc") }
        assertTrue { ClassFilters.matchWildcard("a?c", "aac") }
        assertTrue { ClassFilters.matchWildcard("a?c", "acc") }
        assertFalse { ClassFilters.matchWildcard("a?c", "acb") }
        assertFalse { ClassFilters.matchWildcard("a?c", "a") }
        assertFalse { ClassFilters.matchWildcard("a?c", "aa") }
        assertFalse { ClassFilters.matchWildcard("a?c", "bbb") }

        assertTrue { ClassFilters.matchWildcard("a??c", "abbc") }
        assertTrue { ClassFilters.matchWildcard("a??c", "aaac") }
        assertTrue { ClassFilters.matchWildcard("a??c", "acac") }
    }

    @Test
    fun wildcardWithStar() {
        assertTrue { ClassFilters.matchWildcard("a*c", "ac") }
        assertTrue { ClassFilters.matchWildcard("a*c", "aaaac") }
        assertTrue { ClassFilters.matchWildcard("a*c", "abbbc") }
        assertFalse { ClassFilters.matchWildcard("a*c", "ab") }
        assertFalse { ClassFilters.matchWildcard("a*c", "a.c") }
        assertFalse { ClassFilters.matchWildcard("a*c", "aaa.c") }
    }

    @Test
    fun wildcardWithDoubleStar() {
        assertTrue { ClassFilters.matchWildcard("a**c", "ac") }
        assertTrue { ClassFilters.matchWildcard("a**c", "aaaac") }
        assertTrue { ClassFilters.matchWildcard("a**c", "abbbc") }
        assertFalse { ClassFilters.matchWildcard("a**c", "ab") }
        assertTrue { ClassFilters.matchWildcard("a**c", "a.c") }
        assertTrue { ClassFilters.matchWildcard("a**c", "a.b.c") }
        assertTrue { ClassFilters.matchWildcard("ab**", "abccc") }
        assertTrue { ClassFilters.matchWildcard("ab**", "ab") }
    }
}
