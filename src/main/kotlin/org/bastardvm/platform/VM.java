package org.bastardvm.platform;

import org.bastardvm.classlib.java.lang.TString;
import org.bastardvm.ffi.JSBody;
import org.bastardvm.ffi.Omitted;

public class VM {
    private static StringPool stringPool = createStringPool();
    private static Object booleanType = createType();
    private static Object byteType = createType();
    private static Object shortType = createType();
    private static Object charType = createType();
    private static Object intType = createType();
    private static Object longType = createType();
    private static Object floatType = createType();
    private static Object doubleType = createType();
    private static Object voidType = createType();

    private VM() {
    }

    public static NativeArray createNumericArray(int size) {
        return new NativeArray(createNumericArrayData(size));
    }

    public static NativeArray createObjectArray(int size) {
        return new NativeArray(createObjectArrayData(size));
    }

    public static Object createMultiArray(Object dimensions, Object defaultValue) {
        return createMultiArray(dimensions, defaultValue, 0);
    }

    private static Object createMultiArray(Object dimensions, Object defaultValue, int index) {
        int dimensionsLength = length(dimensions);
        int sz = intItem(dimensions, index);
        NativeArray array = new NativeArray(createEmptyArrayData(sz));
        ++index;
        if (index == dimensionsLength) {
            for (int i = 0; i < sz; ++i) {
                setItem(array.data, i, defaultValue);
            }
        } else {
            for (int i = 0; i < sz; ++i) {
                setItem(array.data, i, createMultiArray(dimensions, defaultValue, index));
            }
        }
        return array;
    }

    public static NativeArray cloneArray(NativeArray array) {
        int sz = length(array.data);
        Object copyData = createEmptyArrayData(sz);
        for (int i = 0; i < sz; ++i) {
            setItem(copyData, i, item(array.data, i));
        }
        return new NativeArray(copyData);
    }

    @JSBody(parameters = "size", script = ""
            + "var array = new Array(size);"
            + "for (var i = 0; i < size; ++i) {"
                + "array[i] = 0;"
            + "}"
            + "return array;")
    private static native Object createNumericArrayData(int size);

    @JSBody(parameters = "size", script = ""
            + "var array = new Array(size);"
            + "for (var i = 0; i < size; ++i) {"
                + "array[i] = null;"
            + "}"
            + "return array;")
    private static native Object createObjectArrayData(int size);

    @JSBody(parameters = "size", script = "return new Array(size);")
    private static native Object createEmptyArrayData(int size);

    public static TString string(TString.NativeString nativeString) {
        if (existsInPool(stringPool, nativeString)) {
            return getFromPool(stringPool, nativeString);
        } else {
            TString string = new TString(nativeString);
            putToPool(stringPool, nativeString, string);
            return string;
        }
    }

    public static boolean instanceOf(Object object, Object type) {
        if (object == null) {
            return false;
        }
        Object constructor = constructor(object);
        return isAssignable(constructor, type);
    }

    private static boolean isAssignable(Object from, Object to) {
        if (from == to) {
            return true;
        }
        Object superclass = superclass(from);
        if (!isUndefined(superclass) && isAssignable(superclass, to)) {
            return true;
        }
        Object interfaces = interfaces(from);
        int interfacesLength = length(interfaces);
        for (int i = 0; i < interfacesLength; ++i) {
            if (isAssignable(item(interfaces, i), to)) {
                return true;
            }
        }
        return false;
    }

    @JSBody(parameters = "type", script = ""
            + "if (!('array' in type)) {"
                + "type.array = new Object();"
            + "}"
            + "return type.array;")
    public static native Object arrayType(Object type);

    public static Object booleanType() {
        return booleanType;
    }

    public static Object byteType() {
        return byteType;
    }

    public static Object shortType() {
        return shortType;
    }

    public static Object charType() {
        return charType;
    }

    public static Object intType() {
        return intType;
    }

    public static Object longType() {
        return longType;
    }

    public static Object floatType() {
        return floatType;
    }

    public static Object doubleType() {
        return doubleType;
    }

    public static Object voidType() {
        return voidType;
    }

    @JSBody(script = "return {};")
    private static native Object createType();

    @JSBody(parameters = "obj", script = "return obj.constructor;")
    private static native Object constructor(Object object);

    @JSBody(parameters = "cls", script = "return cls.prototype;")
    private static native Object superclass(Object cls);

    @JSBody(parameters = "cls", script = "return cls.interfaces;")
    private static native Object interfaces(Object cls);

    @JSBody(parameters = "array", script = "return array.length;")
    private static native int length(Object array);

    @JSBody(parameters = { "array", "index" }, script = "return array[index];")
    private static native Object item(Object array, int index);

    @JSBody(parameters = { "array", "index" }, script = "return array[index];")
    private static native int intItem(Object array, int index);

    @JSBody(parameters = { "array", "index", "value" }, script = "array[index] = value;")
    private static native void setItem(Object array, int index, Object value);

    @JSBody(parameters = "obj", script = "return typeof(obj) == 'undefined';")
    private static native boolean isUndefined(Object obj);

    @JSBody(parameters = { "pool", "string" }, script = "return pool[string];")
    private static native TString getFromPool(StringPool pool, TString.NativeString string);

    @JSBody(parameters = { "pool", "string" }, script = "return string in pool ? 1 : 0;")
    private static native boolean existsInPool(StringPool pool, TString.NativeString string);

    @JSBody(parameters = { "pool", "key", "string" }, script = "pool[key] = string;")
    private static native void putToPool(StringPool pool, TString.NativeString key, TString string);

    @JSBody(script = "return {};")
    private static native StringPool createStringPool();

    @JSBody(parameters = { "a", "b" }, script = "return a > b ? 1 : a < b ? -1 : 0;")
    public static native int compare(JSObject a, JSObject b);

    @JSBody(parameters = "value", script = "return value;")
    public static native Object cast(Object value);

    public static class NativeArray {
        public final Object data;

        public NativeArray(Object data) {
            this.data = data;
        }
    }

    @Omitted
    public static class StringPool {
        private StringPool() {
        }
    }

    @Omitted
    public static class JSObject {
        private JSObject() {
        }
    }
}
