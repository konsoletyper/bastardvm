package org.bastardvm.compiler.model

data class FullMethodReference(val target : Type, val nameWithSignature : MethodReference) {
    constructor(target: Type, name: String, result: Type, vararg parameters: Type)
    : this(target, MethodReference(name, MethodSignature(result, *parameters)))

    constructor(target: Type, name: String, signature: MethodSignature)
    : this(target, MethodReference(name, signature))

    val name: String
        get() = nameWithSignature.name

    val signature: MethodSignature
        get() = nameWithSignature.signature

    val result: Type?
        get() = signature.result

    val parameters: List<Type>
        get() = signature.parameters

    override fun toString(): String = target.toString() + "." + nameWithSignature
}
