package org.bastardvm.compiler.model

class ModelException(message: String) : Exception(message)