package org.bastardvm.compiler.model

data class MethodReference(val name: String, val signature: MethodSignature) {
    constructor(name: String, result: Type, vararg parameters: Type) :
            this(name, MethodSignature(result, *parameters))

    val result: Type
        get() = signature.result

    val parameters: List<Type>
        get() = signature.parameters

    override fun toString(): String = name + signature
}