package org.bastardvm.compiler.model

interface AnnotatedModel {
    val annotations: Map<String, AnnotationModel>
}
