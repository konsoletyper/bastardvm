package org.bastardvm.compiler.model

import org.bastardvm.compiler.bytecode.ClassSource
import org.bastardvm.compiler.diagnostics.ProblemLocation
import org.bastardvm.compiler.diagnostics.Reporter
import org.objectweb.asm.*
import java.io.InputStream

class BytecodeModel(val source: ClassSource, val reporter: Reporter) : Model {
    private val cache = hashMapOf<String, ClassModelImpl?>()

    override fun get(name: String): ClassModel? = cache.getOrPut(name) {
        source.getBytecode(name)?.use {
            ClassModelImpl(name, it)
        }
    }

    override fun getClassNames() = source.getClasses()

    internal inner class ClassModelImpl(override val name: String, data: InputStream): ClassModel {
        val parentName: String?
        val interfaceNames: List<String>
        override val memberMethods: Map<MethodReference, MethodModelImpl>
        override val memberFields: Map<FieldReference, FieldModelImpl>
        override val annotations: Map<String, AnnotationModel>

        init {
            val visitor = ClassModelVisitor(this)
            val reader = ClassReader(data)
            reader.accept(visitor, ClassReader.SKIP_CODE or ClassReader.SKIP_DEBUG or ClassReader.SKIP_FRAMES)
            interfaceNames = visitor.interfaces!!
            parentName = if (visitor.parent != name) visitor.parent else null
            memberMethods = visitor.methods
            memberFields = visitor.fields
            annotations = visitor.annotationTarget.annotations
        }

        override val parent by lazy {
            parentName?.let {
                val result = get(it)
                if (result == null) {
                    reporter.error(null, ProblemLocation.Class(name), "Can't load parent class: $it")
                }
                result
            }
        }

        override val interfaces by lazy {
            interfaceNames.map {
                val result = get(it)
                if (result == null) {
                    reporter.error(null, ProblemLocation.Class(name), "Can't load interface: $it")
                }
                result
            }.filterNotNull().toSet()
        }
    }

    internal class MethodModelImpl(
            override val owner: ClassModelImpl,
            override val reference: MethodReference,
            override val static: Boolean,
            override val abstractModifier: Boolean,
            override val nativeModifier: Boolean,
            override val annotations: Map<String, AnnotationModel>)
    : MethodModel

    internal class FieldModelImpl(
            override val owner: ClassModel,
            override val reference: FieldReference,
            override val static: Boolean,
            override val annotations: Map<String, AnnotationModel>)
    : FieldModel

    internal class AnnotationModelImpl(override val className: String) : AnnotationModel {
        override val fields = hashMapOf<String, AnnotationValue>()
    }

    internal interface AnnotationTargetVisitor {
        fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor
    }

    internal class ClassModelVisitor(val cls: ClassModelImpl,
            val annotationTarget: AnnotationTargetModelVisitor = AnnotationTargetModelVisitor())
    : ClassVisitor(Opcodes.ASM5), AnnotationTargetVisitor by annotationTarget {
        var parent : String? = null
        var interfaces: List<String>? = null
        val methods = hashMapOf<MethodReference, MethodModelImpl>()
        val fields = hashMapOf<FieldReference, FieldModelImpl>()
        val typeParser = TypeParser()

        override fun visit(version: Int, access: Int, name: String, signature: String?, superName: String?,
                           interfaces: Array<String>) {
            parent = superName?.replace('/', '.')
            this.interfaces = interfaces.map { it.replace('/', '.') }.toList()
        }

        override fun visitMethod(access: Int, name: String, desc: String, signature: String?,
                                 exceptions: Array<out String>?): MethodVisitor? {
            val reference = MethodReference(name, typeParser.reset().parseSignature(desc))
            if (typeParser.lastIndex < desc.length) {
                throw ModelException("Wrong method descriptor: $desc")
            }
            val visitor = MethodModelVisitor()
            methods.put(reference, MethodModelImpl(
                    owner = cls,
                    reference = reference,
                    static = access and Opcodes.ACC_STATIC != 0,
                    abstractModifier = access and Opcodes.ACC_ABSTRACT != 0,
                    nativeModifier = access and Opcodes.ACC_NATIVE != 0,
                    annotations = visitor.annotationTarget.annotations))
            return visitor
        }

        override fun visitField(access: Int, name: String, desc: String, signature: String?,
                                value: Any?): FieldVisitor? {
            val reference = FieldReference(name, typeParser.reset().parse(desc))
            if (typeParser.lastIndex < desc.length) {
                throw ModelException("Wrong field descriptor: $desc")
            }
            val visitor = FieldModelVisitor()
            fields.put(reference, FieldModelImpl(
                    owner = cls,
                    reference = reference,
                    static = access and Opcodes.ACC_STATIC != 0,
                    annotations = visitor.annotationTarget.annotations))
            return visitor
        }

        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor {
            return annotationTarget.visitAnnotation(desc, visible)
        }
    }

    internal class FieldModelVisitor(
            val annotationTarget: AnnotationTargetModelVisitor = AnnotationTargetModelVisitor())
    : FieldVisitor(Opcodes.ASM5), AnnotationTargetVisitor by annotationTarget {
        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor {
            return annotationTarget.visitAnnotation(desc, visible)
        }
    }

    internal class MethodModelVisitor(
            val annotationTarget: AnnotationTargetModelVisitor = AnnotationTargetModelVisitor())
    : MethodVisitor(Opcodes.ASM5), AnnotationTargetVisitor by annotationTarget {
        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor {
            return annotationTarget.visitAnnotation(desc, visible)
        }
    }

    internal class AnnotationTargetModelVisitor : AnnotationTargetVisitor {
        val annotations = hashMapOf<String, AnnotationModelImpl>()

        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor {
            val annotClassName = parseDescAsObject(desc)
            val result = AnnotationModelImpl(annotClassName)
            annotations.put(annotClassName, result)
            return AnnotationModelVisitor(result)
        }
    }

    internal abstract class AbstractAnnotationModelVisitor : AnnotationVisitor(Opcodes.ASM5) {
        override fun visit(name: String?, value: Any?) {
            put(name, when (value) {
                is Boolean -> AnnotationValue.Boolean(value)
                is Byte -> AnnotationValue.Byte(value)
                is Short -> AnnotationValue.Short(value)
                is Char -> AnnotationValue.Char(value)
                is Int -> AnnotationValue.Int(value)
                is Long -> AnnotationValue.Long(value)
                is Float -> AnnotationValue.Float(value)
                is Double -> AnnotationValue.Double(value)
                is String -> AnnotationValue.String(value)
                is org.objectweb.asm.Type -> AnnotationValue.Type(TypeParser().parse(value.descriptor))
                else -> throw ModelException("Invalid annotation value $value")
            })
        }

        override fun visitEnum(name: String, desc: String, value: String) {
            put(name, AnnotationValue.Enum(parseDescAsObject(desc), value))
        }

        override fun visitArray(name: String): AnnotationVisitor {
            val list = arrayListOf<AnnotationValue>()
            put(name, AnnotationValue.Array(list))
            return AnnotationArrayVisitor(list)
        }

        override fun visitAnnotation(name: String, desc: String): AnnotationVisitor {
            val annot = AnnotationModelImpl(parseDescAsObject(desc))
            put(name, AnnotationValue.Annotation(annot))
            return AnnotationModelVisitor(annot)
        }

        abstract fun put(name: String?, value: AnnotationValue)
    }

    internal class AnnotationModelVisitor(val target: AnnotationModelImpl) : AbstractAnnotationModelVisitor() {
        override fun put(name: String?, value: AnnotationValue) {
            target.fields.put(name!!, value)
        }
    }

    internal class AnnotationArrayVisitor(val target: MutableList<AnnotationValue>) : AbstractAnnotationModelVisitor() {
        override fun put(name: String?, value: AnnotationValue) {
            target.add(value)
        }
    }

    internal companion object {
        internal fun parseDescAsObject(desc: String): String {
            val type = TypeParser().parse(desc)
            return when (type) {
                is Type.Obj -> type.className
                else -> throw ModelException("Wrong annotation type $desc")
            }
        }
    }
}
