package org.bastardvm.compiler.model

class TypeParser {
    var lastIndex : Int = 0;

    fun reset() : TypeParser {
        lastIndex = 0
        return this
    }

    fun parse(str: String, index: Int = lastIndex): Type {
        lastIndex = index + 1
        return when (str.elementAtOrElse(index, { '\n' })) {
            'Z' -> Type.Boolean
            'B' -> Type.Byte
            'S' -> Type.Short
            'C' -> Type.Char
            'I' -> Type.Int
            'J' -> Type.Long
            'F' -> Type.Float
            'D' -> Type.Double
            'V' -> Type.Void
            'L' -> {
                val end = str.indexOf(';', index);
                if (end < 0) {
                    throw TypeParseException(str, index);
                }
                lastIndex = end + 1;
                Type.Obj(str.substring(index + 1, end).replace('/', '.'))
            }
            '[' -> {
                Type.Array(parse(str, index + 1))
            }
            else -> {
                lastIndex = index
                throw TypeParseException(str, index)
            }
        }
    }

    fun parseClass(str: String, index: Int = lastIndex): Type {
        return when (str.elementAtOrElse(index, { '\n' })) {
            '[' -> Type.Array(parse(str, index + 1))
            else -> Type.Obj(str.replace('/', '.'))
        }
    }

    fun parseSignature(str: String, index: Int = lastIndex): MethodSignature {
        var current = index;
        if (str.elementAtOrElse(current++, { '\n' }) != '(') {
            throw TypeParseException(str, current);
        }
        val parameters = arrayListOf<Type>()
        while (str.elementAtOrElse(current, { '\n' }) != ')') {
            parameters.add(parse(str, current))
            current = lastIndex
        }
        ++current;
        var result = parse(str, current);
        return MethodSignature(result, parameters)
    }
}