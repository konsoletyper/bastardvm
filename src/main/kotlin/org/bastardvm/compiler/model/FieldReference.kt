package org.bastardvm.compiler.model

data class FieldReference(val name: String, val type: Type)
