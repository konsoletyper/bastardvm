package org.bastardvm.compiler.model

import java.util.*

class MethodSignature(val result: Type, parameters: List<Type>) {
    val parameters : List<Type> = parameters.toList()

    constructor(result: Type, vararg parameters: Type) : this(result, arrayListOf(*parameters))

    override fun toString(): String = "(" + parameters.map { it.toString() }.joinToString("") + ")$result"

    override fun hashCode() = Objects.hash(result, Arrays.hashCode(parameters.toTypedArray()))

    override fun equals(other: Any?): Boolean = when (other) {
        is MethodSignature -> result == other.result && parameters.size == other.parameters.size &&
                parameters.zip(other.parameters).all { it.first == it.second }
        else -> false
    }
}