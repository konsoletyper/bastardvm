package org.bastardvm.compiler.model

interface ClassModel : AnnotatedModel {
    val name: String

    val parent: ClassModel?

    val interfaces: Set<ClassModel>

    val memberMethods: Map<MethodReference, MethodModel>

    val memberFields: Map<FieldReference, FieldModel>

    fun memberMethod(name: String, result: Type, vararg parameters: Type) {
        memberMethods[MethodReference(name, result, *parameters)]
    }

    fun isAssignableTo(supertypeCandidate: ClassModel): Boolean = name == supertypeCandidate.name
            || (sequenceOf(parent) + interfaces).filterNotNull().any { it.isAssignableTo(supertypeCandidate) }

    fun findMethod(method: MethodReference): MethodModel? = memberMethods[method]
            ?: parent?.findMethod(method)
            ?: interfaces.map { it.findMethod(method) }.firstOrNull()

    fun findField(field: FieldReference): FieldModel? = memberFields[field] ?: parent?.findField(field)
}
