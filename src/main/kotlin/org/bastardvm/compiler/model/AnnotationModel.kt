package org.bastardvm.compiler.model

interface AnnotationModel {
    val className: String

    val fields: Map<String, AnnotationValue>
}
