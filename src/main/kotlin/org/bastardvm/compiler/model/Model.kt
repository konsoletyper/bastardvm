package org.bastardvm.compiler.model

interface Model {
    operator fun get(name: String): ClassModel?

    fun getClassNames(): Set<String>
}