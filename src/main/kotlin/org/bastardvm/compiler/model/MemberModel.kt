package org.bastardvm.compiler.model

interface MemberModel : AnnotatedModel {
    val static: Boolean

    val owner: ClassModel
}
