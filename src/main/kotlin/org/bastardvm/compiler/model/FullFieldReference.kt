package org.bastardvm.compiler.model

data class FullFieldReference(val className: String, val nameAndType: FieldReference) {
    constructor(className: String, name: String, type: Type) : this(className, FieldReference(name, type))

    val name: String
        get() = nameAndType.name

    val type: Type
        get() = nameAndType.type
}
