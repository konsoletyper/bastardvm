package org.bastardvm.compiler.model

sealed class AnnotationValue {
    class Boolean(val value: kotlin.Boolean) : AnnotationValue()
    class Byte(val value: kotlin.Byte) : AnnotationValue()
    class Short(val value: kotlin.Short) : AnnotationValue()
    class Char(val value: kotlin.Char) : AnnotationValue()
    class Int(val value: kotlin.Int) : AnnotationValue()
    class Long(val value: kotlin.Long) : AnnotationValue()
    class Float(val value: kotlin.Float) : AnnotationValue()
    class Double(val value: kotlin.Double) : AnnotationValue()
    class String(val value: kotlin.String) : AnnotationValue()
    class Type(val value: org.bastardvm.compiler.model.Type) : AnnotationValue()
    class Array(val value: List<AnnotationValue>) : AnnotationValue()
    class Annotation(val value: AnnotationModel) : AnnotationValue()
    class Enum(val className: kotlin.String, val fieldName: kotlin.String) : AnnotationValue()
}