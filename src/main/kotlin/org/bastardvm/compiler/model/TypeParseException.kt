package org.bastardvm.compiler.model

class TypeParseException(str: String, val index: Int)
        : Exception("Error parsing type from string $str at $index")
