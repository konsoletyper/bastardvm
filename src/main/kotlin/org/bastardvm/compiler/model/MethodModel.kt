package org.bastardvm.compiler.model

interface MethodModel : MemberModel {
    val reference: MethodReference

    val abstractModifier: Boolean

    val nativeModifier: Boolean
}
