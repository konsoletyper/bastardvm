package org.bastardvm.compiler.model

sealed class Type {
    object Boolean : Type() {
        override fun toString() = "Z"
    }
    object Byte : Type() {
        override fun toString() = "B"
    }
    object Char : Type() {
        override fun toString() = "C"
    }
    object Short : Type() {
        override fun toString() = "S"
    }
    object Int : Type() {
        override fun toString() = "I"
    }
    object Long : Type() {
        override fun toString() = "J"
    }
    object Float : Type() {
        override fun toString() = "F"
    }
    object Double : Type() {
        override fun toString() = "D"
    }
    object Void : Type() {
        override fun toString() = "V"
    }

    class Obj(val className: String) : Type() {
        override fun toString() = "L" + className.replace('.', '/') + ";"

        override fun equals(other: Any?) = when (other) {
            is Obj -> other.className == className
            else -> false
        }

        override fun hashCode() = className.hashCode() * 31
    }

    class Array(val elementType: Type) : Type() {
        override fun toString() = "[" + elementType.toString()

        override fun equals(other: Any?) = when (other) {
            is Array -> elementType == other.elementType
            else -> false
        }

        override fun hashCode() = elementType.hashCode() * 17
    }
}

