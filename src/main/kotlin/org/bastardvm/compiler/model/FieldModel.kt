package org.bastardvm.compiler.model

interface FieldModel : MemberModel {
    val reference: FieldReference
}