package org.bastardvm.compiler.parsing

import org.bastardvm.compiler.ast.*
import org.bastardvm.compiler.ast.ArithmeticBinaryOperation.*
import org.bastardvm.compiler.ast.BinaryOperation.*
import org.bastardvm.compiler.ast.BitwiseBinaryOperation.*
import org.bastardvm.compiler.ast.Expression.*
import org.bastardvm.compiler.bytecode.Procedure
import org.bastardvm.compiler.diagnostics.ProblemLocation
import org.bastardvm.compiler.diagnostics.Reporter
import org.bastardvm.compiler.diagnostics.SourceLocation
import org.bastardvm.compiler.model.*
import org.objectweb.asm.Handle
import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.LineNumberNode
import org.objectweb.asm.tree.MethodNode
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceMethodVisitor
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

class BytecodeParser(val reporter: Reporter) {
    private var problemLocation: ProblemLocation? = null

    fun parse(sourceFile: String?, owner: String, method: MethodNode): Procedure {
        val insnParser = InstructionParser(sourceFile, method)
        val ref = FullMethodReference(Type.Obj(owner), method.name, TypeParser().parseSignature(method.desc))
        problemLocation = ProblemLocation.Method(ref)
        if (!insnParser.instructions.isEmpty()) {
            insnParser.parse()
        }
        val parameterCount = org.objectweb.asm.Type.getArgumentTypes(method.desc).size
        val parameterNames = 0.until(parameterCount).map {
            insnParser.localNameAt(it + 1 - insnParser.minLocal, 0)
        }
        return Procedure(parameterNames, insnParser.blocks.firstOrNull())
    }

    internal class LocalVariable(val index: Int, var name: String? = null)

    internal inner class InstructionParser(internal val sourceFile: String?,
            internal val method: MethodNode) : MethodVisitor(Opcodes.ASM5) {
        val instructions = method.instructions?.toArray()?.toMutableList() ?: emptyList<AbstractInsnNode>()
        val labelIndexes = instructions
                .mapIndexed { i, r -> if (r is LabelNode) Pair(i, r.label) else null }
                .filter { it != null }
                .associate { it!!.second to it.first }
        val blocks = if (instructions.isNotEmpty()) BlockStartFinder(instructions, labelIndexes).blocks
                else emptyList<Block>()
        val stackBefore = arrayOfNulls<StackFrame?>(instructions.size).toMutableList()
        val stackAfter = arrayOfNulls<StackFrame?>(instructions.size).toMutableList()
        var stack: StackFrame = StackFrame(0)
        var currentIndex = 0
        var currentBlock = blocks[0]!!
        val nextIndexes = arrayOfNulls<IntArray?>(instructions.size).toMutableList()
        val minLocal = if (method.access and Opcodes.ACC_STATIC != 0) 1 else 0
        val localNames = arrayListOf<MutableList<LocalVariable>>()
        val lineNumbers: List<Pair<Int, Int>>
        var currentLine: Int? = null

        init {
            prepareParameterNames()
            prepareVariableNames()
            lineNumbers = if (sourceFile != null) prepareLineNumbers() else emptyList()
        }

        fun parse() {
            try {
                class Step(val from: Int, val to: Int)

                val worklist = ArrayDeque<Step>()
                worklist.push(Step(-1, 0))
                currentLine = null
                var lineNumberIndex = 0
                while (!worklist.isEmpty()) {
                    val step = worklist.pop()
                    if (stackBefore[step.to] != null) {
                        continue
                    }
                    if (step.from >= 0) {
                        stack = stackAfter[step.from]!!
                    }
                    stackBefore[step.to] = stack
                    currentIndex = step.to
                    currentBlock = blocks[step.to] ?: currentBlock
                    nextIndexes[step.to] = intArrayOf(step.to + 1)
                    val nextLineNumber = lineNumbers.getOrNull(lineNumberIndex)
                    if (nextLineNumber != null && nextLineNumber.first == step.to) {
                        ++lineNumberIndex
                        currentLine = nextLineNumber.second
                    }
                    instructions[step.to].accept(this)
                    stackAfter[step.to] = stack
                    if (nextIndexes[step.to]!!.size == 1 && nextIndexes[step.to]!![0] == step.to + 1) {
                        val nextBlock = blocks[step.to + 1]
                        if (nextBlock != null && currentBlock.transition is Transition.Empty) {
                            currentBlock.transition = Transition.Jump(nextBlock)
                        }
                    }
                    if (step.to + 1 >= blocks.size || blocks[step.to + 1] != null) {
                        currentBlock.stackSizeAfter = stack.depth
                    }
                    for (next in nextIndexes[step.to]!!) {
                        worklist.push(Step(step.to, next))
                    }
                }
            } catch (e: Exception) {
                throw ParserException("Error parsing bytecode", buildListing(), e)
            }
        }

        private fun prepareParameterNames() {
            if (method.parameters == null) {
                return
            }
            for (parameter in method.parameters) {
                localNames.add(arrayListOf())
                if (parameter.name != null) {
                    localNames.last().add(LocalVariable(0, parameter.name))
                }
            }
        }

        private fun prepareLineNumbers(): List<Pair<Int, Int>> {
            return instructions.filter { it is LineNumberNode }.map { it as LineNumberNode }.map {
                val index = labelIndexes[it.start.label]!!
                Pair(index, it.line)
            }.toList()
        }

        fun localNameAt(localIndex: Int, instructionIndex: Int): String {
            val names = localNames.getOrNull(localIndex) ?: emptyList<LocalVariable>()
            var key = names.binarySearch { it.index.compareTo(instructionIndex) }
            if (key < 0) {
                key = -key - 2
            }
            return names.getOrNull(key)?.name ?: "\$local${localIndex + minLocal}"
        }

        private fun prepareVariableNames() {
            for (localVarNode in method.localVariables.sortedBy { labelIndexes[it.start.label] }) {
                while (localNames.size <= localVarNode.index) {
                    localNames.add(arrayListOf())
                }
                val names = localNames[localVarNode.index]
                val start = labelIndexes[localVarNode.start.label]!! - 1
                val end = labelIndexes[localVarNode.end.label]!! - 1
                val last = names.lastOrNull()
                if (last != null && last.index == start) {
                    last.name = localVarNode.name
                } else {
                    names.add(LocalVariable(start, localVarNode.name))
                }
                names.add(LocalVariable(end, null))
            }
        }

        private fun buildListing(): String {
            val textifier = Textifier()
            val visitor = TraceMethodVisitor(textifier)
            for (i in 0.until(instructions.size)) {
                instructions[i].accept(visitor)
            }
            val writer = StringWriter()
            writer.use {
                textifier.print(PrintWriter(it))
            }
            return writer.toString()
        }

        override fun visitVarInsn(opcode: Int, local: Int) {
            when (opcode) {
                Opcodes.ILOAD, Opcodes.FLOAD, Opcodes.ALOAD -> {
                    val value = localVar(local)
                    val target = stackVar(pushSingle())
                    assign(target, value)
                }
                Opcodes.LLOAD, Opcodes.DLOAD -> {
                    val value = localVar(local)
                    val target = stackVar(pushDouble())
                    assign(target, value)
                }
                Opcodes.ISTORE, Opcodes.FSTORE, Opcodes.ASTORE -> {
                    val value = stackVar(popSingle())
                    val target = localVar(local)
                    assign(target, value)
                }
                Opcodes.LSTORE, Opcodes.DSTORE -> {
                    val value = stackVar(popDouble())
                    val target = localVar(local)
                    assign(target, value)
                }
            }
        }

        override fun visitTypeInsn(opcode: Int, typeName: String) {
            when (opcode) {
                Opcodes.NEW -> {
                    assign(stackVar(pushSingle()), Construct(typeName.replace('/', '.')))
                }
                Opcodes.INSTANCEOF -> {
                    val value = stackVar(popSingle())
                    val type = parseClassType(typeName)
                    assign(stackVar(pushSingle()), InstanceOf(value, type))
                }
                Opcodes.CHECKCAST -> {
                    val value = stackVar(popSingle())
                    val type = parseClassType(typeName)
                    assign(stackVar(pushSingle()), Cast(value, type))
                }
                Opcodes.ANEWARRAY -> {
                    val size = stackVar(popSingle())
                    val type = parseClassType(typeName)
                    assign(stackVar(pushSingle()), ConstructArray(type, arrayListOf(size)))
                }
            }
        }

        override fun visitTableSwitchInsn(min: Int, max: Int, dflt: Label, vararg labels: Label) {
            val clauses = labels.asSequence()
                    .map { labelIndexes[it]!! }
                    .map { blocks[it]!! }
                    .mapIndexed { i, block -> Transition.SwitchClause(min + i, block) }
                    .toList()
            val defaultBlock = blocks[labelIndexes[dflt]!!]!!
            currentBlock.transition = Transition.Switch(stackVar(popSingle()), defaultBlock, clauses)
            nextIndexes[currentIndex] = (labels.map { labelIndexes[it]!! } + labelIndexes[dflt]!!).toIntArray()
        }

        override fun visitLookupSwitchInsn(dflt: Label, keys: IntArray, labels: Array<out Label>) {
            val clauses = labels.asSequence()
                    .map { labelIndexes[it]!! }
                    .map { blocks[it]!! }
                    .mapIndexed { i, block -> Transition.SwitchClause(keys[i], block) }
                    .toList()
            val defaultBlock = blocks[labelIndexes[dflt]!!]!!
            currentBlock.transition = Transition.Switch(stackVar(popSingle()), defaultBlock, clauses)
            nextIndexes[currentIndex] = (labels.map { labelIndexes[it]!! } + labelIndexes[dflt]!!).toIntArray()
        }

        override fun visitMultiANewArrayInsn(desc: String, dims: Int) {
            val type = parseType(desc)
            val dimensions = generateSequence { popSingle() }
                    .take(dims)
                    .map { stackVar(it) }
                    .toList().reversed()
            assign(stackVar(pushSingle()), ConstructArray(type, dimensions.toMutableList()))
        }

        override fun visitInvokeDynamicInsn(name: String, desc: String, bsm: Handle, vararg bsmArgs: Any?) {
            val signature = TypeParser().parseSignature(desc)
            for (param in signature.parameters.reversed()) {
                popTyped(param)
            }
            val result = signature.result
            when (result) {
                is Type.Boolean,
                is Type.Byte,
                is Type.Short,
                is Type.Char,
                is Type.Int -> assign(stackVar(pushSingle()), Expression.IntConstant(0))
                is Type.Long -> assign(stackVar(pushDouble()), Expression.LongConstant(0))
                is Type.Float -> assign(stackVar(pushSingle()), Expression.DoubleConstant(0.0))
                is Type.Double -> assign(stackVar(pushDouble()), Expression.DoubleConstant(0.0))
                is Type.Void -> {}
                is Type.Array,
                is Type.Obj -> assign(stackVar(pushSingle()), Expression.Null)
            }

            reporter.error(null, problemLocation!!, "InvokeDynamic instruction is not supported yet")
        }

        override fun visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean) {
            val signature = try {
                TypeParser().parseSignature(desc)
            } catch (e: TypeParseException) {
                throw BytecodeException(currentIndex, "Error parsing method signature $desc at ${e.index}")
            }

            val parameters = signature
                    .parameters
                    .reversed()
                    .map { stackVar(popTyped(it)) }
                    .reversed()
            val instance = when (opcode) {
                Opcodes.INVOKEVIRTUAL, Opcodes.INVOKEINTERFACE, Opcodes.INVOKESPECIAL -> stackVar(popSingle())
                else -> null
            }

            val resultType = signature.result
            val result = if (resultType != Type.Void) {
                stackVar(pushTyped(resultType))
            } else {
                null
            }

            val targetType = parseClassType(owner)
            when (opcode) {
                Opcodes.INVOKEVIRTUAL, Opcodes.INVOKEINTERFACE -> {
                    tryAssign(result, Invoke(instance!!, FullMethodReference(targetType, name, signature), parameters))
                }
                Opcodes.INVOKESPECIAL -> {
                    tryAssign(result, InvokeSpecial(instance!!, FullMethodReference(targetType, name, signature),
                            parameters))
                }
                Opcodes.INVOKESTATIC -> {
                    tryAssign(result, InvokeStatic(FullMethodReference(targetType, name, signature), parameters))
                }
            }
        }

        override fun visitFieldInsn(opcode: Int, owner: String, name: String, desc: String) {
            val type = parseType(desc)
            val targetType = owner.replace('/', '.')
            when (opcode) {
                Opcodes.GETFIELD -> {
                    val instance = stackVar(popSingle())
                    val result = stackVar(pushTyped(type))
                    assign(result, GetField(instance, FullFieldReference(targetType, name, type)))
                }
                Opcodes.GETSTATIC -> {
                    val result = stackVar(pushTyped(type))
                    assign(result, GetStaticField(FullFieldReference(targetType, name, type), init = true))
                }
                Opcodes.PUTFIELD -> {
                    val value = stackVar(popTyped(type))
                    val instance = stackVar(popSingle())
                    assign(GetField(instance, FullFieldReference(targetType, name, type)), value)
                }
                Opcodes.PUTSTATIC -> {
                    val value = stackVar(popTyped(type))
                    assign(GetStaticField(FullFieldReference(targetType, name, type)), value)
                }
            }
        }

        override fun visitLdcInsn(cst: Any?) {
            when (cst) {
                null -> assign(stackVar(pushSingle()), Null)
                is Int -> assign(stackVar(pushSingle()), IntConstant(cst))
                is Long -> assign(stackVar(pushDouble()), LongConstant(cst))
                is Float -> assign(stackVar(pushSingle()), DoubleConstant(cst.toDouble()))
                is Double -> assign(stackVar(pushDouble()), DoubleConstant(cst))
                is String -> assign(stackVar(pushSingle()), StringConstant(cst))
                is org.objectweb.asm.Type -> assign(stackVar(pushSingle()), TypeConstant(parseType(cst.descriptor)))
            }
        }

        override fun visitJumpInsn(opcode: Int, label: Label) {
            val targetIndex = labelIndexes[label]!!
            val target = blocks[targetIndex]!!
            when (opcode) {
                Opcodes.GOTO -> {
                    currentBlock.transition = Transition.Jump(target)
                    nextIndexes[currentIndex] = intArrayOf(targetIndex)
                }
                else -> {
                    currentBlock.transition = Transition.Conditional(getCondition(opcode),
                            target, blocks[currentIndex + 1]!!)
                    nextIndexes[currentIndex] = intArrayOf(currentIndex + 1, targetIndex)
                }
            }
        }

        fun getCondition(opcode: Int) = when (opcode) {
            Opcodes.IFEQ -> Binary(EQUAL, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFNE -> Binary(NOT_EQUAL, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFLT -> Binary(LESS, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFLE -> Binary(LESS_OR_EQUAL, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFGT -> Binary(GREATER, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFGE -> Binary(GREATER_OR_EQUAL, stackVar(popSingle()), IntConstant(0))
            Opcodes.IFNULL -> Binary(EQUAL, stackVar(popSingle()), Null)
            Opcodes.IFNONNULL -> Binary(NOT_EQUAL, stackVar(popSingle()), Null)
            else -> {
                val b = stackVar(popSingle())
                val a = stackVar(popSingle())
                Binary(getBinaryCondition(opcode), a, b)
            }
        }

        fun getBinaryCondition(opcode: Int) : BinaryOperation = when(opcode) {
            Opcodes.IF_ICMPEQ, Opcodes.IF_ACMPEQ -> EQUAL
            Opcodes.IF_ICMPNE, Opcodes.IF_ACMPNE -> NOT_EQUAL
            Opcodes.IF_ICMPLT -> LESS
            Opcodes.IF_ICMPLE -> LESS_OR_EQUAL
            Opcodes.IF_ICMPGT -> GREATER
            Opcodes.IF_ICMPGE -> GREATER_OR_EQUAL
            else -> throw BytecodeException(currentIndex, "Unexpected opcode $opcode")
        }

        override fun visitIincInsn(variable: Int, increment: Int) {
            tryAssign(null, PostIncrement(localNameAt(variable, currentIndex), increment))
        }

        override fun visitIntInsn(opcode: Int, operand: Int) {
            when (opcode) {
                Opcodes.BIPUSH, Opcodes.SIPUSH -> assign(stackVar(pushSingle()), IntConstant(operand))
                Opcodes.NEWARRAY -> {
                    val type = when (operand) {
                        Opcodes.T_BOOLEAN -> Type.Boolean
                        Opcodes.T_BYTE -> Type.Byte
                        Opcodes.T_SHORT -> Type.Short
                        Opcodes.T_CHAR -> Type.Char
                        Opcodes.T_INT -> Type.Int
                        Opcodes.T_LONG -> Type.Long
                        Opcodes.T_FLOAT -> Type.Float
                        Opcodes.T_DOUBLE -> Type.Double
                        else -> throw BytecodeException(currentIndex, "Unexpected array type $opcode")
                    }
                    val size = stackVar(popSingle())
                    assign(stackVar(pushSingle()), ConstructArray(type, arrayListOf(size)))
                }
            }
        }

        override fun visitInsn(opcode: Int) {
            when (opcode) {
                Opcodes.ACONST_NULL -> assignAndPushSingle(Null)
                Opcodes.ICONST_0 -> assignAndPushSingle(IntConstant(0))
                Opcodes.ICONST_1 -> assignAndPushSingle(IntConstant(1))
                Opcodes.ICONST_2 -> assignAndPushSingle(IntConstant(2))
                Opcodes.ICONST_3 -> assignAndPushSingle(IntConstant(3))
                Opcodes.ICONST_4 -> assignAndPushSingle(IntConstant(4))
                Opcodes.ICONST_5 -> assignAndPushSingle(IntConstant(5))
                Opcodes.ICONST_M1 -> assignAndPushSingle(IntConstant(-1))
                Opcodes.FCONST_0 -> assignAndPushSingle(DoubleConstant(0.0))
                Opcodes.FCONST_1 -> assignAndPushSingle(DoubleConstant(1.0))
                Opcodes.FCONST_2 -> assignAndPushSingle(DoubleConstant(2.0))
                Opcodes.LCONST_0 -> assignAndPushDouble(LongConstant(0L))
                Opcodes.LCONST_1 -> assignAndPushDouble(LongConstant(1L))
                Opcodes.DCONST_0 -> assignAndPushDouble(DoubleConstant(0.0))
                Opcodes.DCONST_1 -> assignAndPushDouble(DoubleConstant(1.0))

                Opcodes.BALOAD, Opcodes.IALOAD, Opcodes.CALOAD, Opcodes.SALOAD, Opcodes.FALOAD,
                Opcodes.AALOAD -> binarySingle(ARRAY_ELEMENT)
                Opcodes.LALOAD, Opcodes.DALOAD -> binaryDouble(ARRAY_ELEMENT)
                Opcodes.BASTORE -> arrayStore { leftRightShift(it, 24) }
                Opcodes.SASTORE -> arrayStore { leftRightShift(it, 16) }
                Opcodes.CASTORE -> arrayStore { BitwiseBinary(BITWISE_AND, it, IntConstant(0xFFFF)) }
                Opcodes.IASTORE, Opcodes.FASTORE, Opcodes.AASTORE -> arrayStore()
                Opcodes.LASTORE, Opcodes.DASTORE -> {
                    val value = stackVar(popDouble())
                    val index = stackVar(popSingle())
                    val array = stackVar(popSingle())
                    assign(Binary(ARRAY_ELEMENT, array, index), value)
                }

                Opcodes.POP -> tryAssign(null, stackVar(popSingle()))
                Opcodes.POP2 -> {
                    if (stack.type == StackFrameType.SINGLE) {
                        tryAssign(null, stackVar(popSingle()))
                        tryAssign(null, stackVar(popSingle()))
                    } else {
                        tryAssign(null, stackVar(popDouble()))
                    }
                }

                Opcodes.DUP -> {
                    val value = stackVar(popSingle())
                    pushSingle()
                    val target = stackVar(pushSingle())
                    assign(target, value)
                }
                Opcodes.DUP_X1 -> {
                    if (stack.depth < 2) {
                        throw BytecodeException(currentIndex, "DUP_X1 instruction requires stack "
                                + "of at least two elements")
                    }
                    pushSingle()
                    val c = stackVar(stack.depth)
                    val b = stackVar(stack.depth - 1)
                    val a = stackVar(stack.depth - 2)
                    assign(b, c)
                    assign(a, b)
                    assign(c, a)
                }
                Opcodes.DUP_X2 -> dupx2()
                Opcodes.DUP2 -> {
                    if (stack.type == StackFrameType.SINGLE) {
                        val value1 = popSingle()
                        val value2 = popSingle()
                        pushSingle()
                        pushSingle()
                        assign(stackVar(pushSingle()), stackVar(value2))
                        assign(stackVar(pushSingle()), stackVar(value1))
                    } else {
                        val value = stackVar(popDouble())
                        pushDouble()
                        val target = stackVar(pushDouble())
                        assign(target, value)
                    }
                }
                Opcodes.DUP2_X1 -> dup2x1()
                Opcodes.DUP2_X2 -> dup2x2()

                Opcodes.SWAP -> {
                    val tmp = stackVar(stack.depth + 1)
                    val a = stackVar(popSingle())
                    val b = stackVar(popSingle())
                    pushSingle()
                    pushSingle()
                    assign(a, tmp)
                    assign(b, a)
                    assign(a, b)
                }

                Opcodes.IADD -> binarySingle(ADD, ArithmeticType.INT)
                Opcodes.FADD -> binarySingle(ADD, ArithmeticType.DOUBLE)
                Opcodes.LADD -> binaryDouble(ADD, ArithmeticType.LONG)
                Opcodes.DADD -> binaryDouble(ADD, ArithmeticType.DOUBLE)

                Opcodes.ISUB -> binarySingle(SUBTRACT, ArithmeticType.INT)
                Opcodes.FSUB -> binarySingle(SUBTRACT, ArithmeticType.DOUBLE)
                Opcodes.LSUB -> binaryDouble(SUBTRACT, ArithmeticType.LONG)
                Opcodes.DSUB -> binaryDouble(SUBTRACT, ArithmeticType.DOUBLE)

                Opcodes.IMUL -> binarySingle(MULTIPLY, ArithmeticType.INT)
                Opcodes.FMUL -> binarySingle(MULTIPLY, ArithmeticType.DOUBLE)
                Opcodes.LMUL -> binaryDouble(MULTIPLY, ArithmeticType.LONG)
                Opcodes.DMUL -> binaryDouble(MULTIPLY, ArithmeticType.DOUBLE)

                Opcodes.IDIV -> binarySingle(DIVIDE, ArithmeticType.INT)
                Opcodes.FDIV -> binarySingle(DIVIDE, ArithmeticType.DOUBLE)
                Opcodes.LDIV -> binaryDouble(DIVIDE, ArithmeticType.LONG)
                Opcodes.DDIV -> binaryDouble(DIVIDE, ArithmeticType.DOUBLE)

                Opcodes.IREM -> binarySingle(REMAINDER, ArithmeticType.INT)
                Opcodes.FREM -> binarySingle(REMAINDER, ArithmeticType.DOUBLE)
                Opcodes.LREM -> binaryDouble(REMAINDER, ArithmeticType.LONG)
                Opcodes.DREM -> binaryDouble(REMAINDER, ArithmeticType.DOUBLE)

                Opcodes.FCMPG, Opcodes.FCMPL -> binarySingle(COMPARE, ArithmeticType.DOUBLE)
                Opcodes.LCMP -> binarySingle(COMPARE, ArithmeticType.LONG, 2, 2)
                Opcodes.DCMPG, Opcodes.DCMPL -> binarySingle(COMPARE, ArithmeticType.DOUBLE, 2, 2)

                Opcodes.INEG -> unary({ Negate(it, ArithmeticType.INT) })
                Opcodes.FNEG -> unary({ Negate(it, ArithmeticType.DOUBLE) })
                Opcodes.LNEG -> unary({ Negate(it, ArithmeticType.LONG) }, 2, 2)
                Opcodes.DNEG -> unary({ Negate(it, ArithmeticType.DOUBLE) }, 2, 2)

                Opcodes.IAND -> binarySingle(BITWISE_AND, BitwiseType.INT)
                Opcodes.IOR -> binarySingle(BITWISE_OR, BitwiseType.INT)
                Opcodes.IXOR -> binarySingle(BITWISE_XOR, BitwiseType.INT)
                Opcodes.ISHL -> binarySingle(LEFT_SHIFT, BitwiseType.INT)
                Opcodes.ISHR -> binarySingle(RIGHT_SHIFT, BitwiseType.INT)
                Opcodes.IUSHR -> binarySingle(UNSIGNED_RIGHT_SHIFT, BitwiseType.INT)

                Opcodes.LAND -> binaryDouble(BITWISE_AND, BitwiseType.LONG)
                Opcodes.LOR -> binaryDouble(BITWISE_OR, BitwiseType.LONG)
                Opcodes.LXOR -> binaryDouble(BITWISE_XOR, BitwiseType.LONG)
                Opcodes.LSHL -> binaryDouble(LEFT_SHIFT, BitwiseType.LONG, 2, 1)
                Opcodes.LSHR -> binaryDouble(RIGHT_SHIFT, BitwiseType.LONG, 2, 1)
                Opcodes.LUSHR -> binaryDouble(UNSIGNED_RIGHT_SHIFT, BitwiseType.LONG, 2, 1)

                Opcodes.I2B -> unary({ leftRightShift(it, 24) })
                Opcodes.I2S -> unary({ leftRightShift(it, 16) })
                Opcodes.I2C -> unary({ BitwiseBinary(BITWISE_AND, it, IntConstant(0xFFFF)) })
                Opcodes.I2F -> unary({ ArithmeticCast(ArithmeticType.INT, ArithmeticType.DOUBLE, it) })
                Opcodes.I2L -> unary({ ArithmeticCast(ArithmeticType.INT, ArithmeticType.LONG, it) }, result = 2)
                Opcodes.I2D -> unary({ ArithmeticCast(ArithmeticType.INT, ArithmeticType.DOUBLE, it) }, result = 2)

                Opcodes.F2I -> unary({ ArithmeticCast(ArithmeticType.DOUBLE, ArithmeticType.INT, it) })
                Opcodes.F2L -> unary({ ArithmeticCast(ArithmeticType.DOUBLE, ArithmeticType.LONG, it) }, result = 2)
                Opcodes.F2D -> unary({ it }, result = 2)

                Opcodes.L2I -> unary({ ArithmeticCast(ArithmeticType.LONG, ArithmeticType.INT, it) }, parameter = 2)
                Opcodes.L2F -> unary({ ArithmeticCast(ArithmeticType.LONG, ArithmeticType.DOUBLE, it) }, parameter = 2)
                Opcodes.L2D -> unary({ ArithmeticCast(ArithmeticType.LONG, ArithmeticType.DOUBLE, it) },
                        result = 2, parameter = 2)

                Opcodes.D2I -> unary({ ArithmeticCast(ArithmeticType.DOUBLE, ArithmeticType.INT, it) }, parameter = 2)
                Opcodes.D2F -> unary({ it }, parameter = 2)
                Opcodes.D2L -> unary({ ArithmeticCast(ArithmeticType.DOUBLE, ArithmeticType.LONG, it) },
                        result = 2, parameter = 2)

                Opcodes.IRETURN, Opcodes.ARETURN, Opcodes.FRETURN -> generateReturn(stackVar(popSingle()))
                Opcodes.DRETURN, Opcodes.LRETURN -> generateReturn(stackVar(popDouble()))
                Opcodes.RETURN -> generateReturn(null)
                Opcodes.ATHROW -> {
                    nextIndexes[currentIndex] = intArrayOf()
                    currentBlock.transition = Transition.Empty
                    val stmt = Statement.Throw(stackVar(popSingle()))
                    stmt.location = getLocation()
                    add(stmt)
                }

                Opcodes.ARRAYLENGTH -> unary(::ArrayLength)
                Opcodes.MONITORENTER, Opcodes.MONITOREXIT -> popSingle()
            }
        }

        fun generateReturn(value: Expression?) {
            currentBlock.transition = Transition.Empty
            nextIndexes[currentIndex] = intArrayOf()
            val stmt = Statement.Return(value)
            stmt.location = getLocation()
            add(stmt)
        }

        fun dupx2() {
            if (stack.depth < 3) {
                throw BytecodeException(currentIndex, "DUP_X1 instruction requires stack "
                        + "of at least three elements")
            }
            val d = stackVar(stack.depth + 1)
            val c = stackVar(stack.depth)
            val b = stackVar(stack.depth - 1)
            val a = stackVar(stack.depth - 2)
            if (stack.next?.type == StackFrameType.SINGLE) {
                (1..3).forEach { popSingle() }
                (1..3).forEach { pushSingle() }
                assign(c, d)
                assign(b, c)
                assign(a, b)
                assign(d, a)
            } else {
                popSingle()
                popDouble()
                pushSingle()
                pushDouble()
                pushSingle()
                assign(c, d)
                tryAssign(null, c)
                assign(a, b)
                assign(d, a)
            }
        }

        fun dup2x1() {
            if (stack.depth < 3) {
                throw BytecodeException(currentIndex, "DUP_X1 instruction requires stack "
                        + "of at least three elements")
            }
            val e = stackVar(stack.depth + 2)
            val d = stackVar(stack.depth + 1)
            val c = stackVar(stack.depth)
            val b = stackVar(stack.depth - 1)
            val a = stackVar(stack.depth - 2)
            if (stack.type == StackFrameType.SINGLE) {
                (1..3).forEach { popSingle() }
                (1..5).forEach { pushSingle() }
                assign(c, e)
                assign(b, d)
                assign(a, c)
                assign(d, a)
                assign(e, b)
            } else {
                popDouble()
                popSingle()
                pushDouble()
                pushSingle()
                pushDouble()
                assign(c, e)
                assign(b, d)
                assign(a, c)
                assign(d, a)
                assign(e, b)
            }
        }

        fun dup2x2() {
            if (stack.depth < 3) {
                throw BytecodeException(currentIndex, "DUP_X1 instruction requires stack "
                        + "of at least four elements")
            }
            val f = stackVar(stack.depth + 2)
            val e = stackVar(stack.depth + 1)
            val d = stackVar(stack.depth)
            val c = stackVar(stack.depth - 1)
            val b = stackVar(stack.depth - 2)
            val a = stackVar(stack.depth - 3)
            if (stack.type == StackFrameType.SINGLE) {
                if (stack.next?.next?.type == StackFrameType.SINGLE) {
                    (1..4).forEach { popSingle() }
                    (1..6).forEach { pushSingle() }
                    assign(d, f)
                    assign(c, e)
                    assign(b, d)
                    assign(a, c)
                    assign(f, b)
                    assign(e, a)
                } else {
                    (1..2).forEach { popSingle() }
                    popDouble()
                    (1..2).forEach { pushSingle() }
                    pushDouble()
                    (1..2).forEach { pushSingle() }
                    assign(d, f)
                    assign(c, e)
                    tryAssign(null, d)
                    assign(a, c)
                    assign(f, b)
                    assign(e, a)
                }
            } else {
                if (stack.next?.next?.type == StackFrameType.SINGLE) {
                    popDouble()
                    (1..2).forEach { popSingle() }
                    pushDouble()
                    (1..2).forEach { popSingle() }
                    pushDouble()
                    assign(c, e)
                    assign(b, d)
                    assign(a, c)
                    tryAssign(null, b)
                    assign(e, a)
                } else {
                    (1..2).forEach { popDouble() }
                    (1..3).forEach { pushDouble() }
                    assign(c, e)
                    assign(a, c)
                    assign(e, a)
                }
            }
        }

        fun localVar(index: Int) = if (index + minLocal > 0) Variable(localNameAt(index, currentIndex)) else Self

        fun stackVar(index: Int) = StackVariable(index)

        fun assignAndPushSingle(value: Expression) {
            assign(stackVar(pushSingle()), value)
        }

        fun assignAndPushDouble(value: Expression) {
            assign(stackVar(pushDouble()), value)
        }

        fun arrayStore(wrap: (Expression) -> Expression = { it }) {
            val value = stackVar(popSingle())
            val index = stackVar(popSingle())
            val array = stackVar(popSingle())
            assign(Binary(ARRAY_ELEMENT, array, index), wrap(value))
        }

        fun leftRightShift(value: Expression, amount: Int): Expression {
            val left = BitwiseBinary(BitwiseBinaryOperation.LEFT_SHIFT, value, IntConstant(amount))
            return BitwiseBinary(RIGHT_SHIFT, left, IntConstant(amount))
        }

        fun binarySingle(operation: ArithmeticBinaryOperation, type: ArithmeticType, first: Int = 1, second: Int = 1) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushSingle()), ArithmeticBinary(operation, a, b, type))
        }

        fun binarySingle(operation: BitwiseBinaryOperation, type: BitwiseType, first: Int = 1, second: Int = 1) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushSingle()), BitwiseBinary(operation, a, b, type))
        }

        fun binarySingle(operation: BinaryOperation, first: Int = 1, second: Int = 1) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushSingle()), Binary(operation, a, b))
        }

        fun binaryDouble(operation: ArithmeticBinaryOperation, type: ArithmeticType, first: Int = 2, second: Int = 2) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushDouble()), ArithmeticBinary(operation, a, b, type))
        }

        fun binaryDouble(operation: BitwiseBinaryOperation, type: BitwiseType, first: Int = 2, second: Int = 2) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushDouble()), BitwiseBinary(operation, a, b, type))
        }

        fun binaryDouble(operation: BinaryOperation, first: Int = 2, second: Int = 2) {
            val b = stackVar(if (second == 2) popDouble() else popSingle())
            val a = stackVar(if (first == 2) popDouble() else popSingle())
            assign(stackVar(pushDouble()), Binary(operation, a, b))
        }

        inline fun unary(wrap: (Expression) -> Expression, result: Int = 1, parameter: Int = 1) {
            val a = stackVar(if (parameter == 2) popDouble() else popSingle())
            assign(stackVar(if (result == 2) pushDouble() else pushSingle()), wrap(a))
        }

        fun tryAssign(target: Expression?, value: Expression) {
            if (target != null) {
                assign(target, value)
            } else {
                value.location = getLocation()
                add(Statement.ExpressionStatement(value))
            }
        }

        fun getLocation(): SourceLocation? {
            val file = sourceFile
            return if (file == null) null else SourceLocation(file, currentLine)
        }

        fun assign(target: Expression, value: Expression) {
            value.location = getLocation()
            add(Statement.Assignment(target, value))
        }

        fun add(statement: Statement) {
            currentBlock.body.add(statement)
        }

        fun pushSingle(): Int {
            stack = StackFrame(stack, StackFrameType.SINGLE)
            return stack.depth
        }

        fun pushDouble(): Int {
            stack = StackFrame(stack, StackFrameType.DOUBLE_FIRST_HALF)
            stack = StackFrame(stack, StackFrameType.DOUBLE_SECOND_HALF)
            return stack.depth
        }

        fun popSingle(): Int {
            val next = stack.next
            next ?: throw BytecodeException(currentIndex, "Trying to pop off empty stack")
            if (stack.type != StackFrameType.SINGLE) {
                throw BytecodeException(currentIndex, "Stack type mismatch. Expected SINGLE, "
                        + "but ${stack.type} occurred")
            }
            val depth = stack.depth
            stack = next
            return depth
        }

        fun popDouble(): Int {
            val next = stack.next
            next ?: throw BytecodeException(currentIndex, "Trying to pop off empty stack")
            if (stack.type != StackFrameType.DOUBLE_SECOND_HALF) {
                throw BytecodeException(currentIndex, "Stack type mismatch. Expected DOUBLE_SECOND_HALF, "
                        + "but ${stack.type} occurred")
            }

            val last = next.next
            last ?: throw BytecodeException(currentIndex, "Trying to pop off empty stack")
            if (next.type != StackFrameType.DOUBLE_FIRST_HALF) {
                throw BytecodeException(currentIndex, "Stack type mismatch. Expected SINGLE, "
                        + "but ${stack.type} occurred")
            }

            val depth = stack.depth
            stack = last
            return depth
        }

        fun popTyped(type: Type) = if (typeSize(type) == 2) popDouble() else popSingle()

        fun pushTyped(type: Type) = if (typeSize(type) == 2) pushDouble() else pushSingle()

        fun typeSize(type: Type) = when (type) {
            is Type.Long,
            is Type.Double -> 2
            else -> 1
        }

        fun parseClassType(type: String) = try {
            TypeParser().parseClass(type)
        } catch (e: TypeParseException) {
            throw BytecodeException(currentIndex, "Error parsing type $type")
        }

        fun parseType(str: String) = try {
            TypeParser().parse(str)
        } catch (e: TypeParseException) {
            throw BytecodeException(currentIndex, "Error parsing type $str")
        }
    }

    internal class BlockStartFinder(instructions: List<AbstractInsnNode>, val labelIndexes: Map<Label, Int>)
    : MethodVisitor(Opcodes.ASM5) {
        val blocks: MutableList<Block?> = arrayOfNulls<Block>(instructions.size).toMutableList()
        private var index = 0

        init {
            blocks[0] = Block()
            for (i in 0.until(instructions.size)) {
                index = i
                instructions[i].accept(this)
            }
        }

        override fun visitJumpInsn(opcode: Int, label: Label) {
            createBlock(index + 1)
            createBlock(labelIndexes[label]!!)
        }

        override fun visitTableSwitchInsn(min: Int, max: Int, dflt: Label, vararg labels: Label) {
            createBlock(labelIndexes[dflt]!!)
            labels.forEach { createBlock(labelIndexes[it]!!) }
        }

        override fun visitLookupSwitchInsn(dflt: Label, keys: IntArray?, labels: Array<out Label>) {
            createBlock(labelIndexes[dflt]!!)
            labels.forEach { createBlock(labelIndexes[it]!!) }
        }

        fun createBlock(index: Int) {
            if (index < blocks.size && blocks[index] == null) {
                blocks[index] = Block()
            }
        }
    }

    class StackFrame internal constructor(val next: StackFrame?, val type: StackFrameType, val depth: Int) {

        constructor(next: StackFrame, type: StackFrameType) : this(next as StackFrame?, type, next.depth + 1)

        constructor(depth: Int) : this(null, StackFrameType.ROOT, depth)
    }

    enum class StackFrameType {
        ROOT,
        SINGLE,
        DOUBLE_FIRST_HALF,
        DOUBLE_SECOND_HALF
    }
}
