package org.bastardvm.compiler.parsing

class ParserException(message: String, listing: String, cause: Throwable)
: RuntimeException("$message:\n$listing", cause)
