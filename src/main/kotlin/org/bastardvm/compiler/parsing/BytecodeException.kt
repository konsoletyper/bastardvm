package org.bastardvm.compiler.parsing

class BytecodeException(val index: Int, text: String) : Exception("$text at $index")
