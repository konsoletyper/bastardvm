package org.bastardvm.compiler

import org.bastardvm.compiler.bytecode.ClassSource
import org.bastardvm.compiler.bytecode.Procedure
import org.bastardvm.compiler.diagnostics.Reporter
import org.bastardvm.compiler.diagnostics.WritingReporter
import org.bastardvm.compiler.model.*
import org.bastardvm.compiler.optimizing.BlockOptimizer
import org.bastardvm.compiler.optimizing.StatementOptimizer
import org.bastardvm.compiler.optimizing.removeLabels
import org.bastardvm.compiler.parsing.BytecodeParser
import org.bastardvm.compiler.rendering.BlockRenderer
import org.bastardvm.compiler.rendering.DefaultWriter
import org.bastardvm.compiler.rendering.Naming
import org.bastardvm.compiler.rendering.Renderer
import org.bastardvm.ffi.JSBody
import org.bastardvm.ffi.Omitted
import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodNode
import java.io.OutputStreamWriter

class Compiler(
        var source: ClassSource,
        val model: Model,
        val filters: List<(ClassModel) -> Boolean?> = arrayListOf(),
        val nameFilters: List<(String) -> Boolean?> = arrayListOf(),
        val naming: Naming,
        val reporter: Reporter = WritingReporter(OutputStreamWriter(System.err))) {
    val classes = hashSetOf<ClassModel>()

    fun compile(target: Appendable) {
        buildClassSet()
        val renderer = Renderer(DefaultWriter(
                naming = naming,
                appendable = target,
                minifying = false))
        for (classModel in classes) {
            writeClassHeader(renderer, classModel)
        }
        for (classModel in classes) {
            writeMetadata(renderer, classModel)
        }
        for (classModel in classes) {
            compile(renderer, classModel)
        }
    }

    private fun buildClassSet() {
        for (className in model.getClassNames()) {
            val nameAccepted = accept(className)
            if (!(nameAccepted ?: false)) {
                continue
            }
            val classModel = model[className]!!
            if (accept(classModel) ?: false) {
                classes.add(classModel)
            }
        }
    }

    private fun writeClassHeader(renderer: Renderer, classModel: ClassModel) {
        if (Omitted::class.qualifiedName!! in classModel.annotations) {
            return
        }

        renderer.append("function ").append(naming.getNameFor(classModel.name)).append("()").ws().append("{")
                .indent().newLine()
        for (entry in classModel.memberFields.entries.filter { !it.value.static }) {
            renderer.append("this.").append(naming.getNameFor(entry.key)).ws().append("=").ws()
                    .append(defaultValue(entry.key.type)).append(";").newLine()
        }
        renderer.outdent().append("}").newLine()

        for (entry in classModel.memberFields.entries.filter { it.value.static }) {
            renderer.append(naming.getNameFor(FullFieldReference(classModel.name, entry.key))).ws().append("=").ws()
                    .append(defaultValue(entry.key.type)).append(";").newLine()
        }
    }

    private fun writeMetadata(renderer: Renderer, classModel: ClassModel) {
        if (Omitted::class.qualifiedName!! in classModel.annotations) {
            return
        }

        val parent = classModel.parent
        val parentMangledName = if (parent != null) naming.getNameFor(parent.name) else "Object"
        val mangledName = naming.getNameFor(classModel.name)
        renderer.append(mangledName).append(".prototype").ws().append("=").ws()
                .append("new ").append(parentMangledName).append(";").newLine()
        renderer.append(mangledName).append(".prototype.constructor").ws().append("=").ws()
                .append(mangledName).append(";").newLine()
        renderer.append(mangledName).append(".interfaces").ws().append("=").ws().append("[")
                .commaSeparated(classModel.interfaces.map { naming.getNameFor(it.name) })
                .append("];").newLine()

        renderer.append(mangledName).append(".init").ws().append("=").ws().append("function()")
                .ws().append("{").indent().newLine()
        val clinitRef = MethodReference("<clinit>", Type.Void)
        val clinitMethod = classModel.memberMethods[clinitRef]
        if (clinitMethod != null) {
            renderer.append(naming.getNameFor(FullMethodReference(Type.Obj(classModel.name), clinitRef)))
                    .append("();").newLine()
            renderer.append(mangledName).append(".init").ws().append("=").ws()
                    .append("function(){};").newLine()
        }
        renderer.outdent().append("};").newLine()
    }

    private fun defaultValue(type: Type) = when (type) {
        is Type.Boolean, is Type.Byte, is Type.Short, is Type.Int, is Type.Float, is Type.Double -> "0"
        is Type.Long -> "VM.intToDouble(0)"
        else -> "null"
    }

    private fun compile(renderer: Renderer, classModel: ClassModel) {
        val parser = BytecodeParser(reporter)
        val blockRenderer = BlockRenderer(renderer)
        source.getBytecode(classModel.name)?.use {
            val classReader = ClassReader(it)
            val classNode = ClassNode()
            classReader.accept(classNode, 0)
            for (methodNode in classNode.methods) {
                compileMethod(classNode.sourceFile, parser, blockRenderer, methodNode, classModel)
            }
        }
    }

    private fun compileMethod(sourceFile: String?, parser: BytecodeParser, blockRenderer: BlockRenderer,
            methodNode: MethodNode, classModel: ClassModel) {
        try {
            var methodRef = MethodReference(methodNode.name, TypeParser().parseSignature(methodNode.desc))
            val methodModel = classModel.memberMethods[methodRef] ?: return
            if (methodModel.abstractModifier) {
                return
            }
            if (Omitted::class.qualifiedName!! in methodModel.annotations) {
                return
            }
            if (methodRef.name == "init" && classModel.name == "java.lang.Object") {
                methodRef = MethodReference("<init>", methodRef.signature)
            }
            if (!methodModel.static && Omitted::class.qualifiedName!! in methodModel.owner.annotations) {
                return
            }
            if (methodModel.nativeModifier && JSBody::class.qualifiedName!! !in methodModel.annotations) {
                return
            }

            val renderer = blockRenderer.renderer
            val fullRef = FullMethodReference(Type.Obj(classModel.name), methodRef)
            if (methodModel.static) {
                renderer.append("function ").append(naming.getNameFor(fullRef))
            } else {
                renderer.append(naming.getNameFor(classModel.name)).append(".prototype.")
                        .append(naming.getNameFor(methodRef)).ws().append("=").ws()
                        .append("function")
            }
            if (JSBody::class.qualifiedName!! in methodModel.annotations) {
                compileNativeMethod(blockRenderer.renderer, methodModel)
                return
            }

            val procedure = parser.parse(sourceFile, classModel.name, methodNode)
            renderer.append("(").commaSeparated(procedure.parameterNames).append(")").ws().append("{")
                    .newLine().indent()

            val block = procedure.block
            if (block != null) {
                val optimizedBlock = BlockOptimizer(block).optimize()
                StatementOptimizer().optimizeBody(optimizedBlock.body)
                optimizedBlock.body.removeLabels()
                val normalizer = Normializer(model, reporter, fullRef)
                normalizer.normalizeAll(optimizedBlock.body)
                blockRenderer.write(Procedure(procedure.parameterNames, optimizedBlock))
            }

            renderer.outdent().append("}").newLine()
        } catch (e: Exception) {
            throw CompilerException("Unexpected error occurred decompiling method " +
                    "${classModel.name}.${methodNode.name}${methodNode.desc}", e)
        }
    }

    private fun compileNativeMethod(renderer: Renderer, methodModel: MethodModel) {
        val methodRef = methodModel.reference
        val annotation = methodModel.annotations[JSBody::class.qualifiedName!!]!!
        val parametersAnnot = annotation.fields["parameters"]
        val parameterNames = if (parametersAnnot != null) {
            (parametersAnnot as AnnotationValue.Array).value.map {
                (it as AnnotationValue.String).value
            }
        }  else {
            listOf()
        }
        val body = (annotation.fields["script"] as AnnotationValue.String).value
        val firstParameter = if (methodModel.static) 1 else 0
        val parameters = 1.until(methodRef.parameters.size + firstParameter).map {
            parameterNames.getOrElse(it - firstParameter, { index -> "local$index" })
        }
        renderer.append("(").commaSeparated(parameters).append(")").ws().append("{").newLine().indent()
        renderer.append(body).newLine()
        renderer.outdent().append("}").newLine()
    }

    private fun accept(cls: String) = nameFilters.map { it(cls) }.filterNotNull().firstOrNull()

    private fun accept(cls: ClassModel) = filters.map { it(cls) }.filterNotNull().firstOrNull()
}
