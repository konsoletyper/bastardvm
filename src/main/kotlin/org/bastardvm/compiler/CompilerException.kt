package org.bastardvm.compiler

class CompilerException(message: String, cause: Throwable) : RuntimeException(message, cause)