package org.bastardvm.compiler.optimizing

import org.bastardvm.compiler.ast.BinaryOperation
import org.bastardvm.compiler.ast.Expression
import org.bastardvm.compiler.ast.Statement

class StatementOptimizer(private val target: MutableList<Statement> = arrayListOf()) {
    private val stack = hashMapOf<Int, StackEntry>()

    fun appendAll(statements: Iterable<Statement>) {
        statements.forEach { append(it) }
    }

    fun append(statement: Statement) {
        return when (statement) {
            is Statement.Group -> {
                optimizeBody(statement.body)
                doAppend(optimizeGroup(statement))
            }
            is Statement.While -> {
                optimizeBody(statement.body)
                doAppend(optimizeLoop(statement))
            }
            is Statement.If -> {
                optimizeBody(statement.consequent)
                optimizeBody(statement.alternative)
                doAppend(optimizeBranching(statement))
            }
            is Statement.Switch -> {
                for (clause in statement.clauses) {
                    optimizeBody(clause.body)
                }
                optimizeBody(statement.defaultBody)
                doAppend(statement)
            }
            else -> doAppend(statement)
        }
    }

    internal fun doAppend(statement: Statement) {
        return when (statement) {
            is Statement.Assignment -> {
                if (hasIncrement(statement.value)) {
                    stack.clear()
                }
                val left = statement.target
                if (left is Expression.StackVariable) {
                    statement.value = optimize(statement.value, left.index)
                    stack.put(left.index, StackEntry(target.size, statement.value))
                } else {
                    statement.value = optimize(statement.value, 0)
                    statement.target = optimize(statement.target, 0)
                }
                target.add(statement)
                Unit
            }
            is Statement.ExpressionStatement -> {
                if (hasIncrement(statement.expression)) {
                    stack.clear()
                }
                statement.expression = optimize(statement.expression, 0)
                target.add(statement)
                Unit
            }
            is Statement.Return -> {
                if (statement.value?.let { hasIncrement(it) } ?: false) {
                    stack.clear()
                }
                statement.value = statement.value?.let { optimize(it, 0) }
                target.add(statement)
                stack.clear()
            }
            is Statement.If -> {
                if (hasIncrement(statement.condition)) {
                    stack.clear()
                }
                statement.condition = optimize(statement.condition, 0)
                target.add(statement)
                stack.clear()
            }
            is Statement.Switch -> {
                if (hasIncrement(statement.condition)) {
                    stack.clear()
                }
                statement.condition = optimize(statement.condition, 0)
                target.add(statement)
                stack.clear()
            }
            else -> {
                target.add(statement)
                stack.clear()
            }
        }
    }

    fun optimizeBody(body: MutableList<Statement>) {
        val copy = body.toList()
        body.clear()
        val nestedOptimizer = StatementOptimizer(body)
        nestedOptimizer.appendAll(copy)
    }

    internal fun optimizeBranching(statement: Statement.If): Statement {
        do {
            var changed = false
            val firstStatement = statement.consequent.firstOrNull()
            val label = statement.label
            if (label != null && firstStatement is Statement.If && statement.alternative.isEmpty()) {
                if (firstStatement.consequent.size == 1) {
                    val shortCircuit = firstStatement.consequent[0]
                    if (shortCircuit is Statement.Break && shortCircuit.label == label) {
                        statement.consequent.removeAt(0)
                        statement.condition = tryOptimizeLogicalExpression(Expression.Binary(
                                BinaryOperation.AND,
                                statement.condition,
                                tryInvert(Expression.Not(firstStatement.condition))))
                        changed = true
                    }
                }
            }
        } while (changed)
        val label = statement.label
        if (label != null) {
            removeRedundantBreak(statement.consequent, label)
            removeRedundantBreak(statement.alternative, label)
        }
        return tryConvertToExpression(statement)
    }

    internal fun removeRedundantBreak(statements: MutableList<Statement>, label: String) {
        val statement = statements.lastOrNull()
        when (statement) {
            is Statement.Break -> {
                if (statement.label == label) {
                    statements.removeAt(statements.lastIndex)
                }
            }
            is Statement.If -> {
                removeRedundantBreak(statement.consequent, label)
                removeRedundantBreak(statement.alternative, label)
            }
            is Statement.Group -> {
                removeRedundantBreak(statement.body, label)
            }
        }
    }

    internal fun tryConvertToExpression(statement: Statement.If): Statement {
        if (statement.consequent.size != 1 || statement.alternative.size != 1) {
            return statement
        }
        val consequent = statement.consequent[0]
        val alternative = statement.alternative[0]
        if (consequent is Statement.Assignment && alternative is Statement.Assignment) {
            val consequentTarget = consequent.target
            val alternativeTarget = alternative.target
            if (consequentTarget !is Expression.StackVariable || alternativeTarget !is Expression.StackVariable
                    || consequentTarget.index != alternativeTarget.index) {
                return statement
            }

            return Statement.Assignment(consequentTarget, Expression.Conditional(statement.condition,
                    consequent.value, alternative.value))
        } else if (consequent is Statement.Return && alternative is Statement.Return) {
            val consequentValue = consequent.value
            val alternativeValue = alternative.value
            if (consequentValue == null || alternativeValue == null) {
                return statement
            }
            return Statement.Return(Expression.Conditional(statement.condition,
                    consequentValue, alternativeValue))
        } else {
            return statement
        }
    }

    internal fun optimizeLoop(statement: Statement.While): Statement {
        do {
            var changed = false
            val firstStatement = statement.body.firstOrNull()
            val label = statement.label
            if (label != null && firstStatement is Statement.If) {
                if (firstStatement.consequent.size == 1) {
                    val shortCircuit = firstStatement.consequent[0]
                    if (shortCircuit is Statement.Break && shortCircuit.label == statement.label) {
                        statement.body.removeAt(0)
                        statement.condition = tryOptimizeLogicalExpression(Expression.Binary(
                                BinaryOperation.AND,
                                statement.condition,
                                tryInvert(Expression.Not(firstStatement.condition))))
                        changed = true
                    }
                }
            }
        } while (changed)
        val label = statement.label
        if (label != null) {
            removeRedundantContinue(statement.body, label)
        }
        //return tryMakeDoWhile(statement)
        //TODO: repair
        return statement
    }

    internal fun removeRedundantContinue(statements: MutableList<Statement>, label: String) {
        val statement = statements.lastOrNull()
        when (statement) {
            is Statement.Continue -> {
                if (statement.label == label) {
                    statements.removeAt(statements.lastIndex)
                }
            }
            is Statement.If -> {
                removeRedundantContinue(statement.consequent, label)
                removeRedundantContinue(statement.alternative, label)
                if (statement.consequent.isEmpty()) {
                    statement.consequent.addAll(statement.alternative)
                    statement.alternative.clear()
                    statement.condition = tryInvert(Expression.Not(statement.condition))
                }
            }
            is Statement.Group -> {
                removeRedundantContinue(statement.body, label)
            }
        }
    }

    internal fun tryMakeDoWhile(loop: Statement.While): Statement {
        val label = loop.label
        if (loop.condition != Expression.True || loop.body.isEmpty() || label == null) {
            return loop
        }
        val last = loop.body.last()
        if (last !is Statement.If) {
            return loop
        }
        if (last.consequent.size != 1 || !last.alternative.isEmpty()) {
            return loop
        }
        val consequent = last.consequent[0]
        if (consequent !is Statement.Break || consequent.label != label) {
            return loop
        }

        val optimized = Statement.DoWhile(tryInvert(Expression.Not(last.condition)), loop.label)
        optimized.body.addAll(loop.body.dropLast(1))
        return optimized
    }

    internal fun optimizeGroup(group: Statement.Group): Statement {
        val label = group.label
        if (label == null || group.body.isEmpty()) {
            return group
        }
        val first = group.body.first()
        if (first !is Statement.If || first.alternative.isNotEmpty() || first.consequent.isEmpty()) {
            return group
        }
        val last = first.consequent.last()
        if (last !is Statement.Break || last.label != label) {
            return group
        }

        first.alternative.addAll(group.body.drop(1))
        first.consequent.let { it.removeAt(it.lastIndex) }
        removeRedundantBreak(first.alternative, label)
        return first
    }

    internal class StackEntry(val location: Int, val expression: Expression)

    internal fun optimize(expression: Expression, maxStack: Int): Expression {
        return when (expression) {
            is Expression.StackVariable -> {
                val entry = stack.remove(expression.index)
                if (entry != null && expression.index >= maxStack) {
                    target.removeAt(entry.location)
                    for ((key, value) in stack.entries.toTypedArray()) {
                        if (value.location > entry.location) {
                            stack.remove(key)
                        }
                    }
                    entry.expression
                } else {
                    expression
                }
            }
            is Expression.Binary -> {
                expression.right = optimize(expression.right, maxStack)
                expression.left = optimize(expression.left, maxStack)
                tryOptimizeLogicalExpression(expression)
            }
            is Expression.ArithmeticBinary -> {
                expression.right = optimize(expression.right,maxStack)
                expression.left = optimize(expression.left, maxStack)
                expression
            }
            is Expression.BitwiseBinary -> {
                expression.right = optimize(expression.right, maxStack)
                expression.left = optimize(expression.left, maxStack)
                expression
            }
            is Expression.ArithmeticCast -> {
                expression.value = optimize(expression.value, maxStack)
                expression
            }
            is Expression.BitwiseInversion -> {
                expression.value = optimize(expression.value, maxStack)
                expression
            }
            is Expression.ArrayLength -> {
                expression.array = optimize(expression.array, maxStack)
                expression
            }
            is Expression.Cast -> {
                expression.value = optimize(expression.value, maxStack)
                expression
            }
            is Expression.ConstructArray -> {
                for ((i, dim) in expression.dimensions.withIndex().reversed()) {
                    expression.dimensions[i] = optimize(dim, maxStack)
                }
                expression
            }
            is Expression.GetField -> {
                expression.target = optimize(expression.target, maxStack)
                expression
            }
            is Expression.Invoke -> {
                for ((i, arg) in expression.arguments.withIndex().reversed()) {
                    expression.arguments[i] = optimize(arg, maxStack)
                }
                expression.target = optimize(expression.target, maxStack)
                expression
            }
            is Expression.InvokeStatic -> {
                for ((i, arg) in expression.arguments.withIndex().reversed()) {
                    expression.arguments[i] = optimize(arg, maxStack)
                }
                expression
            }
            is Expression.InvokeSpecial -> {
                for ((i, arg) in expression.arguments.withIndex().reversed()) {
                    expression.arguments[i] = optimize(arg, maxStack)
                }
                expression.target = optimize(expression.target, maxStack)
                expression
            }
            is Expression.InstanceOf -> {
                expression.value = optimize(expression.value, maxStack)
                expression
            }
            is Expression.Negate -> {
                expression.value = optimize(expression.value, maxStack)
                expression
            }
            is Expression.Not -> {
                expression.value = optimize(expression.value, maxStack)
                tryInvert(expression)
            }

            is Expression.Conditional -> {
                expression.condition = optimize(expression.condition, maxStack)
                if (expression.consequent is Expression.Conditional
                        && expression.alternative !is Expression.Conditional) {
                    expression.condition = tryInvert(Expression.Not(expression.condition))
                    val tmp = expression.consequent
                    expression.consequent = expression.alternative
                    expression.alternative = tmp
                }
                expression
            }

            is Expression.Construct,
            is Expression.PostIncrement,
            is Expression.PreIncrement,
            is Expression.GetStaticField,
            is Expression.Variable,
            is Expression.True,
            is Expression.False,
            is Expression.IntConstant,
            is Expression.LongConstant,
            is Expression.DoubleConstant,
            is Expression.StringConstant,
            is Expression.TypeConstant,
            is Expression.Null,
            is Expression.Self -> expression
        }
    }

    internal fun tryOptimizeLogicalExpression(expression: Expression.Binary): Expression {
        return when (expression.operation) {
            BinaryOperation.AND -> when {
                expression.left == Expression.True -> expression.right
                expression.right == Expression.True -> expression.left
                else -> expression
            }
            BinaryOperation.OR -> when {
                expression.left == Expression.False -> expression.right
                expression.right == Expression.False -> expression.left
                else -> expression
            }
            else -> expression
        }
    }

    internal fun tryInvert(expression: Expression.Not): Expression {
        val value = expression.value
        return when (value) {
            is Expression.Binary -> {
                val op = when (value.operation) {
                    BinaryOperation.EQUAL -> BinaryOperation.NOT_EQUAL
                    BinaryOperation.NOT_EQUAL -> BinaryOperation.EQUAL
                    BinaryOperation.LESS -> BinaryOperation.GREATER_OR_EQUAL
                    BinaryOperation.LESS_OR_EQUAL -> BinaryOperation.GREATER
                    BinaryOperation.GREATER -> BinaryOperation.LESS_OR_EQUAL
                    BinaryOperation.GREATER_OR_EQUAL -> BinaryOperation.LESS
                    else -> null
                }
                if (op == null) expression else Expression.Binary(op, value.left, value.right)
            }
            is Expression.Not -> value.value
            else -> expression
        }
    }

    internal fun hasIncrement(expression: Expression): Boolean {
        if (expression is Expression.PostIncrement || expression is Expression.PreIncrement) {
            return true
        }
        return expression.nestedExpressions.any { hasIncrement(it) }
    }
}