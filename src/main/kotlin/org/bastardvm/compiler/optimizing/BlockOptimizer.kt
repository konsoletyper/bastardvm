package org.bastardvm.compiler.optimizing

import org.bastardvm.compiler.ast.Block
import org.bastardvm.compiler.ast.Expression
import org.bastardvm.compiler.ast.Statement
import org.bastardvm.compiler.ast.Transition
import org.bastardvm.compiler.common.graph.*
import java.util.*

class BlockOptimizer(start: Block) {
    internal var cfg = start.buildFlowGraph()
    internal val sortedNodes = cfg.topologicalSort()
    internal val nodeIndexes = sortedNodes.withIndex().associate { it.value to it.index }
    internal var domTree = cfg.buildDominatorTree { it.value }
    internal val stack = ArrayDeque<() -> Unit>()
    internal val labels = hashMapOf<Block, (MutableList<Statement>) -> Unit>()
    internal var labelIndex = 0
    internal var loopNodes = hashSetOf<Graph.Node<Block>>()

    fun optimize(): Block {
        val resultBlock = Block()
        stack.add { singleStep(domTree.root, resultBlock.body) }
        while (stack.isNotEmpty()) {
            stack.pop()()
        }
        return resultBlock
    }

    internal fun singleStep(domNode: LCATree.Node<Block>, optimized: MutableList<Statement>) {
        val node = cfg.nodeMap[domNode.value]!!
        var target = optimized
        val block = node.value

        // Check whether this node is a loop header
        val loopTails = node.predecessors.filter { domNode.dominates(it) }
        if (loopTails.isNotEmpty()) {
            val loopStmt = Statement.While(Expression.True)
            val label = "label${labelIndex++}"
            loopStmt.label = label

            // Jump back to loop header is expressed as continue statement
            addLabel(block) { it.add(Statement.Continue(label)) }

            // Build loop exits
            val currentLoopNodes = findLoop(domNode, loopTails)
            loopNodes.addAll(currentLoopNodes)
            val trueLoopExits = currentLoopNodes.findExits().filter { it.value !in labels }
            val loopExits = extendLoopExits(trueLoopExits, domNode).sortedByDescending { nodeIndexes[it]!! }
            target = addExits(loopExits, target, label)
            target.add(loopStmt)
            target = loopStmt.body
        }

        target.addAll(block.body)

        val transition = block.transition
        when (transition) {
            is Transition.Empty -> { }

            is Transition.Jump -> {
                val label = labels[transition.target]
                if (label != null) {
                    label(target)
                } else if (domNode.successors.isNotEmpty()) {
                    deferSingleStep(domNode.successors.first(), target)
                }
            }

            is Transition.Conditional -> conditional(domNode, transition, target)
            is Transition.Switch -> switchCase(domNode, transition, target)
        }
    }

    internal fun conditional(domNode: LCATree.Node<Block>, transition: Transition.Conditional,
            optimized: MutableList<Statement>) {
        val ownsTrue = cfg.nodeMap[transition.whenTrue]!!.predecessors.size == 1
        val ownsFalse = cfg.nodeMap[transition.whenFalse]!!.predecessors.size == 1
        val labelTrue = labels[transition.whenTrue]
        val labelFalse = labels[transition.whenFalse]

        val branching = Statement.If(transition.condition)

        // TODO: generalize with switch/case
        val exits = domNode.successors
                .map { cfg.nodeMap[it.value]!! }
                .filter { it.value !in labels }
                .filter { !ownsTrue || it.value != transition.whenTrue }
                .filter { !ownsFalse || it.value != transition.whenFalse }
                .toSet()
                .sortedByDescending { nodeIndexes[it] }

        val target = if (exits.isNotEmpty()) {
            val label = "label${labelIndex++}"
            branching.label = label
            addExits(exits, optimized, label)
        } else {
            optimized
        }

        if (labelTrue == null && labelFalse == null) {
            if (ownsTrue && ownsFalse) {
                deferSingleStep(domTree.nodeMap[transition.whenTrue]!!, branching.consequent)
                deferSingleStep(domTree.nodeMap[transition.whenFalse]!!, branching.alternative)
            } else if (ownsTrue) {
                deferSingleStep(domTree.nodeMap[transition.whenTrue]!!, branching.consequent)
            } else if (ownsFalse) {
                branching.condition = Expression.Not(branching.condition)
                deferSingleStep(domTree.nodeMap[transition.whenFalse]!!, branching.consequent)
            }
        } else if (labelTrue != null && labelFalse != null) {
            labelTrue(branching.consequent)
            labelFalse(branching.alternative)
        } else if (labelTrue != null) {
            labelTrue(branching.consequent)
            deferSingleStep(domTree.nodeMap[transition.whenFalse]!!, target)
        } else if (labelFalse != null) {
            branching.condition = Expression.Not(branching.condition)
            labelFalse(branching.consequent)
            deferSingleStep(domTree.nodeMap[transition.whenTrue]!!, target)
        }

        target.add(branching)
    }

    internal fun switchCase(domNode: LCATree.Node<Block>, transition: Transition.Switch,
            optimized: MutableList<Statement>) {
        val jumpTargets = (transition.clauses.map { it.target } + transition.defaultTarget).toSet()

        // Find fallthrough cases
        val head = cfg.nodeMap[domNode.value]!!
        val successors =(transition.clauses.map { it.target } + transition.defaultTarget)
                .map { cfg.nodeMap[it]!! }
                .sortedBy { nodeIndexes[it] }
        val fallThroughTargets = successors.zip(successors.drop(1))
                .filter { it.second.predecessors.size == 2 }
                .filter { it.first.value !in labels && domNode.dominates(it.first) }
                .filter { it.second.value !in labels && domNode.dominates(it.second) }
                .filter { it.first.dominates(it.second.predecessors.filter { it != head }.first()) }
                .map { it.second }
                .toSet()

        val exits = domNode.successors
                .map { cfg.nodeMap[it.value]!! }
                .filter { it.value !in labels && it !in fallThroughTargets }
                .filter { it.predecessors.size != 1 || it.value !in jumpTargets }
                .toSet()
                .sortedByDescending { nodeIndexes[it] }

        val switchStatement = Statement.Switch(transition.condition)

        val target = if (exits.isNotEmpty()) {
            val label = "label${labelIndex++}"
            switchStatement.label = label
            addExits(exits, optimized, label)
        } else {
            optimized
        }

        val clauses = transition.clauses
                .groupBy { it.target }
                .map { Pair(cfg.nodeMap[it.key]!!, it.value) }
                .plus(Pair(cfg.nodeMap[transition.defaultTarget]!!, emptyList()))
                .sortedBy { nodeIndexes[it.first] }

        // Defer clauses in direct order, so they actually get processed in reverse order.
        // It's needed to allow fall-through cases work correctly
        for (clause in clauses) {
            val statementClauses = clause.second.map { Statement.SwitchClause(it.number) }
            switchStatement.clauses.addAll(statementClauses)
            val body = statementClauses.lastOrNull()?.body ?: switchStatement.defaultBody
            val label = labels[clause.first.value]
            if (label == null) {
                if (clause.first in fallThroughTargets) {
                    defer { addLabel(clause.first.value, { }) }
                }
                deferSingleStep(domTree.nodeMap[clause.first.value]!!, body)
            } else {
                label(body)
            }
        }

        target.add(switchStatement)
    }

    internal fun addExits(exits: List<Graph.Node<Block>>, output: MutableList<Statement>,
            innermostLabel: String): MutableList<Statement> {
        var currentTarget = output
        val labels = ArrayDeque<String>()
        for (exit in exits.dropLast(1)) {
            val label = "label${labelIndex++}"
            labels.addLast(label)
            addLabel(exit.value) { it.add(Statement.Break(label)) }
        }
        if (exits.isNotEmpty()) {
            addLabel(exits.last().value) { it.add(Statement.Break(innermostLabel)) }
        }
        val outermostExit = exits.firstOrNull()
        if (outermostExit != null) {
            val outerTarget = currentTarget
            deferSingleStep(domTree.nodeMap[outermostExit.value]!!, outerTarget)
            for (exit in exits.drop(1)) {
                val label = labels.removeFirst()
                val group = Statement.Group(label)
                val groupTarget = currentTarget
                //append(groupTarget, group)
                currentTarget = group.body
                val target = currentTarget
                groupTarget.add(group)
                deferSingleStep(domTree.nodeMap[exit.value]!!, target)
            }
        }
        return currentTarget
    }

    internal fun addLabel(block: Block, label: (MutableList<Statement>) -> Unit) {
        labels[block] = label
        defer { labels.remove(block) }
    }

    internal fun findLoop(dom: LCATree.Node<Block>, tails: Iterable<Graph.Node<Block>>): Set<Graph.Node<Block>> {
        val visited = hashSetOf<Graph.Node<Block>>()
        val stack = ArrayDeque<Graph.Node<Block>>()
        stack.addAll(tails)
        while (stack.isNotEmpty()) {
            val node = stack.pop()
            if (visited.add(node)) {
                for (predecessor in node.predecessors.filter { dom.dominates(it) }) {
                    stack.push(predecessor)
                }
            }
        }
        return visited
    }

    /**
     * Extends loop with additional nodes beyond loop exits. It is possible for these loop exits
     * which are dominated by loop head. In this case we can include dominance frontier of an exit.
     *
     * Example:
     *
     * ```
     * while (true) {
     *     if (x) {
     *         A;
     *         break;
     *     }
     *     if (y) {
     *         B;
     *         break;
     *     }
     * }
     * C;
     * ```
     *
     * in this case both *A* and *B* are beyond loop in terms of CFG, however we can place them inside
     * loop in AST, since they are dominated by loop head.
     *
     * There is special case when loop exit dominates all reachable nodes. This means that we
     * can include entire branch into loop body. Usually it looks like this:
     *
     * ```
     * * while (true) {
     *     if (x) {
     *         A;
     *         return;
     *     }
     *     if (y) {
     *         break;
     *     }
     * }
     * B;
     * ```
     *
     * Both *A* and *B* exits can be placed inside loop (we can eliminate break from `y` condition).
     */
    internal fun extendLoopExits(exits: Iterable<Graph.Node<Block>>,
            dom: LCATree.Node<Block>): Iterable<Graph.Node<Block>> {
        val (internal, external) = exits.partition { dom.dominates(it) && it.value !in labels }
        val internalWithFrontiers = internal.map {
            val (frontiers, sizeEstimation) = frontiers(it)
            Triple(it, frontiers, sizeEstimation)
        }
        val (full, partial) = internalWithFrontiers.partition { it.second.none() }
        return if (external.none() && partial.none() && full.any()) {
            listOf(full.first().first) + full.sortedByDescending { it.third }.drop(1).map { it.second }.flatten()
        } else {
            external + partial.map { it.second }.flatten()
        }.toSet()
    }

    internal fun frontiers(start: Graph.Node<Block>): Pair<Iterable<Graph.Node<Block>>, Int> {
        val visited = hashSetOf<Graph.Node<Block>>()
        val frontiers = hashSetOf<Graph.Node<Block>>()
        val stack = ArrayDeque<Graph.Node<Block>>()
        stack.push(start)
        while (stack.isNotEmpty()) {
            val node = stack.pop()
            if (visited.add(node)) {
                if (start.dominates(node)) {
                    stack.addAll(node.successors)
                } else if (node.value !in labels) {
                    frontiers.add(node)
                }
            }
        }
        return Pair(frontiers, visited.map { it.value.body.size }.sum())
    }

    internal fun Graph.Node<Block>.dominates(node: Graph.Node<Block>) =
            domTree.nodeMap[this.value]!!.isAncestorOf(domTree.nodeMap[node.value]!!)

    internal fun LCATree.Node<Block>.dominates(node: Graph.Node<Block>) =
            this.isAncestorOf(domTree.nodeMap[node.value]!!)

    internal fun deferSingleStep(domNode: LCATree.Node<Block>, optimized: MutableList<Statement>) {
        defer { singleStep(domNode, optimized ) }
    }

    internal fun defer(action: () -> Unit) {
        stack.push(action)
    }
}