package org.bastardvm.compiler.optimizing

import org.bastardvm.compiler.ast.Block
import org.bastardvm.compiler.common.graph.Graph
import org.bastardvm.compiler.common.graph.MutableGraph

fun Block.buildFlowGraph(): Graph<Block> = MutableGraph<Block>().let {
    buildFlowGraph(this, it, hashSetOf())
    it
}


private fun buildFlowGraph(block: Block, graph: MutableGraph<Block>, visited: MutableSet<Block>) {
    if (!visited.add(block)) {
        return
    }
    val node = graph.nodeOf(block)
    for (successor in block.successors) {
        node.connectTo(graph.nodeOf(successor))
        if (successor !in visited) {
            buildFlowGraph(successor, graph, visited)
        }
    }
}
