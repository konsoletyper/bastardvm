package org.bastardvm.compiler.optimizing

import org.bastardvm.compiler.ast.Statement

fun Iterable<Statement>.removeLabels() {
    removeLabels(usedLabels())
}
internal fun Iterable<Statement>.removeLabels(used: Set<String>) {
    for (statement in this) {
        when (statement) {
            is Statement.Group -> {
                statement.label?.let { if (it !in used) statement.label = null }
                statement.body.removeLabels(used)
            }
            is Statement.While -> {
                statement.label?.let { if (it !in used) statement.label = null }
                statement.body.removeLabels(used)
            }
            is Statement.DoWhile -> {
                statement.label?.let { if (it !in used) statement.label = null }
                statement.body.removeLabels(used)
            }
            is Statement.Switch -> {
                statement.label?.let { if (it !in used) statement.label = null }
                statement.clauses.forEach { it.body.removeLabels(used) }
            }
            is Statement.If -> {
                statement.label?.let { if (it !in used) statement.label = null }
                statement.consequent.removeLabels(used)
                statement.alternative.removeLabels(used)
            }
        }
    }
}

fun Iterable<Statement>.usedLabels(): Set<String> {
    return this.map { statement ->
        when (statement) {
            is Statement.Group -> statement.body.usedLabels()
            is Statement.If -> statement.consequent.usedLabels() + statement.alternative.usedLabels()
            is Statement.While -> statement.body.usedLabels()
            is Statement.DoWhile -> statement.body.usedLabels()
            is Statement.Switch -> statement.clauses.map { it.body.usedLabels() }.flatten()
            is Statement.Break -> listOf(statement.label)
            is Statement.Continue -> listOf(statement.label)
            is Statement.Return,
            is Statement.Throw,
            is Statement.Assignment,
            is Statement.ExpressionStatement -> emptyList<String>()
        }
    }.flatten().toSet()
}
