package org.bastardvm.compiler

import org.bastardvm.compiler.ast.Expression
import org.bastardvm.compiler.ast.Statement
import org.bastardvm.compiler.diagnostics.ProblemLocation
import org.bastardvm.compiler.diagnostics.Reporter
import org.bastardvm.compiler.diagnostics.SourceLocation
import org.bastardvm.compiler.model.FullFieldReference
import org.bastardvm.compiler.model.FullMethodReference
import org.bastardvm.compiler.model.Model
import org.bastardvm.compiler.model.Type

class Normializer(val model: Model, val reporter: Reporter, val location: FullMethodReference) {
    fun normalizeAll(statements: List<Statement>) {
        statements.forEach { normalize(it) }
    }

    fun normalize(statement: Statement) {
        statement.expressions.forEach { normalize(it) }
    }

    fun normalize(expression: Expression) {
        when (expression) {
            is Expression.InvokeStatic -> expression.method = normalize(expression.location, expression.method)
            is Expression.Invoke -> expression.method = normalize(expression.location, expression.method)
            is Expression.InvokeSpecial -> expression.method = normalize(expression.location, expression.method)
            is Expression.GetField -> expression.field = normalize(expression.location, expression.field)
        }
        expression.nestedExpressions.forEach { normalize(it) }
    }

    fun normalize(source: SourceLocation?, method: FullMethodReference): FullMethodReference {
        val target = method.target
        val foundMethod = if (target is Type.Obj) {
            val cls = model[target.className]
            if (cls == null) {
                null
            } else {
                val methodModel = cls.findMethod(method.nameWithSignature)
                if (methodModel != null) {
                    FullMethodReference(Type.Obj(methodModel.owner.name), methodModel.reference)
                } else {
                    null
                }
            }
        } else {
            method
        }
        return if (foundMethod == null) {
            reporter.error(source, ProblemLocation.Method(location), "Method nof found: $method")
            method
        } else {
            foundMethod
        }
    }

    fun normalize(source: SourceLocation?, field: FullFieldReference): FullFieldReference {
        val cls = model.get(field.className)
        val foundField = if (cls == null) {
            null
        } else {
            val fieldModel = cls.findField(field.nameAndType)
            if (fieldModel != null) {
                FullFieldReference(fieldModel.owner.name, fieldModel.reference)
            } else {
                null
            }
        }
        return if (foundField == null) {
            reporter.error(source, ProblemLocation.Method(location), "Field nof found: $field")
            field
        } else {
            foundField
        }
    }
}