package org.bastardvm.compiler.ast

enum class BitwiseType {
    INT,
    LONG
}
