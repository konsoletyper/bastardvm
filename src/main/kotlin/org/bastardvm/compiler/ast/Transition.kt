package org.bastardvm.compiler.ast

sealed class Transition {
    object Empty: Transition()

    class Jump(val target: Block) : Transition()

    class Conditional(val condition: Expression, val whenTrue: Block, val whenFalse: Block) : Transition()

    class Switch(val condition: Expression, val defaultTarget: Block, clauses: List<SwitchClause>) : Transition() {
        val clauses = clauses.toList()
    }

    class SwitchClause(val number: Int, val target: Block)
}
