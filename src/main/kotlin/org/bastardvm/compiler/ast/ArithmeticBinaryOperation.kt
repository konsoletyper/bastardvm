package org.bastardvm.compiler.ast

enum class ArithmeticBinaryOperation {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    REMAINDER,
    COMPARE
}
