package org.bastardvm.compiler.ast

enum class ArithmeticType {
    INT,
    DOUBLE,
    LONG
}