package org.bastardvm.compiler.ast

import org.bastardvm.compiler.diagnostics.SourceLocation

sealed class Statement {
    class If(var condition: Expression, var label: String? = null) : Statement() {
        val consequent: MutableList<Statement> = arrayListOf()
        val alternative: MutableList<Statement> = arrayListOf()
    }

    class While(var condition: Expression, var label: String? = null) : Statement() {
        val body: MutableList<Statement> = arrayListOf()
    }

    class DoWhile(var condition: Expression, var label: String? = null) : Statement() {
        val body: MutableList<Statement> = arrayListOf()
    }

    class Group(var label: String? = null) : Statement() {
        val body: MutableList<Statement> = arrayListOf()
    }

    class Switch(var condition: Expression, var label: String? = null) : Statement() {
        val clauses: MutableList<SwitchClause> = arrayListOf()
        val defaultBody: MutableList<Statement> = arrayListOf()
    }

    class SwitchClause(var number: Int) {
        val body: MutableList<Statement> = arrayListOf()
    }

    class Return(var value: Expression? = null) : Statement() {
        var location: SourceLocation? = null
    }

    class Throw(var value: Expression) : Statement() {
        var location: SourceLocation? = null
    }

    class Break(var label: String) : Statement() {
        var location: SourceLocation? = null
    }

    class Continue(var label: String) : Statement() {
        var location: SourceLocation? = null
    }

    class Assignment(var target: Expression, var value: Expression) : Statement()

    class ExpressionStatement(var expression: Expression) : Statement()

    val nestedStatements: List<Statement>
        get() = when (this) {
            is If -> consequent + alternative
            is While -> body
            is DoWhile -> body
            is Group -> body
            is Switch -> clauses.map { it.body }.reduce { a : Iterable<Statement>, b -> a + b }.toList()
            is Return,
            is Throw,
            is Break,
            is Continue,
            is Assignment,
            is ExpressionStatement -> listOf()
        }

    val expressions: List<Expression>
        get() = when(this) {
            is If -> listOf(condition)
            is While -> listOf(condition)
            is DoWhile -> listOf(condition)
            is Switch -> listOf(condition)
            is Return -> value.let { if (it != null) listOf(it) else listOf() }
            is Throw -> listOf(value)
            is Group,
            is Break,
            is Continue -> listOf()
            is Assignment -> listOf(target, value)
            is ExpressionStatement -> listOf(expression)
        }
}
