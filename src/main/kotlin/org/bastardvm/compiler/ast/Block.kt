package org.bastardvm.compiler.ast

class Block {
    var body: MutableList<Statement> = arrayListOf()
    var transition: Transition = Transition.Empty
    var stackSizeAfter = 0

    val singleExpression: Expression?
        get() {
            return if (body.size == 1 && body[0] is Statement.ExpressionStatement)
                (body[0] as Statement.ExpressionStatement).expression else null
        }

    val successors: List<Block>
        get() = transition.let {
            when (it) {
                is Transition.Empty -> listOf<Block>()
                is Transition.Conditional -> listOf(it.whenTrue, it.whenFalse)
                is Transition.Jump -> listOf(it.target)
                is Transition.Switch -> it.clauses.map { it.target } + it.defaultTarget
            }
        }

    fun findReachableBlocks() = arrayListOf<Block>().let { findReachableBlocks(hashSetOf(), it); it }

    private fun findReachableBlocks(found: MutableSet<Block>, list: MutableList<Block>) {
        if (found.add(this)) {
            list.add(this)
            successors.forEach { it.findReachableBlocks(found, list) }
        }
    }
}
