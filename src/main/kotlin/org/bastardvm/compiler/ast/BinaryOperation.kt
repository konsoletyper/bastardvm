package org.bastardvm.compiler.ast

enum class BinaryOperation {
    AND,
    OR,
    ARRAY_ELEMENT,
    EQUAL,
    NOT_EQUAL,
    LESS,
    LESS_OR_EQUAL,
    GREATER,
    GREATER_OR_EQUAL
}
