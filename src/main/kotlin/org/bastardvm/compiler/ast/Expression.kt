package org.bastardvm.compiler.ast

import org.bastardvm.compiler.diagnostics.SourceLocation
import org.bastardvm.compiler.model.*

sealed class Expression {
    var location: SourceLocation? = null

    class Variable(var name: String) : Expression()

    class StackVariable(var index: Int) : Expression()

    class IntConstant(var value: Int) : Expression()

    class LongConstant(var value: Long) : Expression()

    class DoubleConstant(var value: Double) : Expression()

    class StringConstant(var value: String) : Expression()

    class TypeConstant(var value: Type) : Expression()

    object Null : Expression()

    object Self : Expression()

    object True : Expression()

    object False : Expression()

    class ArithmeticBinary(var operation: ArithmeticBinaryOperation, var left: Expression, var right: Expression,
                           var type: ArithmeticType = ArithmeticType.INT) : Expression()

    class BitwiseBinary(var operation: BitwiseBinaryOperation, var left: Expression, var right: Expression,
                        var type: BitwiseType = BitwiseType.INT) : Expression()

    class Negate(var value: Expression, var type: ArithmeticType = ArithmeticType.INT) : Expression()

    class BitwiseInversion(var value: Expression, var type: BitwiseType = BitwiseType.INT) : Expression()

    class Binary(var operation: BinaryOperation, var left: Expression, var right: Expression) : Expression()

    class Not(var value: Expression) : Expression()

    class ArrayLength(var array: Expression) : Expression()

    class GetField(var target: Expression, var field: FullFieldReference) : Expression()

    class GetStaticField(var field: FullFieldReference, var init: Boolean = false) : Expression()

    class Invoke(var target: Expression, var method: FullMethodReference, arguments: List<Expression>) : Expression() {
        val arguments = arguments.toMutableList()
    }

    class InvokeSpecial(var target: Expression, var method: FullMethodReference, arguments: List<Expression>)
    : Expression() {
        val arguments = arguments.toMutableList()
    }

    class InvokeStatic(var method: FullMethodReference, arguments: List<Expression>) : Expression() {
        val arguments = arguments.toMutableList()
    }

    class Construct(var className: String) : Expression()

    class ConstructArray(var type: Type, val dimensions: MutableList<Expression>) : Expression()

    class InstanceOf(var value: Expression, var type: Type) : Expression()

    class Cast(var value: Expression, var type: Type) : Expression()

    class PreIncrement(var variable: String, val amount: Int) : Expression()

    class PostIncrement(var variable: String, val amount: Int) : Expression()

    class ArithmeticCast(var source: ArithmeticType, var target: ArithmeticType, var value: Expression) : Expression()

    class Conditional(var condition: Expression, var consequent: Expression, var alternative: Expression)
    : Expression()

    val nestedExpressions: List<Expression>
        get() = when (this) {
            is Variable,
            is StackVariable,
            is IntConstant,
            is LongConstant,
            is DoubleConstant,
            is StringConstant,
            is TypeConstant,
            is Null,
            is Self,
            is True,
            is False -> listOf()

            is Binary -> listOf(left, right)
            is BitwiseBinary -> listOf(left, right)
            is BitwiseInversion -> listOf(value)
            is ArithmeticBinary -> listOf(left, right)
            is Negate -> listOf(value)
            is Not -> listOf(value)
            is ArrayLength -> listOf(array)

            is GetField -> listOf(target)
            is GetStaticField -> listOf()
            is Invoke -> listOf(target, *arguments.toTypedArray())
            is InvokeSpecial -> listOf(target, *arguments.toTypedArray())
            is InvokeStatic -> arguments
            is Construct -> listOf()
            is ConstructArray -> dimensions.toList()

            is InstanceOf -> listOf(value)
            is Cast -> listOf(value)
            is ArithmeticCast -> listOf(value)

            is PreIncrement, is PostIncrement -> listOf()

            is Conditional -> listOf(condition, consequent, alternative)
        }
}

