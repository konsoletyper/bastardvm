package org.bastardvm.compiler.common.graph

import java.util.*

fun <T> Set<Graph.Node<T>>.findExits(): Set<Graph.Node<T>> {
    val exits = hashSetOf<Graph.Node<T>>()
    for (node in this) {
        exits.addAll(node.successors.filter { !this.contains(it) })
    }
    return exits
}

fun <T> Graph<T>.topologicalSort(): List<Graph.Node<T>> {
    val start = this.nodes.filter { it.predecessors.isEmpty() }
    val stack = ArrayDeque<() -> Unit>()
    val visited = hashSetOf<Graph.Node<T>>()
    val sorted = arrayListOf<Graph.Node<T>>()

    fun step(node: Graph.Node<T>) {
        if (!visited.add(node)) {
            return
        }
        stack.push { sorted.add(node) }
        for (successor in node.successors.filter { it !in visited }) {
            stack.push { step(successor) }
        }
    }

    start.forEach { stack.push { step(it) } }
    while (stack.isNotEmpty()) {
        stack.pop()()
    }
    return sorted.reversed()
}

fun <T, S> Graph<T>.buildDominatorTree(start: Graph.Node<T> = this.nodes[0],
        mapper: (Graph.Node<T>) -> S): LCATree<S> {
    if (start.graph != this) {
        throw IllegalArgumentException("Node does not belong to this graph")
    }
    val builder = DominatorTreeBuilder(this)
    builder.build(start)

    val domGraph = MutableGraph<T>()
    for (i in 0.until(this.nodes.size)) {
        domGraph.nodeOf(this.nodes[i].value)
    }
    for (i in 0.until(builder.dominators.size)) {
        if (i == start.index) {
            continue
        }
        val v = domGraph.nodes[i]
        val idom = domGraph.nodes[builder.dominators[i]]
        idom.connectTo(v)
    }

    val tree = MutableLCATree(mapper(start))
    val stack = ArrayDeque<Graph.Node<T>>()
    stack.add(domGraph.nodes[start.index])
    while (stack.isNotEmpty()) {
        val domNode = stack.remove()
        val node = mapper(this.nodes[domNode.index])
        for (successor in domNode.successors) {
            tree.nodeMap[node]!!.appendChild(mapper(this.nodes[successor.index]))
            stack.push(domGraph.nodes[successor.index])
        }
    }

    return tree
}

internal class DominatorTreeBuilder<T>(val graph: Graph<T>) {
    val semidominators = IntArray(graph.nodes.size)
    val vertices = IntArray(graph.nodes.size)
    val parents = IntArray(graph.nodes.size)
    val ancestors = IntArray(graph.nodes.size)
    val labels = 0.until(graph.nodes.size).toList().toIntArray()
    val dominators = IntArray(graph.nodes.size)
    val bucket = generateSequence { arrayListOf<Int>() }.take(graph.nodes.size).toMutableList<MutableList<Int>?>()
    val path = IntArray(graph.nodes.size)
    var effectiveSize = 0

    fun build(start: Graph.Node<T>) {
        ancestors.fill(-1)
        dfs(start)
        dominators[start.index] = -1
        for (i in (effectiveSize - 1).downTo(0)) {
            val w = vertices[i]
            if (parents[w] < 0) {
                continue
            }
            for (u in graph.nodes[w].predecessors.asSequence().map { eval(it) }.filter { semidominators[it] >= 0 }) {
                semidominators[w] = Math.min(semidominators[w], semidominators[u])
            }
            bucket[vertices[semidominators[w]]]!!.add(w)
            link(parents[w], w)
            for (v in bucket[w]!!) {
                val u = eval(graph.nodes[v])
                dominators[v] = if (semidominators[u] < semidominators[v]) u else parents[w]
            }
            bucket[w] = null
        }
        for (w in graph.nodes.indices.map { vertices[it] }.filter { it >= 0 && parents[it] >= 0 }) {
            if (dominators[w] != vertices[semidominators[w]]) {
                dominators[w] = dominators[dominators[w]]
            }
        }
    }

    fun link(v: Int, w: Int) {
        ancestors[w] = v
    }

    fun eval(node: Graph.Node<T>): Int {
        var v = node.index
        var ancestor = ancestors[v]
        if (ancestor == -1) {
            return v
        }
        var i = 0
        while (ancestor >= 0) {
            path[i++] = v
            v = ancestor
            ancestor = ancestors[v]
        }
        ancestor = v
        while (--i >= 0) {
            v = path[i]
            if (semidominators[labels[v]] > semidominators[labels[ancestor]]) {
                labels[v] = labels[ancestor]
            }
            ancestors[v] = ancestor
            ancestor = v
        }
        return labels[v]
    }

    fun dfs(start: Graph.Node<T>) {
        semidominators.fill(-1)
        vertices.fill(-1)
        val stack = ArrayDeque<Graph.Node<T>>(graph.nodes.size)
        stack.push(start)
        parents[start.index] = -1
        var i = 0
        while (!stack.isEmpty()) {
            val v = stack.pop()
            if (semidominators[v.index] >= 0) {
                continue
            }
            semidominators[v.index] = i
            vertices[i++] = v.index
            for (w in v.successors) {
                if (semidominators[w.index] < 0) {
                    parents[w.index] = v.index
                    stack.push(w)
                }
            }
        }
        effectiveSize = i
    }
}