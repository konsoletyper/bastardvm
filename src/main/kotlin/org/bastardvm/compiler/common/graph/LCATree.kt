package org.bastardvm.compiler.common.graph

interface LCATree<T> : Tree<T> {
    override val nodes: List<Node<T>>
    override val nodeMap: Map<T, Node<T>>
    override val root: Node<T>

    interface Node<S> : Tree.Node<S> {
        override val parent: Node<S>?
        override val successors: Set<Node<S>>
        override val predecessors: Set<Node<S>>
        override val graph: LCATree<S>
        fun lca(other: Node<S>): Node<S>

        fun isAncestorOf(other: Node<S>) = lca(other) == this
        fun hasAncestor(other: Node<S>) = lca(other) == other
    }
}
