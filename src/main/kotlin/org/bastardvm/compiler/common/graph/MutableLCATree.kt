package org.bastardvm.compiler.common.graph

/**
 * Tree that is capable of searching of least common ancestor (LCA) of two nodes.
 * The complexity of searching is log(H), where H is the length of the longest path in the tree.
 * The tree supports only addition operation (see [Node.appendChild]).
 */
class MutableLCATree<T>(rootValue: T) : LCATree<T> {
    internal val nodesImpl = arrayListOf<Node<T>>()
    internal val nodeMapImpl = hashMapOf<T, Node<T>>()
    override val nodes: List<Node<T>>
        get() = nodesImpl
    override val nodeMap: Map<T, Node<T>>
        get() = nodeMapImpl
    override val root: Node<T>

    init {
        root = Node(rootValue, 0, null, this)
        nodesImpl.add(root)
        nodeMapImpl[rootValue] = root
    }

    class Node<S> internal constructor(override val value: S, override val index: Int,
            override val parent: Node<S>?, override val graph: MutableLCATree<S>)
    : LCATree.Node<S> {
        override val predecessors = if (parent != null) setOf(parent) else emptySet()
        internal val successorsImpl = hashSetOf<Node<S>>()
        override val successors: Set<Node<S>>
            get() = successorsImpl
        private val depth: Int
        private val pathToRoot: List<Node<S>>

        init {
            depth = if (parent == null) 0 else parent.depth + 1
            val logDepth = getLogDepth(depth)
            val pathToRootBuilder = arrayListOf<Node<S>>()
            if (logDepth > 0) {
                var ancestor = parent!! // we get to this branch only if current node is not the root
                pathToRootBuilder.add(ancestor)
                for (i in 1.until(logDepth)) {
                    ancestor = ancestor.pathToRoot[i - 1]
                    pathToRootBuilder.add(ancestor)
                }
            }
            pathToRoot = pathToRootBuilder.toMutableList()
        }

        private fun getLogDepth(depth: Int): Int {
            var result = 0
            var d = depth
            while (d > 0) {
                d /= 2
                result++
            }
            return result
        }

        fun appendChild(value: S): Node<S> {
            if (value in graph.nodeMapImpl) {
                throw IllegalArgumentException("Value $value is already in the tree")
            }
            val child = Node(value, graph.nodesImpl.size, this, graph)
            graph.nodeMapImpl[value] = child
            graph.nodesImpl.add(child)
            successorsImpl.add(child)
            return child
        }

        override fun lca(other: LCATree.Node<S>): Node<S> {
            if (other !is Node<S> || graph != other.graph) {
                throw IllegalArgumentException("Nodes are from different trees")
            }
            if (this == other) {
                return this
            }

            var (a, b) = if (depth > other.depth) Pair(this, other) else Pair(other, this)
            if (a.depth != b.depth) {
                var h = a.depth - b.depth
                var diff = 1
                var logDiff = 0
                while (diff <= h) {
                    ++logDiff
                    diff *= 2
                }
                --logDiff
                diff /= 2
                while (h > 0) {
                    h -= diff
                    a = a.pathToRoot[logDiff]
                    while (diff > h) {
                        diff /= 2
                        --logDiff
                    }
                }
            }
            if (a == b) {
                return a
            }

            while (true) {
                var i = 0
                val len = Math.min(a.pathToRoot.size, b.pathToRoot.size)
                while (i < len) {
                    val p = a.pathToRoot[i]
                    val q = b.pathToRoot[i]
                    if (p == q) {
                        break
                    }
                    ++i
                }
                if (--i < 0) {
                    return a.pathToRoot[0]
                }
                a = a.pathToRoot[i]
                b = b.pathToRoot[i]
                if (a == b) {
                    return a
                }
            }
        }
    }
}
