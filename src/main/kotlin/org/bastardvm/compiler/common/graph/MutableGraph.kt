package org.bastardvm.compiler.common.graph

class MutableGraph<T> : Graph<T> {
    private val nodesImpl = arrayListOf<Node<T>>()
    private val nodeMapImpl = hashMapOf<T, Node<T>>()
    override val nodes: List<Node<T>>
        get() = nodesImpl
    override val nodeMap: Map<T, Node<T>>
        get() = nodeMapImpl

    fun nodeOf(value: T): Node<T> {
        var node = nodeMapImpl[value]
        if (node == null) {
            node = Node(value, nodeMapImpl.size, this)
            nodeMapImpl[value] = node
            nodesImpl.add(node)
        }
        return node
    }

    class Node<S> internal constructor(override val value: S, override val index: Int,
            override val graph: MutableGraph<S>) : Graph.Node<S> {
        private val predecessorsImpl = hashSetOf<Node<S>>()
        private val successorsImpl = hashSetOf<Node<S>>()
        override val predecessors: Set<Node<S>>
            get() = predecessorsImpl
        override val successors: Set<Node<S>>
            get() = successorsImpl

        fun connectTo(successor: Node<S>) {
            if (successor.graph != graph) {
                throw IllegalArgumentException("Can't connect nodes from different graphs")
            }
            if (successorsImpl.add(successor)) {
                successor.predecessorsImpl.add(this)
            }
        }

        fun disconnectFrom(successor: Node<S>) {
            if (successorsImpl.remove(successor)) {
                successor.predecessorsImpl.remove(this)
            }
        }

        fun removePredecessor(predecessor: Node<S>) {
            predecessor.disconnectFrom(this)
        }
    }
}
