package org.bastardvm.compiler.common.graph

interface Tree<T> : Graph<T> {
    override val nodes: List<Node<T>>
    override val nodeMap: Map<T, Node<T>>
    val root: Node<T>

    interface Node<S> : Graph.Node<S> {
        override val predecessors: Set<Node<S>>
        override val successors: Set<Node<S>>
        override val graph: Tree<S>
        val parent: Node<S>?
    }
}