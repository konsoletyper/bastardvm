package org.bastardvm.compiler.common.graph

interface Graph<T> {
    val nodes: List<Node<T>>
    val nodeMap: Map<T, Node<T>>

    interface Node<S> {
        val value: S
        val index: Int
        val predecessors: Set<Node<S>>
        val successors: Set<Node<S>>
        val graph: Graph<S>
    }
}
