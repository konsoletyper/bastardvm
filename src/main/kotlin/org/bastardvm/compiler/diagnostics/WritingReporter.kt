package org.bastardvm.compiler.diagnostics

import java.io.Writer

class WritingReporter(val writer: Writer) : Reporter {
    override fun error(source: SourceLocation?, problemLocation: ProblemLocation, message: String) {
        report("ERROR", source, problemLocation, message)
    }

    override fun warn(source: SourceLocation?, problemLocation: ProblemLocation, message: String) {
        report("WARN", source, problemLocation, message)
    }

    fun report(severity: String, source: SourceLocation?, location: ProblemLocation, message: String) {
        val sb = StringBuilder()
        sb.append("[").append(severity).append("] at ")

        sb.append(when (location) {
            is ProblemLocation.Class -> location.className
            is ProblemLocation.Method -> location.method
            is ProblemLocation.Field -> location.field
        })

        val locationString = if (source == null) {
            "unknown location"
        } else {
            val line = source.lineNumber
            source.fileName + (if (line != null) ":$line" else "")
        }
        sb.append("(").append(locationString).append(")")

        sb.append(": ").append(message).append('\n')
        writer.write(sb.toString())
    }
}
