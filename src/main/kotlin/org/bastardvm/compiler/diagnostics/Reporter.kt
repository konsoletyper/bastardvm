package org.bastardvm.compiler.diagnostics

import org.bastardvm.compiler.model.FullFieldReference
import org.bastardvm.compiler.model.FullMethodReference

interface Reporter {
    fun error(source: SourceLocation?, problemLocation: ProblemLocation, message: String)

    fun warn(source: SourceLocation?, problemLocation: ProblemLocation, message: String)
}

data class SourceLocation(val fileName: String, val lineNumber: Int?)

sealed class ProblemLocation {
    class Method(val method: FullMethodReference) : ProblemLocation()

    class Field(val field: FullFieldReference) : ProblemLocation()

    class Class(val className: String) : ProblemLocation()
}
