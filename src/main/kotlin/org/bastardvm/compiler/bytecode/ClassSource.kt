package org.bastardvm.compiler.bytecode

import java.io.InputStream

interface ClassSource {
    fun getClasses(): Set<String>

    fun getBytecode(name: String): InputStream?
}
