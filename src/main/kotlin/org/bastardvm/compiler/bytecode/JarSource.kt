package org.bastardvm.compiler.bytecode

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarInputStream

class JarSource(file: File): ClassSource {
    val jarFile = JarFile(file)
    private val classes = hashSetOf<String>()

    init {
        JarInputStream(FileInputStream(file)).use { input ->
            var entry: JarEntry?
            while (true) {
                entry = input.nextJarEntry
                if (entry == null) {
                    break
                }
                val name = entry.name
                if (name.endsWith(".class")) {
                    classes.add(name.removeSuffix(".class").replace('/', '.'))
                }
            }
        }
    }

    override fun getClasses(): Set<String> = classes

    override fun getBytecode(name: String): InputStream? {
        val entry = jarFile.getJarEntry(name.replace('.', '/') + ".class")
        return if (entry != null) {
            try {
                jarFile.getInputStream(entry)
            } catch (e: IOException) {
                return null
            }
        } else {
            null
        }
    }
}