package org.bastardvm.compiler.bytecode

import org.bastardvm.compiler.ast.Block

class Procedure(val parameterNames: List<String>, val block: Block?)