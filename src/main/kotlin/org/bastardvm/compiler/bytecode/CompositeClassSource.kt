package org.bastardvm.compiler.bytecode

import java.io.File

class CompositeClassSource(sources: Iterable<ClassSource>) : ClassSource {
    private val map = hashMapOf<String, ClassSource>()

    constructor(vararg sources: ClassSource) : this(sources.asIterable())

    init {
        for (source in sources) {
            source.getClasses().forEach { map.put(it, source) }
        }
    }

    override fun getClasses(): Set<String> = map.keys

    override fun getBytecode(name: String) = map[name]?.getBytecode(name)

    companion object {
        fun fromClasspath(classPath: String = System.getProperty("java.class.path", "")): CompositeClassSource {
            return CompositeClassSource(classPath
                    .splitToSequence(File.pathSeparatorChar)
                    .map { File(it) }
                    .filter { it.exists() }
                    .map { if (it.isDirectory) DirectorySource(it) else JarSource(it) }
                    .asIterable())
        }
    }
}