package org.bastardvm.compiler.bytecode

import org.bastardvm.compiler.model.MethodSignature
import org.bastardvm.compiler.model.Type
import org.bastardvm.compiler.model.TypeParser
import org.objectweb.asm.*
import java.io.ByteArrayInputStream
import java.io.InputStream

class RenamingClassSource(packageName: String, private val prefixName: String, val innerSource: ClassSource)
        : ClassSource {
    private val packageName = "$packageName."
    private val classes: Map<String, String> = innerSource.getClasses().asSequence().associate { rename(it) to it }

    override fun getClasses(): Set<String> = classes.keys

    override fun getBytecode(name: String): InputStream? {
        val originalName = classes[name]
        (originalName?.let { innerSource.getBytecode(it) } ?: return null).use {
            val writer = ClassWriter(0)
            ClassReader(it).accept(RenamingClassVisitor(writer), 0)
            return ByteArrayInputStream(writer.toByteArray())
        }
    }

    internal fun rename(name: String): String {
        val lastDot = name.lastIndexOf('.')
        if (lastDot < 0) {
            return name
        }
        val packageName = name.take(lastDot)
        val simpleName = name.drop(lastDot + 1)
        return if (packageName.startsWith(this.packageName) && simpleName.startsWith(prefixName)) {
            val renamedPackage = packageName.drop(this.packageName.length)
            val renamedClass = simpleName.drop(prefixName.length)
            if (renamedPackage.isNotEmpty()) "$renamedPackage.$renamedClass" else renamedClass
        } else {
            name
        }
    }

    internal fun renameInternal(name: String) = rename(name.replace('/', '.')).replace('.', '/')

    internal fun rename(type: Type): Type = when (type) {
        is Type.Obj -> Type.Obj(renameInternal(type.className))
        is Type.Array -> Type.Array(rename(type.elementType))
        else -> type
    }

    internal fun rename(signature: MethodSignature) = MethodSignature(rename(signature.result),
            signature.parameters.map { rename(it) })

    internal fun renameDesc(desc: String) = rename(TypeParser().parse(desc)).toString()

    internal fun renameMethodDesc(desc: String) = rename(TypeParser().parseSignature(desc)).toString()

    internal inner class RenamingClassVisitor(inner: ClassVisitor) : ClassVisitor(Opcodes.ASM5, inner) {
        override fun visit(version: Int, access: Int, name: String, signature: String?, superName: String?,
                           interfaces: Array<out String>?) {
            super.visit(version, access, renameInternal(name), signature,
                    if (superName != null) renameInternal(superName) else null,
                    if (interfaces != null) interfaces.map { renameInternal(it) }.toTypedArray() else null)
        }

        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor {
            return RenamingAnnotationVisitor(super.visitAnnotation(renameDesc(desc), visible))
        }

        override fun visitField(access: Int, name: String, desc: String, signature: String?,
                                value: Any?): FieldVisitor {
            return RenamingFieldVisitor(super.visitField(access, name, renameDesc(desc), signature, value))
        }

        override fun visitMethod(access: Int, name: String, desc: String, signature: String?,
                                 exceptions: Array<out String>?): MethodVisitor? {
            return RenamingMethodVisitor(super.visitMethod(access, name, renameMethodDesc(desc), signature,
                    if (exceptions != null) exceptions.map { renameInternal(it) }.toTypedArray() else null ))
        }
    }

    internal inner class RenamingFieldVisitor(inner: FieldVisitor) : FieldVisitor(Opcodes.ASM5, inner) {
        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor? {
            return RenamingAnnotationVisitor(super.visitAnnotation(renameDesc(desc), visible))
        }
    }

    internal inner class RenamingMethodVisitor(inner: MethodVisitor) : MethodVisitor(Opcodes.ASM5, inner) {
        override fun visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor? {
            return RenamingAnnotationVisitor(super.visitAnnotation(renameDesc(desc), visible))
        }

        override fun visitFieldInsn(opcode: Int, owner: String, name: String, desc: String) {
            super.visitFieldInsn(opcode, renameInternal(owner), name, renameDesc(desc))
        }

        override fun visitLdcInsn(cst: Any?) {
            super.visitLdcInsn(when (cst) {
                is org.objectweb.asm.Type ->
                    org.objectweb.asm.Type.getType(renameDesc(cst.descriptor))
                else -> cst
            })
        }

        override fun visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean) {
            super.visitMethodInsn(opcode, renameInternal(owner), name, renameMethodDesc(desc), itf)
        }

        override fun visitTryCatchBlock(start: Label, end: Label, handler: Label, type: String?) {
            super.visitTryCatchBlock(start, end, handler, if (type != null) renameInternal(type) else null)
        }

        override fun visitTypeInsn(opcode: Int, type: String) {
            val renamedType = if (type.startsWith("[")) renameDesc(type) else renameInternal(type)
            super.visitTypeInsn(opcode, renamedType)
        }
    }

    internal inner class RenamingAnnotationVisitor(inner: AnnotationVisitor) : AnnotationVisitor(Opcodes.ASM5, inner) {
        override fun visit(name: String?, value: Any?) {
            val renamedValue: Any? = when (value) {
                is org.objectweb.asm.Type ->
                    org.objectweb.asm.Type.getType(renameDesc(value.descriptor))
                else -> value
            }
            super.visit(name, renamedValue)
        }

        override fun visitArray(name: String) = RenamingAnnotationVisitor(super.visitArray(name))

        override fun visitAnnotation(name: String?, desc: String): AnnotationVisitor? {
            return RenamingAnnotationVisitor(super.visitAnnotation(name, renameDesc(desc)))
        }
    }
}
