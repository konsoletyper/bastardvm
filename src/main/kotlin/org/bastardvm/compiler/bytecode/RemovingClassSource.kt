package org.bastardvm.compiler.bytecode

import java.io.InputStream

class RemovingClassSource(packageNames: Iterable<String>, val innerSource: ClassSource) : ClassSource {
    private val classSet = innerSource.getClasses().filter { className ->
        !packageNames.any { className.startsWith(it) }
    }.toSet()

    override fun getClasses() = classSet

    override fun getBytecode(name: String): InputStream? {
        return if (classSet.contains(name)) innerSource.getBytecode(name) else null
    }
}
