package org.bastardvm.compiler.bytecode

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

class DirectorySource(val file: File) : ClassSource {
    private val classes = hashSetOf<String>()

    init {
        walk(file)
    }

    private fun walk(dir: File, path: StringBuilder = StringBuilder()) {
        val lengthBackup = path.length
        for (file in dir.listFiles()) {
            if (path.isNotEmpty()) {
                path.append('.')
            }
            path.append(file.name)
            if (file.isDirectory) {
                walk(file, path)
            } else if (file.name.endsWith(".class")) {
                classes.add(path.toString().removeSuffix(".class"))
            }
            path.setLength(lengthBackup)
        }
    }

    override fun getClasses(): Set<String> = classes

    override fun getBytecode(name: String): InputStream? {
        try {
            val file = File(file, name.replace('.', '/') + ".class")
            return if (file.exists()) FileInputStream(file) else null
        } catch (e: IOException) {
            return null
        }
    }
}
