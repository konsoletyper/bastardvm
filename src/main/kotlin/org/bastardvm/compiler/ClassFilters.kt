package org.bastardvm.compiler

object ClassFilters {
    fun includeWildcard(wildcard: String) = { clsName: String ->
        if (matchWildcard(wildcard, clsName)) true else null
    }

    fun excludeWildcard(wildcard: String) = { clsName: String ->
        if (matchWildcard(wildcard, clsName)) false else null
    }

    fun matchWildcard(wildcard: String, test: String) = matchWildcard(wildcard, test, 0, 0)

    private fun matchWildcard(wildcard: String, test: String, wildcardPos: Int, testPos: Int): Boolean {
        val wildcardChar = wildcard.getOrNull(wildcardPos)
        val testChar = test.getOrNull(testPos)
        return when(wildcardChar) {
            '?' -> testChar != null && matchWildcard(wildcard, test, wildcardPos + 1, testPos + 1)
            '*' -> {
                if (wildcard.getOrNull(wildcardPos + 1) == '*') {
                    (testChar != null && matchWildcard(wildcard, test, wildcardPos, testPos + 1))
                            || matchWildcard(wildcard, test, wildcardPos + 2, testPos)
                } else {
                    (testChar != '.' && testChar != null && matchWildcard(wildcard, test, wildcardPos, testPos + 1))
                            || matchWildcard(wildcard, test, wildcardPos + 1, testPos)
                }
            }
            testChar -> wildcardChar == null || matchWildcard(wildcard, test, wildcardPos + 1, testPos + 1)
            else -> false
        }
    }
}
