package org.bastardvm.compiler.rendering

import org.bastardvm.compiler.model.FieldReference
import org.bastardvm.compiler.model.FullFieldReference
import org.bastardvm.compiler.model.FullMethodReference
import org.bastardvm.compiler.model.MethodReference

interface Naming {
    fun getNameFor(cls: String): String

    fun getNameFor(method: MethodReference): String

    fun getNameFor(method: FullMethodReference): String

    fun getNameFor(field: FieldReference): String

    fun getNameFor(field: FullFieldReference): String
}
