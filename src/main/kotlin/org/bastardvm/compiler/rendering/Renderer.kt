package org.bastardvm.compiler.rendering

import org.bastardvm.compiler.ast.*
import org.bastardvm.compiler.ast.ArithmeticBinaryOperation.*
import org.bastardvm.compiler.ast.BinaryOperation.*
import org.bastardvm.compiler.ast.BitwiseBinaryOperation.*
import org.bastardvm.compiler.ast.Expression.*
import org.bastardvm.compiler.model.*
import org.bastardvm.platform.VM
import kotlin.reflect.KClass

class Renderer(val writer: Writer) {
    fun collectVariables(statements: Iterable<Statement>) = hashSetOf<String>().let {
        statements.forEach { statement -> collectVariables(statement, it) }
        it
    }

    private fun collectVariables(statement: Statement, variables: MutableSet<String>) {
        statement.expressions.forEach { collectVariables(it, variables) }
        statement.nestedStatements.forEach { collectVariables(it, variables) }
    }

    private fun collectVariables(expr: Expression, variables: MutableSet<String>) {
        when (expr) {
            is Expression.Variable -> variables.add(expr.name)
            is Expression.StackVariable -> variables.add("\$stack${expr.index}")
            is Expression.PostIncrement -> variables.add("\$it")
            else -> expr.nestedExpressions.forEach { collectVariables(it, variables) }
        }
    }

    fun write(statement: Statement): Renderer {
        when (statement) {
            is Statement.If -> {
                statement.label?.let { append(it).wsAfter(":") }
                append("if").wsBefore("(").write(statement.condition).wsAfter(")").append("{").newLine().indent()
                statement.consequent.forEach { write(it) }
                outdent().append("}")
                if (statement.alternative.isNotEmpty()) {
                    wsAround("else").append("{").newLine().indent()
                    statement.alternative.forEach { write(it) }
                    outdent().append("}")
                }
                newLine()
            }
            is Statement.While -> {
                statement.label?.let { append(it).wsAfter(":") }
                append("while").wsBefore("(").write(statement.condition).wsAfter(")").append("{").newLine().indent()
                statement.body.forEach { write(it) }
                outdent().append("}").newLine()
            }
            is Statement.Group -> {
                statement.label?.let { append(it).wsAfter(":") }
                append("{").newLine().indent()
                statement.body.forEach { write(it) }
                outdent().append("}").newLine()
            }
            is Statement.DoWhile -> {
                statement.label?.let { append(it).wsAfter(":") }
                append("do").wsBefore("{").newLine().indent()
                statement.body.forEach { write(it) }
                outdent().wsAfter("}").append("while").wsBefore("(").write(statement.condition)
                        .append(");").newLine()
            }
            is Statement.Switch -> {
                statement.label?.let { append(it).wsAfter(":") }
                append("switch").wsBefore("(").write(statement.condition).wsAfter(")").append("{").newLine().indent()
                for (clause in statement.clauses) {
                    append("case ").append(clause.number.toString()).append(":").newLine().indent()
                    clause.body.forEach { write(it) }
                    outdent()
                }

                append("default:").newLine().indent()
                statement.defaultBody.forEach { write(it) }
                outdent()

                outdent().append("}").newLine()
            }
            is Statement.Return -> {
                append("return")
                statement.value?.let { append(" ").write(it) }
                append(";").newLine()
            }
            is Statement.Throw -> {
                append("throw ").write(statement.value).append(";").newLine()
            }
            is Statement.Break -> {
                append("break")
                statement.label.let { append(" ").append(it) }
                append(";").newLine()
            }
            is Statement.Continue -> {
                append("continue")
                statement.label.let { append(" ").append(it) }
                append(";").newLine()
            }
            is Statement.Assignment -> {
                write(statement.target).wsAround("=").write(statement.value).append(";").newLine()
            }
            is Statement.ExpressionStatement -> {
                val expression = statement.expression
                if (expression is Expression.PostIncrement) {
                    append(expression.variable).wsAround("=").append(expression.variable)
                            .wsAround(if (expression.amount > 0) "+" else "-")
                            .append(Math.abs(expression.amount).toString())
                            .wsAround("|").append("0;").newLine()
                } else {
                    write(statement.expression).append(";").newLine()
                }
            }
        }
        return this
    }

    fun write(expr: Expression): Renderer {
        when (expr) {
            is Variable -> append(expr.name)
            is StackVariable -> append("\$stack${expr.index}")
            is IntConstant -> append("${expr.value}")
            is LongConstant -> append("${expr.value}")
            is DoubleConstant -> append("${expr.value}")
            is StringConstant -> append(naming.getNameFor(stringMethod)).append("(\"${escapeString(expr.value)}\")")
            is TypeConstant -> append(typeToString(expr.value))
            is Null -> append("null")
            is Self -> append("this")
            is True -> append("true")
            is False -> append("false")

            is ArithmeticBinary -> when (expr.type) {
                ArithmeticType.LONG -> when (expr.operation) {
                    ADD -> function("VM.addLong", expr.left, expr.right)
                    SUBTRACT -> function("VM.subtractLong", expr.left, expr.right)
                    MULTIPLY -> function("VM.multiplyLong", expr.left, expr.right)
                    DIVIDE -> function("VM.divideLong", expr.left, expr.right)
                    REMAINDER -> function("VM.remainderLong", expr.left, expr.right)
                    COMPARE -> function("VM.compareLong", expr.left, expr.right)
                }
                ArithmeticType.DOUBLE -> write(expr.operation, expr.left, expr.right)
                ArithmeticType.INT -> toInt { it.write(expr.operation, expr.left, expr.right) }
            }
            is BitwiseBinary -> when (expr.type) {
                BitwiseType.LONG -> when (expr.operation) {
                    BITWISE_AND -> function("VM.bitwiseAndLong", expr.left, expr.right)
                    BITWISE_OR -> function("VM.bitwiseOrLong", expr.left, expr.right)
                    BITWISE_XOR -> function("VM.bitwiseXorLong", expr.left, expr.right)
                    LEFT_SHIFT -> function("VM.leftShiftLong", expr.left, expr.right)
                    RIGHT_SHIFT -> function("VM.rightShiftLong", expr.left, expr.right)
                    UNSIGNED_RIGHT_SHIFT -> function("VM.unsignedRightShiftLong", expr.left, expr.right)
                }
                BitwiseType.INT -> when (expr.operation) {
                    BITWISE_AND -> infix("&", expr.left, expr.right)
                    BITWISE_OR -> infix("|", expr.left, expr.right)
                    BITWISE_XOR -> infix("^", expr.left, expr.right)
                    LEFT_SHIFT -> infix("<<", expr.left, expr.right)
                    RIGHT_SHIFT -> infix(">>", expr.left, expr.right)
                    UNSIGNED_RIGHT_SHIFT -> infix(">>>", expr.left, expr.right)
                }
            }

            is Binary -> when (expr.operation) {
                AND -> infix("&&", expr.left, expr.right)
                OR -> infix("||", expr.left, expr.right)
                ARRAY_ELEMENT -> write(expr.left).append(".").append(naming.getNameFor(arrayDataField))
                        .append("[").write(expr.right).append("]")
                EQUAL -> infix("===", expr.left, expr.right)
                NOT_EQUAL -> infix("!==", expr.left, expr.right)
                LESS -> infix("<", expr.left, expr.right)
                LESS_OR_EQUAL -> infix("<=", expr.left, expr.right)
                GREATER -> infix(">", expr.left, expr.right)
                GREATER_OR_EQUAL -> infix(">=", expr.left, expr.right)
            }

            is Negate -> when (expr.type) {
                ArithmeticType.LONG -> function("VM.negateLong", expr.value)
                ArithmeticType.DOUBLE -> prefix("-", expr.value)
                ArithmeticType.INT -> toInt { it.prefix("-", expr.value) }
            }
            is BitwiseInversion -> when (expr.type) {
                BitwiseType.LONG -> function("VM.bitwiseInversionLong", expr.value)
                BitwiseType.INT -> prefix("~", expr.value)
            }
            is Not -> prefix("!", expr.value)

            is ArrayLength -> write(expr.array).append(".").append(naming.getNameFor(arrayDataField))
                    .append(".length")
            is GetField -> write(expr.target).append(".").append(naming.getNameFor(expr.field.nameAndType))
            is GetStaticField ->
                if (expr.init) {
                    append("(").append(naming.getNameFor(expr.field.className)).append(".init(),").ws()
                            .append(naming.getNameFor(expr.field)).append(")")
                } else {
                    append(naming.getNameFor(expr.field))
                }

            is Invoke -> {
                val target = expr.method.target
                if (target is Type.Obj) {
                    write(expr.target).append(".").function(naming.getNameFor(expr.method.nameWithSignature),
                            *expr.arguments.toTypedArray())
                } else if (target is Type.Array) {
                    function(cloneArrayMethod, expr.target)
                }
            }
            is InvokeSpecial -> {
                val mangledClass = naming.getNameFor((expr.method.target as Type.Obj).className)
                append(mangledClass).append(".prototype.").append(naming.getNameFor(expr.method.nameWithSignature))
                        .append(".call(").write(expr.target)
                expr.arguments.forEach { wsAfter(",").write(it) }
                append(")")
            }
            is InvokeStatic -> function(naming.getNameFor(expr.method), *expr.arguments.toTypedArray())

            is Construct -> append("new ").append(naming.getNameFor(expr.className)).append("()")
            is ConstructArray -> {
                if (expr.dimensions.size == 1) {
                    when (expr.type) {
                        is Type.Boolean, is Type.Byte, is Type.Short, is Type.Char, is Type.Int ->
                                function(numericArrayMethod, expr.dimensions[0])
                        else -> function(objectArrayMethod, expr.dimensions[0])
                    }
                } else {
                    val type = generateSequence(expr.type) { (it as Type.Array).elementType }
                            .take(expr.dimensions.size + 1).last()
                    val fullRef = FullMethodReference(vmClass, multiArrayMethod)
                    append(naming.getNameFor(fullRef)).append("([")
                            .commaSeparated(*expr.dimensions.toTypedArray()).append("]").wsAfter(",")
                    when (type) {
                        is Type.Boolean, is Type.Byte, is Type.Short, is Type.Char, is Type.Int ->
                                append("0")
                        else -> append("null")
                    }
                    append(")")
                }
            }
            is InstanceOf -> function(instanceOfMethod, expr.value, Expression.TypeConstant(expr.type))
            is Cast -> function(castMethod, expr.value, Expression.TypeConstant(expr.type))
            is ArithmeticCast -> when (expr.source) {
                ArithmeticType.INT -> when (expr.target) {
                    ArithmeticType.LONG -> function("VM.intToLong", expr.value)
                    ArithmeticType.INT,
                    ArithmeticType.DOUBLE -> write(expr.value)
                }
                ArithmeticType.LONG -> when (expr.target) {
                    ArithmeticType.INT -> function("VM.longToInt", expr.value)
                    ArithmeticType.DOUBLE -> function("VM.longToDouble", expr.value)
                    ArithmeticType.LONG -> write(expr.value)
                }
                ArithmeticType.DOUBLE -> when (expr.target) {
                    ArithmeticType.INT -> toInt { write(expr.value) }
                    ArithmeticType.LONG -> function("VM.doubleToLong", expr.value)
                    ArithmeticType.DOUBLE -> write(expr.value)
                }
            }

            is PreIncrement -> {
                append("(").append(expr.variable).wsAround(if (expr.amount > 0) "+" else "-")
                        .append(Math.abs(expr.amount).toString())
                        .wsAround("|").append("0").append(")")
            }
            is PostIncrement -> {
                append("(\$it").wsAround("=").append(expr.variable).wsAfter(",")
                append(expr.variable).wsAround("=").append(expr.variable)
                        .wsAround(if (expr.amount > 0) "+" else "-")
                        .append(Math.abs(expr.amount).toString())
                        .wsAround("|").append("0")
                        .wsAfter(",")
                append("\$it)")
            }

            is Conditional -> {
                append("(").write(expr.condition).wsAround("?").write(expr.consequent)
                        .wsAround(":").write(expr.alternative).append(")")
            }
        }
        return this
    }

    private fun write(op: ArithmeticBinaryOperation, left: Expression, right: Expression) = when(op) {
        ArithmeticBinaryOperation.ADD -> infix("+", left, right)
        ArithmeticBinaryOperation.SUBTRACT -> infix("-", left, right)
        ArithmeticBinaryOperation.MULTIPLY -> infix("*", left, right)
        ArithmeticBinaryOperation.DIVIDE -> infix("/", left, right)
        ArithmeticBinaryOperation.REMAINDER -> infix("%", left, right)
        ArithmeticBinaryOperation.COMPARE -> function(compareMethod, left, right)
    }

    val naming = writer.naming

    private fun infix(symbol: String, left: Expression, right: Expression) = append("(").write(left).ws()
            .append(symbol).ws().write(right).append(")")

    private fun prefix(symbol: String, value: Expression) = append("(").append(symbol).write(value).append(")")

    private fun function(method: MethodReference, vararg arguments: Expression) =
            function(naming.getNameFor(FullMethodReference(vmClass, method)), *arguments)

    private fun function(name: String, vararg arguments: Expression) = append(name).surrounded(*arguments)

    private inline fun toInt(body: (Renderer) -> Renderer) = body(append("(")).wsAround("|").append("0)")

    fun surrounded(vararg arguments: Expression) = append("(").commaSeparated(*arguments).append(")")

    fun commaSeparated(vararg arguments: Expression): Renderer {
        if (arguments.isNotEmpty()) {
            write(arguments[0])
            for (i in 1.until(arguments.size)) {
                append(",").ws().write(arguments[i])
            }
        }
        return this
    }

    fun commaSeparated(arguments: Iterable<String>): Renderer {
        val iter = arguments.iterator()
        if (iter.hasNext()) {
            append(iter.next())
            while (iter.hasNext()) {
                append(",").ws().append(iter.next())
            }
        }
        return this
    }

    fun append(text: String): Renderer {
        writer.append(text)
        return this
    }

    private fun wsAround(text: String) = ws().append(text).ws()

    private fun wsAfter(text: String) = append(text).ws()

    private fun wsBefore(text: String) = ws().append(text)

    fun ws(): Renderer {
        writer.ws()
        return this
    }

    fun newLine(): Renderer {
        writer.newLine()
        return this
    }

    fun indent(): Renderer {
        writer.indent()
        return this
    }

    fun outdent(): Renderer {
        writer.outdent()
        return this
    }

    fun typeToString(type: Type): String {
        return when (type) {
            is Type.Boolean -> primitiveType("boolean")
            is Type.Byte -> primitiveType("byte")
            is Type.Short -> primitiveType("short")
            is Type.Char -> primitiveType("char")
            is Type.Int -> primitiveType("int")
            is Type.Long -> primitiveType("long")
            is Type.Float -> primitiveType("float")
            is Type.Double -> primitiveType("double")
            is Type.Void -> primitiveType("void")
            is Type.Obj -> naming.getNameFor(type.className)
            is Type.Array -> naming.getNameFor(FullMethodReference(vmClass, arrayTypeMethod)) +
                    "(${typeToString(type.elementType)})"
        }
    }

    private fun primitiveType(name: String) = naming.getNameFor(FullMethodReference(vmClass, "${name}Type",
            type(Any::class)))

    companion object {
        fun escapeString(str: String): String {
            val sb = StringBuilder()
            for (i in 0.until(str.length)) {
                val c = str[i]
                sb.append(when (c) {
                    '\r' -> "\\r"
                    '\n' -> "\\n"
                    '\t' -> "\\t"
                    '\"' -> "\\\""
                    '\\' -> "\\\\"
                    else ->
                        if (c < ' ') {
                            "\\u00" + hexDigits[c.toInt() / 16].toString() + hexDigits[c.toInt() % 16].toString()
                        } else {
                            c.toString()
                        }
                })
            }
            return sb.toString()
        }

        private val hexDigits = "0123456789ABCDEF"

        private val vmClass = type(VM::class)
        private val compareMethod = MethodReference("compare", Type.Int, type(VM.JSObject::class),
                type(VM.JSObject::class))
        private val castMethod = MethodReference("cast", Type.Obj("java.lang.Object"), Type.Obj("java.lang.Object"))
        private val numericArrayMethod = MethodReference("createNumericArray",
                type(VM.NativeArray::class), Type.Int)
        private val objectArrayMethod = MethodReference("createObjectArray",
                type(VM.NativeArray::class), Type.Int)
        private val arrayDataField = FieldReference("data", Type.Obj("java.lang.Object"))
        private val instanceOfMethod = MethodReference("instanceOf", Type.Boolean, Type.Obj("java.lang.Object"),
                Type.Obj("java.lang.Object"))
        private val multiArrayMethod = MethodReference("createMultiArray", Type.Obj("java.lang.Object"),
                Type.Obj("java.lang.Object"), Type.Obj("java.lang.Object"))
        private val cloneArrayMethod = MethodReference("cloneArray", type(VM.NativeArray::class),
                type(VM.NativeArray::class))
        private val arrayTypeMethod = MethodReference("arrayType", type(Any::class), type(Any::class))

        private val stringMethod = FullMethodReference(vmClass, "string", Type.Obj("java.lang.String"),
                Type.Obj("java.lang.String\$NativeString"))

        private fun type(cls: KClass<*>) = Type.Obj(cls.java.name)
    }
}
