package org.bastardvm.compiler.rendering

import org.bastardvm.compiler.ast.Block
import org.bastardvm.compiler.ast.Statement
import org.bastardvm.compiler.ast.Transition
import org.bastardvm.compiler.bytecode.Procedure

class BlockRenderer(val renderer: Renderer) {
    fun write(procedure: Procedure) {
        val blocks = procedure.block!!.findReachableBlocks()
        val blockIndexes = blocks.withIndex().associate { it.value to it.index }
        val simple = blocks.size == 1 && procedure.block.transition is Transition.Empty
        val variables = renderer.collectVariables(allStatements(blocks))
        if (!simple) {
            variables.add("\$ptr")
        }
        variables.removeAll(procedure.parameterNames)
        val variableList = variables.sorted().toList()

        if (variableList.isNotEmpty()) {
            renderer.append("var ").commaSeparated(variableList).append(";").newLine()
        }

        if (simple) {
            procedure.block.body.forEach { renderer.write(it) }
        } else {
            renderer.append("\$ptr").ws().append("=").ws().append("0;").newLine()
            renderer.append("for(;;)").ws().append("{").indent().newLine()
            renderer.append("switch").ws().append("(\$ptr)").ws().append("{").newLine().indent()
            blocks.forEachIndexed { i, block ->
                renderer.append("case ").append(i.toString()).append(":").indent().newLine()
                block.body.forEach { renderer.write(it) }
                writeTransition(block.transition, blockIndexes)
                renderer.outdent()
            }
            renderer.outdent().append("}").newLine() // switch
            renderer.outdent().append("}").newLine() // for
        }
    }

    private fun writeTransition(transition: Transition, indexes: Map<Block, Int>) {
        when (transition) {
            is Transition.Jump -> {
                jump(transition.target, indexes)
                renderer.append("break;").newLine()
            }
            is Transition.Conditional -> {
                renderer.append("if").ws().append("(").write(transition.condition).append(")").ws().append("{")
                        .newLine().indent()
                jump(transition.whenTrue, indexes)
                renderer.outdent().append("}").ws().append("else").ws().append("{").newLine().indent()
                jump(transition.whenFalse, indexes)
                renderer.outdent().append("}").newLine()
                renderer.append("break;").newLine()
            }
            is Transition.Switch -> {
                renderer.append("switch").ws().append("(").write(transition.condition).append(")").ws().append("{")
                        .newLine().indent()
                for (clause in transition.clauses) {
                    renderer.append("case ").append(clause.number.toString()).append(":").indent().newLine()
                    jump(clause.target, indexes)
                    renderer.append("break;").newLine().outdent()
                }

                renderer.append("default:").indent().newLine()
                jump(transition.defaultTarget, indexes)
                renderer.append("break;").newLine().outdent()

                renderer.outdent().append("}").newLine()
                renderer.append("break;").newLine()
            }
        }
    }

    private fun jump(block: Block, indexes: Map<Block, Int>) {
        renderer.append("\$ptr").ws().append("=").ws().append(indexes[block].toString()).append(";").newLine()
    }

    private fun allStatements(blocks: Iterable<Block>) = blocks
            .map { it.body  }
            .reduce { a: Iterable<Statement>, b -> a + b }
}