package org.bastardvm.compiler.rendering

import org.bastardvm.compiler.model.*

interface Writer : Appendable {
    override fun append(value: Char): Writer

    override fun append(value: CharSequence): Writer {
        return append(value, 0, value.length)
    }

    override fun append(value: CharSequence, start: Int, end: Int): Writer

    fun appendMethod(method: MethodReference): Writer = append(naming.getNameFor(method))

    fun appendMethod(name: String, result: Type, vararg parameters: Type): Writer {
        return appendMethod(MethodReference(name, result, *parameters))
    }

    fun appendMethod(method: FullMethodReference): Writer = append(naming.getNameFor(method))

    fun appendMethod(owner: Type, name: String, result: Type, vararg parameters: Type): Writer {
        return appendMethod(FullMethodReference(owner, name, result, *parameters))
    }

    fun appendField(field : FieldReference): Writer = append(naming.getNameFor(field))

    fun appendField(name: String, type: Type): Writer = appendField(FieldReference(name, type))

    fun appendField(field : FullFieldReference): Writer = append(naming.getNameFor(field))

    fun appendField(owner: String, name: String, type: Type): Writer {
        return appendField(FullFieldReference(owner, name, type))
    }

    fun appendClass(name: String) = append(naming.getNameFor(name))

    fun newLine(): Writer

    fun hardNewLine(): Writer

    fun ws(): Writer

    fun tokenBoundary(): Writer

    fun indent(): Writer

    fun outdent(): Writer

    val naming: Naming
}