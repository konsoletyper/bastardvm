package org.bastardvm.compiler.rendering

import org.bastardvm.compiler.model.*

class DefaultNaming : Naming {
    private val occupiedNames = hashSetOf<String>()
    private val classAliases = hashMapOf<String, String>()
    private val methodAliases = hashMapOf<MethodReference, String>()
    private val fullMethodAliases = hashMapOf<FullMethodReference, String>()
    private val fieldAliases = hashMapOf<FieldReference, String>()
    private val fullFieldAliases = hashMapOf<FullFieldReference, String>()

    override fun getNameFor(cls: String) = classAliases.getOrPut(cls) {
        occupyName(nameForClass(cls))
    }

    override fun getNameFor(method: MethodReference) = methodAliases.getOrPut(method) {
        occupyName(methodName(method.name))
    }

    override fun getNameFor(method: FullMethodReference) = fullMethodAliases.getOrPut(method) {
        val target = method.target
        when (target) {
            is Type.Obj -> occupyName(nameForClass(target.className) + "_" + methodName(method.name))
            else -> occupyName("_unknown_" + method.name)
        }
    }

    override fun getNameFor(field: FieldReference) = fieldAliases.getOrPut(field) {
        occupyName(field.name)
    }

    override fun getNameFor(field: FullFieldReference) = fullFieldAliases.getOrPut(field) {
        occupyName(nameForClass(field.className) + "_" + field.name)
    }

    private fun methodName(name: String) = when (name) {
        "<init>" -> "construct"
        "<clinit>" -> "init"
        else -> name
    }

    private fun nameForClass(name: String): String {
        val sb = StringBuilder()
        val parts = name.split('.')
        for (packageName in parts.dropLast(1)) {
            packageName.firstOrNull()?.let { sb.append(it) }
        }
        if (sb.isNotEmpty()) {
            sb.append('_')
        }
        return sb.append(parts.last()).toString()
    }

    private fun occupyName(name: String): String {
        if (occupiedNames.add(name)) {
            return name
        }
        var i = 1
        while (true) {
            val newName = name + "_" + i++
            if (occupiedNames.add(newName)) {
                return newName
            }
        }
    }
}
