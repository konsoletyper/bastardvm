package org.bastardvm.compiler.rendering

class DefaultWriter(override val naming: Naming, private val appendable: Appendable,
                    private val lineWidth: Int = 512, private val minifying: Boolean)
: Writer {
    private var indentSize: Int = 0
    private var column: Int = 0
    private var lineStart: Boolean = true

    override fun append(value: Char): Writer {
        appendIndent()
        appendable.append(value)
        if (value == '\n') {
            newLine()
        } else {
            column++
        }
        return this
    }

    override fun append(value: CharSequence, start: Int, end: Int): Writer {
        var last = start
        for (i in start.until(end)) {
            if (value[i] == '\n') {
                appendSingleLine(value, last, i)
                newLine()
                last = i + 1
            }
        }
        appendSingleLine(value, last, end)
        return this
    }

    private fun appendSingleLine(value: CharSequence, start: Int, end: Int) {
        if (start == end) {
            return
        }
        appendIndent()
        column += end - start
        appendable.append(value, start, end)
    }

    override fun newLine(): Writer {
        if (!minifying) {
            appendable.append('\n')
            column = 0
            lineStart = true
        }
        return this
    }

    override fun hardNewLine(): Writer {
        appendable.append('\n')
        column = 0
        lineStart = true
        return this
    }

    override fun ws(): Writer {
        if (column >= lineWidth) {
            newLine()
        } else if (!minifying) {
            appendable.append(' ')
            column++
        }
        return this
    }

    override fun tokenBoundary(): Writer {
        if (column >= lineWidth) {
            newLine()
        }
        return this
    }

    override fun indent(): Writer {
        ++indentSize
        return this
    }

    override fun outdent(): Writer {
        --indentSize
        return this
    }

    private fun appendIndent() {
        if (minifying) {
            return
        }
        if (lineStart) {
            0.until(indentSize).forEach {
                appendable.append("    ")
                column += 4
            }
            lineStart = false
        }
    }
}