package org.bastardvm.cli

import org.bastardvm.compiler.ClassFilters
import org.bastardvm.compiler.Compiler
import org.bastardvm.compiler.bytecode.CompositeClassSource
import org.bastardvm.compiler.bytecode.RemovingClassSource
import org.bastardvm.compiler.bytecode.RenamingClassSource
import org.bastardvm.compiler.diagnostics.WritingReporter
import org.bastardvm.compiler.model.BytecodeModel
import org.bastardvm.compiler.model.FullMethodReference
import org.bastardvm.compiler.model.Type
import org.bastardvm.compiler.rendering.DefaultNaming
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        printUsage()
        System.exit(0)
    }
    class Once<T>(val name: String) {
        var value: T? = null
            set(value) {
                if (field != null) {
                    return printError("$name is specified multiple times")
                }
                field = value
            }
    }
    val filters = arrayListOf<(String) -> Boolean?>()
    var i = 0
    val mainClass = Once<String>("Main class")
    val outputFile = Once<String>("Output file")
    val classPath = Once<String>("Class path")

    fun expectValue(name: String) = if (++i >= args.size) {
        printError("$name option expects value")
        ""
    } else {
        args[i]
    }
    fun include(wildcard: String) {
        filters.add(ClassFilters.includeWildcard(wildcard))
    }
    fun exclude(wildcard: String) {
        filters.add(ClassFilters.excludeWildcard(wildcard))
    }

    while (i < args.size) {
        val arg = args[i]
        when {
            arg == "-p" -> classPath.value = expectValue("-p")
            arg.startsWith("--classpath=") -> classPath.value = arg.removePrefix("--classpath=")
            arg == "-o" -> outputFile.value = expectValue("-o")
            arg.startsWith("--output=") -> outputFile.value = arg.removePrefix("--output=")
            arg.startsWith("--include=") -> include(arg.removePrefix("--include="))
            arg.startsWith("--exclude=") -> exclude(arg.removePrefix("--exclude="))
            else -> mainClass.value = arg
        }
        ++i
    }

    val mainClassValue = mainClass.value
    if (mainClassValue == null) {
        return printError("Main class is not specified")
    }

    val envClassPath = System.getProperty("java.class.path", "")
            .splitToSequence(File.pathSeparatorChar)
            .filter { !it.endsWith(".jar") || it.endsWith("/bastardc.jar") }
            .joinToString(File.pathSeparator)
    var classPathSource = CompositeClassSource.fromClasspath(envClassPath)
    val classPathValue = classPath.value
    if (classPathValue != null) {
        classPathSource = CompositeClassSource(classPathSource, CompositeClassSource.fromClasspath(classPathValue))
    }

    val source = RenamingClassSource("org.bastardvm.classlib", "T",
            RemovingClassSource(listOf("java", "js"), classPathSource))
    val finalFilter = listOf(
            ClassFilters.includeWildcard("java.**"),
            ClassFilters.includeWildcard("js.**"),
            ClassFilters.includeWildcard("org.bastardvm.platform.**"),
            ClassFilters.includeWildcard("$mainClassValue*"))
            .plus(filters.reversed())
    val errorWriter = OutputStreamWriter(System.err)
    val reporter = WritingReporter(errorWriter)
    val compiler = Compiler(
            source = source,
            nameFilters = finalFilter.toList(),
            filters = listOf({ _ -> true }),
            model = BytecodeModel(source, reporter),
            naming = DefaultNaming(),
            reporter = reporter)

    val output = OutputStreamWriter(outputFile.value?.let { FileOutputStream(it) } ?: System.out, "UTF-8")
    output.use {
        compiler.compile(it)
        val mainRef = FullMethodReference(Type.Obj(mainClassValue), "main", Type.Void,
                Type.Array(Type.Obj("java.lang.String")))
        val mainMethod = compiler.naming.getNameFor(mainRef)
        it.write("var main = $mainMethod;\n")
    }
    errorWriter.flush()
}

internal fun printError(error: String) {
    System.err.println(error)
    printUsage()
    System.exit(1)
}

internal fun printUsage() {
    println("usage: bastardc [OPTION]... CLASS")
    println("Compiles classpath to JavaScript file, with main() function which corresponds")
    println("main method of CLASS")
    println("  -o, --output=FILE           where to write generated JavaScript")
    println("  -p, --classpath=CLASSPATH   classpath to process")
    println("      --include=WILDCARD      which classes to include in classpath")
    println("      --exclude=WILDCARD      which classes to exclude from classpath")
}