package org.bastardvm.examples;

public class Parser {
    private static int index;

    public static void main(String[] args) {
        printResult(parse("2+3"));
        printResult(parse("2+2*3"));
        printResult(parse("(2+2)*3"));
        printResult(parse("((2+2)*3-(5-2))/2"));
        printResult(parse("((2+2)*3-(5-2"));
        printResult(parse("(2+2)*3)-(5-2"));
    }

    public static void printResult(Result r) {
        if (r == null) {
            js.Console.println("Parse error at: " + index);
        } else {
            js.Console.println("Result is: " + r.value);
        }
    }

    public static Result parse(String s) {
        index = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) == '$') {
                return null;
            }
        }
        Result r = additive(s + "$");
        return index == s.length() ? r : null;
    }

    private static Result additive(String s) {
        Result r = multiplicative(s);
        if (r == null) {
            return null;
        }
        int n = r.value;
        outer: while (true) {
            switch (s.charAt(index)) {
                case '+':
                    ++index;
                    r = multiplicative(s);
                    if (r == null) {
                        return null;
                    }
                    n += r.value;
                    break;
                case '-':
                    ++index;
                    r = multiplicative(s);
                    if (r == null) {
                        return null;
                    }
                    n -= r.value;
                    break;
                default:
                    break outer;
            }
        }
        return new Result(n);
    }

    private static Result multiplicative(String s) {
        Result r = atom(s);
        if (r == null) {
            return null;
        }
        int n = r.value;
        outer: while (true) {
            switch (s.charAt(index)) {
                case '*':
                    ++index;
                    r = atom(s);
                    if (r == null) {
                        return null;
                    }
                    n *= r.value;
                    break;
                case '/':
                    ++index;
                    r = atom(s);
                    if (r == null) {
                        return null;
                    }
                    n /= r.value;
                    break;
                default:
                    break outer;
            }
        }
        return new Result(n);
    }

    private static Result atom(String s) {
        switch (s.charAt(index)) {
            case '(':
                ++index;
                Result r = additive(s);
                if (s.charAt(index) != ')') {
                    return null;
                } else {
                    ++index;
                    return r;
                }
            default:
                if (isDigit(s.charAt(index))) {
                    return new Result(parseNumber(s));
                } else {
                    return null;
                }
        }
    }

    private static int parseNumber(String s) {
        int n = 0;
        while (isDigit(s.charAt(index))) {
            n = n * 10 + (s.charAt(index) - '0');
            ++index;
        }
        return n;
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    static class Result {
        public int value;

        public Result(int value) {
            this.value = value;
        }
    }
}
