package org.bastardvm.examples;

public class Sort {
    public static void main(String[] args) {
        int[] array = createArray();
        js.Console.println("Original array: " + arrayToString(array));

        mergeSort(array);
        js.Console.println("Merge sort: " + arrayToString(array));

        array = createArray();
        quickSort(array);
        js.Console.println("Quick sort: " + arrayToString(array));
    }

    public static void mergeSort(int[] a) {
        if (a.length == 0) {
            return;
        }
        int[] first = a;
        int[] second = new int[a.length];
        int chunkSize = 1;
        while (chunkSize < a.length) {
            for (int i = 0; i < first.length; i += chunkSize * 2) {
                merge(first, second, i, min(first.length, i + chunkSize),
                        min(first.length, i + 2 * chunkSize));
            }
            int[] tmp = first;
            first = second;
            second = tmp;
            chunkSize *= 2;
        }
        if (first != a) {
            for (int i = 0; i < first.length; ++i) {
                second[i] = first[i];
            }
        }
    }

    private static void merge(int[] a, int[] b, int from, int split, int to) {
        int index = from;
        int from2 = split;
        while (true) {
            if (from == split) {
                while (from2 < to) {
                    b[index++] = a[from2++];
                }
                break;
            } else if (from2 == to) {
                while (from < split) {
                    b[index++] = a[from++];
                }
                break;
            }
            int p = a[from];
            int q = a[from2];
            if (p <= q) {
                b[index++] = p;
                ++from;
            } else {
                b[index++] = q;
                ++from2;
            }
        }
    }

    public static void quickSort(int[] array) {
        quickSort(array, 0, array.length);
    }

    private static void quickSort(int[] array, int from, int to) {
        int sz = to - from;
        if (sz < 2) {
            return;
        } else if (sz == 2) {
            --to;
            if (array[from] > array[to]) {
                int tmp = array[from];
                array[from] = array[to];
                array[to] = tmp;
            }
            return;
        }
        int pivot = (array[from] + array[to - 1]) >> 1;
        int i = from;
        int j = to - 1;
        outer: do {
            while (array[i] <= pivot) {
                ++i;
                if (i == j) {
                    break outer;
                }
            }
            while (array[j] > pivot) {
                --j;
                if (i == j) {
                    break outer;
                }
            }
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        } while (i < j);

        if (i == from) {
            ++i;
        } else if (i == to) {
            --i;
        }
        quickSort(array, from, i);
        quickSort(array, i, to);
    }

    private static int min(int a, int b) {
        return a < b ? a : b;
    }

    private static String arrayToString(int[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (array.length > 0) {
            sb.append(array[0]);
            for (int i = 1; i < array.length; ++i) {
                sb.append(", ").append(array[i]);
            }
        }
        return sb.append("]").toString();
    }

    private static int[] createArray() {
        return new int[] { 5, 4, 10, 3, 12, 13, 9, 2, 1, 18 };
    }
}
