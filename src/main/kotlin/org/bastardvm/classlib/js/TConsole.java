package org.bastardvm.classlib.js;

import org.bastardvm.classlib.java.lang.TString;
import org.bastardvm.ffi.JSBody;

public final class TConsole {
    private TConsole() {
    }

    public static void println(String obj) {
        TString string = (TString) (Object) obj;
        printlnImpl(string.nativeString);
    }

    @JSBody(parameters = "str", script = "console.log(str);")
    private static native void printlnImpl(TString.NativeString str);
}
