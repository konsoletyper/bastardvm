package org.bastardvm.classlib.java.lang;

import org.bastardvm.ffi.Omitted;

public class TObject {
    private static int idGenerator = 0;
    int id;

    @Omitted
    public TObject() {
    }

    private void init() {
        id = idGenerator++;
    }

    public boolean equals(Object other) {
        return this == other;
    }

    public int hashCode() {
        return id;
    }

    public String toString() {
        return "Object@" + id;
    }
}
