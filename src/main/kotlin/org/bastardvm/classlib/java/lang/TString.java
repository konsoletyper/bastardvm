package org.bastardvm.classlib.java.lang;

import org.bastardvm.ffi.JSBody;
import org.bastardvm.ffi.Omitted;

public class TString {
    public final NativeString nativeString;

    public TString(NativeString nativeString) {
        this.nativeString = nativeString;
    }

    public int length() {
        return NativeString.length(nativeString);
    }

    public char charAt(int index) {
        return NativeString.charAt(nativeString, index);
    }

    public TString substring(int from, int to) {
        return new TString(NativeString.substring(nativeString, from, to));
    }

    public String toString() {
        return (String) (Object) this;
    }

    public static TString valueOf(int n) {
        return new TString(NativeString.fromInt(n));
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof TString)) {
            return false;
        }
        return NativeString.equals(nativeString, ((TString) other).nativeString) != 0;
    }

    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < length(); i++) {
            hash = 31 * hash + charAt(i);
        }
        return hash;
    }

    @Omitted
    public static class NativeString {
        private NativeString() {
        }

        @JSBody(parameters = "$this", script = "return $this.length;")
        public static native int length(NativeString self);

        @JSBody(parameters = { "$this", "index" }, script = "return $this.charCodeAt(index);")
        public static native char charAt(NativeString self, int index);

        @JSBody(parameters = { "$this", "from", "to" }, script = "return $this.substring(from, to);")
        public static native NativeString substring(NativeString self, int from, int to);

        @JSBody(parameters = { "a", "b" }, script = "return (a === b) ? 1 : 0;")
        public static native int equals(NativeString a, NativeString b);

        @JSBody(script = "return \"\";")
        public static native NativeString empty();

        @JSBody(parameters = { "a", "b" }, script = "return a + b;")
        public static native NativeString concat(NativeString a, NativeString b);

        @JSBody(parameters = "code", script = "return String.fromCharCode(code);")
        public static native NativeString fromCharCode(int code);

        @JSBody(parameters = "code", script = "return code.toString();")
        public static native NativeString fromInt(int n);
    }
}
