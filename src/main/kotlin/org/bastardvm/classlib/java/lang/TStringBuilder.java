package org.bastardvm.classlib.java.lang;

public class TStringBuilder {
    private TString.NativeString nativeString;

    public TStringBuilder() {
        nativeString = TString.NativeString.empty();
    }

    public String toString() {
        return (String) (Object) new TString(nativeString);
    }

    public TStringBuilder append(TString string) {
        nativeString = TString.NativeString.concat(nativeString, string.nativeString);
        return this;
    }

    public TStringBuilder append(char c) {
        nativeString = TString.NativeString.concat(nativeString, TString.NativeString.fromCharCode(c));
        return this;
    }

    public TStringBuilder append(int n) {
        nativeString = TString.NativeString.concat(nativeString, TString.NativeString.fromInt(n));
        return this;
    }
}
